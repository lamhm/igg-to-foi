#!/bin/bash

################################################################################
## Set run variables
################################################################################

MCMC_LENGTH=800000
MCMC_BURNIN=300000

MCMC_INPUT_PARAM_FILE="Continuous_Models/parameterPool_conModels.json"
MCMC_INPUT_POP_DIR="Continuous_Models/models"
MCMC_SAMPLE_DATA="VNM_samples.csv"

MCMC_PRESET_PARAM_FILE=
MCMC_PRESET_BURNIN=

MCMC_OUT_DIR="Continuous_Models/mcmc_output" 
MCMC_OUT_SUFFIX=".800k"

MCMC_EXE="../InfEst"
N_PARALLEL_PROCESS=3


################################################################################
## Run the MCMC
################################################################################

mkdir -p ${MCMC_OUT_DIR};
SEPARATOR_LINE="----------------------------------------"

echo
date
echo "${SEPARATOR_LINE}"
echo
echo "  Executable file      :  ${MCMC_EXE}"
echo "  MCMC chain length    :  ${MCMC_LENGTH}"
echo "  Burn-in length       :  ${MCMC_BURNIN}"
echo
echo "  Input pop. directory :  ${MCMC_INPUT_POP_DIR}"
echo "  Input parameter file :  ${MCMC_INPUT_PARAM_FILE}"
echo "  Input sample file    :  ${MCMC_SAMPLE_DATA}"
echo
echo "  Output suffix        :  ${MCMC_OUT_SUFFIX}"
echo "  Output directory     :  ${MCMC_OUT_DIR}"
echo



## Disable exit on errors
set +e

MCMC_CLI_OPT_PARAMS=""
if [ "$MCMC_PRESET_PARAM_FILE" ]; then
	MCMC_CLI_OPT_PARAMS="${MCMC_CLI_OPT_PARAMS}  --param-preset ${MCMC_PRESET_PARAM_FILE}" ;
	if [ "$MCMC_PRESET_BURNIN" ]; then
		MCMC_CLI_OPT_PARAMS="${MCMC_CLI_OPT_PARAMS}  --preset-burnin ${MCMC_PRESET_BURNIN}" ;
	fi ;
fi


find ${MCMC_INPUT_POP_DIR}/ -maxdepth 1 -name "*.json" -type f -exec basename -a -- {} +  \
| sort -n  \
| parallel -j ${N_PARALLEL_PROCESS} \
	echo "${SEPARATOR_LINE}" ';'  \
	./${MCMC_EXE}  --param ${MCMC_INPUT_PARAM_FILE}  \
				   --pop ${MCMC_INPUT_POP_DIR}/{}  \
	               -o ${MCMC_OUT_DIR}/{.}${MCMC_OUT_SUFFIX}.json  \
	               -s ${MCMC_SAMPLE_DATA}  \
	               -l ${MCMC_LENGTH}  \
	               -b ${MCMC_BURNIN}  ${MCMC_CLI_OPT_PARAMS}  \
| tee -a "${MCMC_OUT_DIR}/log.txt"
