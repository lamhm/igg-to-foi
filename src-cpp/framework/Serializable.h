/**
 * Created by Ha Minh Lam on 2018-01-21.
 */

#ifndef DENGUEFOI_SERIALIZABLE_H
#define DENGUEFOI_SERIALIZABLE_H

#include <string>
#include "library/json.h"

using Json = nlohmann::json;

class Serializable {
public:
    virtual Json toJson() const = 0;
};


#endif //DENGUEFOI_SERIALIZABLE_H
