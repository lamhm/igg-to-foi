/**
 * Created by Ha Minh Lam on 2018-01-15.
 */

#ifndef DENGUEELISA_OBSERVABLE_H
#define DENGUEELISA_OBSERVABLE_H

#include <vector>
#include "Observer.h"

class Observable {
protected:
    std::vector<Observer *> observers;

public:
    virtual void attachObserver(Observer * newObserver);
    
    virtual void detachObserver(Observer * observer);
    
    virtual unsigned long countObservers() const;
    
    virtual void notifyObservers() const;
};


#endif //DENGUEELISA_OBSERVABLE_H
