/**


 * Created by Ha Minh Lam on 2018-03-02.
 */

#include "Observer.h"
#include "Observable.h"


Observer::~Observer() {
    // Ask Observable instances to remove this Observer from their observer list
    for (Observable* observable : this->observables) {
        if (observable != nullptr) {
            observable->detachObserver(this);
        }
    }
}
