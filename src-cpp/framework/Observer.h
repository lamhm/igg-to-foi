/**
 * Created by Ha Minh Lam on 2018-01-15.
 */

#ifndef DENGUEELISA_OBSERVER_H
#define DENGUEELISA_OBSERVER_H

#include <vector>


class Observable;


class Observer {
    friend class Observable;

private:
    std::vector<Observable *> observables;

public:
    virtual ~Observer();
    
    virtual void update(const Observable * caller) = 0;
};


#endif //DENGUEELISA_OBSERVER_H
