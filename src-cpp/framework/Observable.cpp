/**
 * Created by Ha Minh Lam on 2018-01-15.
 */

#include "Observable.h"
#include <algorithm>


void Observable::attachObserver(Observer * newObserver) {
    if (newObserver == nullptr) {
        return;
    }
    
    for (Observer* observer : this->observers) {
        if (observer == newObserver) {
            return;
        }
    }
    
    this->observers.push_back(newObserver);
    newObserver->observables.push_back(this);
}


unsigned long Observable::countObservers() const {
    return this->observers.size();
}


void Observable::notifyObservers() const {
    for (Observer* observerPtr : this->observers) {
        observerPtr->update(this);
    }
}


void Observable::detachObserver(Observer * observer) {
    if (observer != nullptr) {
        observers.erase(
                std::remove(observers.begin(), observers.end(), observer),
                observers.end());
    }
}

