/**
 * Created by Ha Minh Lam on 2018-01-21.
 */

#include "UniformProposer.h"
#include "parameter/parameter/Parameter.h"
#include "library/RandomEngine.h"


// -----------------------------------------------------------------------------

UniformProposer::UniformProposer() : Proposer() {
    // Do nothing.
}

// -----------------------------------------------------------------------------

UniformProposer::UniformProposer(Json const &json) : Proposer(json) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

std::string const &UniformProposer::getTypeName() const {
    return Proposer::TypeName::UNIFORM;
}

// -----------------------------------------------------------------------------

//const Proposer::Type UniformProposer::getType() const {
//    return Proposer::Type::UNIFORM;
//}

// -----------------------------------------------------------------------------

Decimal::Type
UniformProposer::proposeNewValue(Decimal::Type const &samplingCenter) const {
    Decimal::Type upBoundVal = Decimal::biggest();
    if (upBound != nullptr &&
        !Decimal::isNaN(upBound->getLastValueAsDecimal())) {
        upBoundVal = upBound->getLastValueAsDecimal();
    }
    
    Decimal::Type lowBoundVal = Decimal::smallest();
    if (lowBound != nullptr &&
        !Decimal::isNaN(lowBound->getLastValueAsDecimal())) {
        lowBoundVal = lowBound->getLastValueAsDecimal();
    }
    
    if (!Decimal::isNaN(this->deviation)) {
        Decimal::Type samplingTop = samplingCenter + this->deviation;
        Decimal::Type samplingBot = samplingCenter - this->deviation;
        
        if (samplingTop > upBoundVal) {
            samplingTop = upBoundVal;
        }
        if (samplingBot < lowBoundVal) {
            samplingBot = lowBoundVal;
        }
        
        return RandomEngine::singleton()->uniformReal<Decimal::Type>(
                samplingBot, samplingTop);
        
    } else {
        return RandomEngine::singleton()->uniformReal<Decimal::Type>(
                lowBoundVal, upBoundVal);
    }
}

// -----------------------------------------------------------------------------

Integer::Type
UniformProposer::proposeNewValue(Integer::Type const &samplingCenter) const {
    Integer::Type upBoundVal = Integer::biggest();
    if (upBound != nullptr && !Integer::isNaN(upBound->getLastValueAsInt())) {
        upBoundVal = upBound->getLastValueAsInt();
    }
    
    Integer::Type lowBoundVal = Integer::smallest();
    if (lowBound != nullptr && !Integer::isNaN(lowBound->getLastValueAsInt())) {
        lowBoundVal = lowBound->getLastValueAsInt();
    }
    
    if (!Decimal::isNaN(this->deviation)) {
        auto intDeviation =
                static_cast<Integer::Type>(std::round(this->deviation));
        Integer::Type samplingTop = samplingCenter + intDeviation;
        Integer::Type samplingBot = samplingCenter - intDeviation;
        
        if (samplingTop > upBoundVal) {
            samplingTop = upBoundVal;
        }
        if (samplingBot < lowBoundVal) {
            samplingBot = lowBoundVal;
        }
        
        return RandomEngine::singleton()->uniformInt<Integer::Type>(
                samplingBot, samplingTop);
        
    } else {
        return RandomEngine::singleton()->uniformInt<Integer::Type>(
                lowBoundVal, upBoundVal);
    }
}

// -----------------------------------------------------------------------------

