/**
 * Created by Ha Minh Lam on 2018-01-21.
 */

#include <library/stats.h>
#include "Proposer.h"
#include "UniformProposer.h"
#include "GaussianProposer.h"
#include "parameter/parameter/Parameter.h"
#include "parameter/ParameterPool.h"
#include "parameter/parameter/DecimalParameter.h"
#include "util/format.h"


// /////////////////////////////////////////////////////////////////////////////
// STATIC CONSTANTS
// /////////////////////////////////////////////////////////////////////////////

// The inverse CDF of Normal(0, 1) distribution at (0.234/2) quantile
const Decimal::Type Proposer::INVERSE_CDF_AT_HALF_OPTIMAL_ACCEPT_RATE =
        -1.1901180418964234509;

const Decimal::Type Proposer::MIN_ACCEPT_RATE = 0.15;
const Decimal::Type Proposer::MAX_ACCEPT_RATE = 0.50;

const std::string Proposer::AttrKey::LOW_BOUND = "lower_bound";
const std::string Proposer::AttrKey::UP_BOUND = "upper_bound";
const std::string Proposer::AttrKey::METHOD = "method";
const std::string Proposer::AttrKey::DEVIATION = "deviation";
const std::string Proposer::AttrKey::TUNING_REQUIRED = "tuning_required";

const std::string Proposer::TypeName::UNIFORM = "uniform";
const std::string Proposer::TypeName::GAUSSIAN = "gaussian";
//const std::string Proposer::TypeName::BETA = "beta";
//const std::string Proposer::TypeName::PRESET = "preset";


// /////////////////////////////////////////////////////////////////////////////
// STATIC METHODS
// /////////////////////////////////////////////////////////////////////////////

Proposer *Proposer::newFromJson(Json const &jsonNode) {
    if (!jsonNode.is_object()) {
        throw std::runtime_error("ERROR: Invalid proposer declaration.");
    }

    if (jsonNode.element_exists(AttrKey::METHOD)) {
        if (jsonNode[AttrKey::METHOD].is_string()) {
            std::string method = jsonNode[AttrKey::METHOD].get<std::string>();

            // Make the method name case-insensitive.
            method = format::toLowerCase(method);

            if (method == TypeName::UNIFORM) {
                return new UniformProposer(jsonNode);
            }
            if (method == TypeName::GAUSSIAN) {
                return new GaussianProposer(jsonNode);
            } else {
                throw std::runtime_error("ERROR: Invalid proposing method.");
            }
        }

    } else {
        throw std::runtime_error("ERROR: Proposing method is required.");
    }

    return nullptr;
}


// /////////////////////////////////////////////////////////////////////////////
// MEMBER METHODS
// /////////////////////////////////////////////////////////////////////////////

Proposer::Proposer() : lowBound(nullptr), upBound(nullptr),
                       linkedLowBound(false), linkedUpBound(false),
                       deviation(Decimal::NaN()),
                       isTuningRequired(true), isTuningFinished(false) {
    // Do nothing
}

// -----------------------------------------------------------------------------

Proposer::Proposer(Json const &jsonNode) : Proposer() {
    if (!jsonNode.is_object()) {
        throw std::runtime_error("ERROR: Invalid JSON object "
                                 "(expecting a Proposer structure).");
    }

    if (jsonNode.element_exists(AttrKey::LOW_BOUND)) {
        Json lowBoundJson = jsonNode[AttrKey::LOW_BOUND];
        if (lowBoundJson.is_number()) {
            this->lowBound = new DecimalParameter(
                    lowBoundJson.get<Decimal::Type>(), true);

        } else if (lowBoundJson.is_string()) {
            this->lowBound = new DecimalParameter(
                    lowBoundJson.get<std::string>(), true);
        }
    }

    if (jsonNode.element_exists(AttrKey::UP_BOUND)) {
        Json upBoundJson = jsonNode[AttrKey::UP_BOUND];
        if (upBoundJson.is_number()) {
            this->upBound = new DecimalParameter(
                    upBoundJson.get<Decimal::Type>(), true);

        } else if (upBoundJson.is_string()) {
            this->upBound = new DecimalParameter(
                    upBoundJson.get<std::string>(), true);
        }
    }

    if (jsonNode.element_exists(AttrKey::DEVIATION)
        && jsonNode[AttrKey::DEVIATION].is_number()) {
        this->deviation = jsonNode[AttrKey::DEVIATION].get<Decimal::Type>();
    }

    if (jsonNode.element_exists(AttrKey::TUNING_REQUIRED)
        && jsonNode[AttrKey::TUNING_REQUIRED].is_boolean()) {
        this->isTuningRequired = jsonNode[AttrKey::TUNING_REQUIRED].get<bool>();
    }
}


// -----------------------------------------------------------------------------

Proposer::~Proposer() {
    if (this->upBound != nullptr && !this->linkedUpBound) {
        /* Only delete the upBound object if it is a dummy Parameter object
         * (not stored in the Parameter Pool). */
        delete this->upBound;
        this->upBound = nullptr;
    }
    if (this->lowBound != nullptr && !this->linkedLowBound) {
        /* Only delete the lowBound object if it is a dummy Parameter object
         * (not stored in the Parameter Pool). */
        delete this->lowBound;
        this->lowBound = nullptr;
    }
}

// -----------------------------------------------------------------------------

Json Proposer::toJson() const {
    Json json;

    json[AttrKey::METHOD] = this->getTypeName();

    if (this->lowBound != nullptr) {
        if (this->lowBound->hasId()) {
            json[AttrKey::LOW_BOUND] = this->lowBound->getId();
        } else if (this->lowBound->hasValues()) {
            json[AttrKey::LOW_BOUND] = this->lowBound->getLastValueAsDecimal();
        }
    }

    if (this->upBound != nullptr) {
        if (this->upBound->hasId()) {
            json[AttrKey::UP_BOUND] = this->upBound->getId();
        } else if (this->upBound->hasValues()) {
            json[AttrKey::UP_BOUND] = this->upBound->getLastValueAsDecimal();
        }
    }

    if (!Decimal::isNaN(this->deviation)) {
        json[AttrKey::DEVIATION] = this->deviation;
    }

    json[AttrKey::TUNING_REQUIRED] = this->isTuningRequired;

    return json;
}

// -----------------------------------------------------------------------------

void Proposer::linkBoundariesToParameters(const ParameterPool &parameterPool) {
    if (this->upBound != nullptr && this->upBound->hasId()
        && this->upBound->isDummy()) {
        Parameter *linkedParam =
                parameterPool.searchParameterById(this->upBound->getId());
        if (linkedParam == nullptr) {
            throw std::runtime_error(
                    "ERROR: Cannot link proposer boundary (parameter "
                    + this->upBound->getId() + " not found).");
        } else {
            delete this->upBound;
            this->upBound = linkedParam;
            this->linkedUpBound = true;
        }
    }

    if (this->lowBound != nullptr && this->lowBound->hasId()
        && this->lowBound->isDummy()) {
        Parameter *linkedParam =
                parameterPool.searchParameterById(this->lowBound->getId());
        if (linkedParam == nullptr) {
            throw std::runtime_error(
                    "ERROR: Cannot link proposer boundary (parameter "
                    + this->lowBound->getId() + " not found).");
        } else {
            delete this->lowBound;
            this->lowBound = linkedParam;
            this->linkedLowBound = true;
        }
    }
}

// -----------------------------------------------------------------------------

void Proposer::setUpBound(Parameter *newUpBound) {
    if (newUpBound == nullptr) {
        return;
    }

    if (this->upBound == nullptr) {
        this->upBound = newUpBound;

    } else if (this->upBound->isDummy()) {
        if (!newUpBound->isDummy()) {
            this->upBound = newUpBound;
        }

    } else {
        throw std::runtime_error(
                "ERROR: Proposer -- Cannot change the upper "
                "bound while it is linked to a real parameter.");
    }

    if (this->upBound->isDummy()) {
        this->linkedUpBound = false;
    } else {
        linkedUpBound = true;
    }
}

// -----------------------------------------------------------------------------

void Proposer::setLowBound(Parameter *newLowBound) {
    if (newLowBound == nullptr) {
        return;
    }

    if (this->lowBound == nullptr) {
        this->lowBound = newLowBound;

    } else if (this->lowBound->isDummy()) {
        if (!newLowBound->isDummy()) {
            this->lowBound = newLowBound;
        }

    } else {
        throw std::runtime_error(
                "ERROR: Proposer -- Cannot change the lower "
                "bound while it is linked to a real parameter.");
    }

    if (this->lowBound->isDummy()) {
        this->linkedLowBound = false;
    } else {
        linkedLowBound = true;
    }
}

// -----------------------------------------------------------------------------

bool Proposer::tune(Decimal::Type currentAcceptRate) {
    if ((!isTuningRequired) || isTuningFinished) {
        return false;
    }

    if (currentAcceptRate >= MIN_ACCEPT_RATE &&
        currentAcceptRate <= MAX_ACCEPT_RATE) {
        isTuningFinished = true;
        return false;  // No more tuning required.
    }

    /* Modify the current accept rate if necessary to avoid over-tuning */
    if (currentAcceptRate < 0.05) {
        currentAcceptRate = 0.05;
    } else if (currentAcceptRate > 0.95) {
        currentAcceptRate = 0.95;
    }

    this->deviation *=
            INVERSE_CDF_AT_HALF_OPTIMAL_ACCEPT_RATE
            / stats::normalCdfInvert<Decimal::Type>(currentAcceptRate / 2.0,
                                                    0.0, 1.0);
    return true;  // More tuning may be required.
}

// -----------------------------------------------------------------------------

