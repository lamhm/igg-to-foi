/**
 * Created by Ha Minh Lam on 2018-01-21.
 */

#ifndef DENGUEFOI_GAUSSIANPROPOSER_H
#define DENGUEFOI_GAUSSIANPROPOSER_H


#include "Proposer.h"

class GaussianProposer : public Proposer {
protected:

    std::string const &getTypeName() const override;
    
public:

    explicit GaussianProposer();
    explicit GaussianProposer(const Json &json);

//    Type const getType() const override;

    Decimal::Type
    proposeNewValue(Decimal::Type const &samplingCenter) const override;

    Integer::Type
    proposeNewValue(Integer::Type const &samplingCenter) const override;
};


#endif //DENGUEFOI_GAUSSIANPROPOSER_H
