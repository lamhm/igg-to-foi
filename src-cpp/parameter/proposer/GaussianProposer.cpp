/**
 * Created by Ha Minh Lam on 2018-01-21.
 */

#include "GaussianProposer.h"

#include "parameter/parameter/Parameter.h"
#include "library/RandomEngine.h"


// -----------------------------------------------------------------------------

GaussianProposer::GaussianProposer() : Proposer() {
    // Do nothing.
}

// -----------------------------------------------------------------------------

GaussianProposer::GaussianProposer(Json const &json) : Proposer(json) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

std::string const &GaussianProposer::getTypeName() const {
    return Proposer::TypeName::GAUSSIAN;
}

// -----------------------------------------------------------------------------

//const Proposer::Type GaussianProposer::getType() const {
//    return Proposer::Type::GAUSSIAN;
//}

// -----------------------------------------------------------------------------

Decimal::Type
GaussianProposer::proposeNewValue(Decimal::Type const &samplingCenter) const {
    Decimal::Type upBoundVal = Decimal::biggest();
    if (upBound != nullptr &&
        !Decimal::isNaN(upBound->getLastValueAsDecimal())) {
        upBoundVal = upBound->getLastValueAsDecimal();
    }

    Decimal::Type lowBoundVal = Decimal::smallest();
    if (lowBound != nullptr &&
        !Decimal::isNaN(lowBound->getLastValueAsDecimal())) {
        lowBoundVal = lowBound->getLastValueAsDecimal();
    }

    if (!Decimal::isNaN(this->deviation)) {
        return RandomEngine::singleton()->normalTruncated(samplingCenter,
                                                          this->deviation,
                                                          lowBoundVal,
                                                          upBoundVal);



    } else {
        throw std::runtime_error(
                "ERROR: Gaussian proposers always require a deviation.");
    }
}

// -----------------------------------------------------------------------------

Integer::Type
GaussianProposer::proposeNewValue(Integer::Type const &samplingCenter) const {
    throw std::logic_error( "ERROR: Gaussian proposers should not be used to "
                            "generate integer samples.");
}

// -----------------------------------------------------------------------------



