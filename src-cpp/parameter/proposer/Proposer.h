/**
 * Created by Ha Minh Lam on 2018-01-21.
 */

#ifndef DENGUEFOI_PROPOSER_H
#define DENGUEFOI_PROPOSER_H

#include <string>
#include "library/Number.h"
#include "framework/Serializable.h"
#include "parameter/ParameterPool.h"


class Parameter;


class Proposer : public Serializable {

private:
    static const Decimal::Type INVERSE_CDF_AT_HALF_OPTIMAL_ACCEPT_RATE;
    static const Decimal::Type MIN_ACCEPT_RATE;
    static const Decimal::Type MAX_ACCEPT_RATE;

    /**
     * This struct contains constants that are needed to serialize Proposer
     * objects.
     */
    struct AttrKey {
        static const std::string LOW_BOUND;
        static const std::string UP_BOUND;
        static const std::string DEVIATION;
        static const std::string METHOD;
        static const std::string TUNING_REQUIRED;
    };

    bool linkedLowBound;
    bool linkedUpBound;

protected:
    struct TypeName {
        static const std::string UNIFORM;
        static const std::string GAUSSIAN;
        //static const std::string BETA;
        //static const std::string PRESET;
    };

    Parameter * lowBound;
    Parameter * upBound;
    Decimal::Type deviation;

    bool isTuningRequired;
    bool isTuningFinished;

    /**
     * This member variable is set to be TRUE only when this proposer uses a
     * parameter from the parameter pool as its upper bound or lower bound but
     * that parameter has NOT been linked to this proposer object.
     */
    //bool parameterBindingRequired;

    explicit Proposer();

    explicit Proposer(Json const &jsonNode);

    virtual std::string const &getTypeName() const = 0;


public:
//    enum class Type {
//        UNIFORM, GAUSSIAN
//    };

    static Proposer * newFromJson(Json const &jsonNode);

    virtual ~Proposer();

    /**
     * Link the lower and/or the upper bounds of the proposer to parameters
     * taken from the given parameter pool.
     * @param parameterPool A parameter pool.
     */
    virtual void linkBoundariesToParameters(ParameterPool const &parameterPool);

    virtual Decimal::Type
    proposeNewValue(Decimal::Type const &samplingCenter) const = 0;

    virtual Integer::Type
    proposeNewValue(Integer::Type const &samplingCenter) const = 0;

//    virtual Type const getType() const = 0;

    Json toJson() const override;

    virtual void setUpBound(Parameter * newUpBound);

    virtual void setLowBound(Parameter * newLowBound);

    /**
     * Fine-tune the proposal distribution.
     * @param currentAcceptRate The current accept rate of the parameter.
     * @return TRUE if more tuning may be required; FALSE otherwise.
     */
    virtual bool tune(Decimal::Type currentAcceptRate);
};


#endif //DENGUEFOI_PROPOSER_H
