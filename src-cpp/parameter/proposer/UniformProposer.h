/**
 * Created by Ha Minh Lam on 2018-01-21.
 */

#ifndef DENGUEFOI_UNIFORMPROPOSER_H
#define DENGUEFOI_UNIFORMPROPOSER_H


#include "Proposer.h"


class UniformProposer : public Proposer {
protected:
    
    std::string const &getTypeName() const override;


public:
    
    explicit UniformProposer();
    
    explicit UniformProposer(Json const &json);
    
//    Type const getType() const override;
    
    Decimal::Type
    proposeNewValue(Decimal::Type const &samplingCenter) const override;
    
    Integer::Type
    proposeNewValue(Integer::Type const &samplingCenter) const override;
    
};


#endif //DENGUEFOI_UNIFORMPROPOSER_H
