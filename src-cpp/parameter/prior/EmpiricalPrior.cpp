/**
 * Created by Ha Minh Lam on 2018-07-19.
 */

#include "EmpiricalPrior.h"
#include <limits>
#include <parameter/parameter/Parameter.h>


// -----------------------------------------------------------------------------
// STATIC CONSTANTS & METHODS
// -----------------------------------------------------------------------------

const Decimal::Type EmpiricalPrior::WORST_LOG_PROBABILITY =
        static_cast<Decimal::Type>(
                std::numeric_limits<Decimal::Type>::min_exponent - 1);

/* The default smooth factors were based on a Binomial(0.5) distribution */
const std::vector<Decimal::Type> EmpiricalPrior::DEFAULT_SMOOTH_FACTORS(
        {5, 23, 92, 312, 895, 2169, 4458, 7791, 11592, 14705, 15918,
         14705, 11592, 7791, 4458, 2169, 895, 312, 92, 23, 5});

const std::string EmpiricalPrior::AttrKey::LOG_DENSITY = "log_density";
const std::string EmpiricalPrior::AttrKey::LOW_END = "low_end";
const std::string EmpiricalPrior::AttrKey::HIGH_END = "high_end";

const std::string EmpiricalPrior::AttrKey::PROB_SOURCE = "prob_density_source";
const std::string EmpiricalPrior::AttrKey::NUMBER_OF_BINS = "number_of_bins";

// -----------------------------------------------------------------------------

void
EmpiricalPrior::convertFreqToProb(std::vector<Decimal::Type> &frequencies,
                                  const Decimal::Type &binWidth,
                                  const bool &saveProbOnLogScale) {
    // Calculate the summation of the frequencies
    Decimal::Type sum = 0.0;
    for (const auto &value : frequencies) {
        sum += value;
    }
    
    // Convert frequencies to probability
    for (auto &value : frequencies) {
        value /= (sum * binWidth);
        if (saveProbOnLogScale) {
            if (value <= 0.0l) {
                value = WORST_LOG_PROBABILITY;
            } else {
                value = std::log(value);
            }
        }
    }
}


// -----------------------------------------------------------------------------
// MEMBER METHODS
// -----------------------------------------------------------------------------

EmpiricalPrior::EmpiricalPrior()
        : logDensity(), lowEnd(Decimal::NaN()), highEnd(Decimal::NaN()),
          stepWidth(0.0), probDensitySource(), numberOfBins(0) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

EmpiricalPrior::EmpiricalPrior(Json const &json) : EmpiricalPrior() {
    if (json.element_exists(AttrKey::PROB_SOURCE)) {
        Json probDensitySourceJson = json[AttrKey::PROB_SOURCE];
        if (probDensitySourceJson.is_string()) {
            this->probDensitySource = probDensitySourceJson.get<std::string>();
        } else {
            throw std::runtime_error(
                    "ERROR: EmpiricalPrior -- Invalid source ID.");
        }
        if (this->probDensitySource.empty()) {
            throw std::runtime_error(
                    "ERROR: EmpiricalPrior -- Source ID must not be empty.");
        }
    }
    
    if (json.element_exists(AttrKey::NUMBER_OF_BINS)) {
        Json numberOfBinsJson = json[AttrKey::NUMBER_OF_BINS];
        if (numberOfBinsJson.is_number_integer()) {
            this->numberOfBins = numberOfBinsJson.get<Integer::Type>();
        } else {
            throw std::runtime_error("ERROR: EmpiricalPrior -- "
                                     "The number of bins must be an integer.");
        }
    }
    
    if (json.element_exists(AttrKey::LOG_DENSITY)) {
        Json logDensityJson = json[AttrKey::LOG_DENSITY];
        bool wrongTypeError = false;
        if (logDensityJson.is_number()) {
            this->logDensity.emplace_back(logDensityJson.get<Decimal::Type>());
        } else if (logDensityJson.is_array()) {
            for (const Json &logProbValue : logDensityJson) {
                if (logProbValue.is_number()) {
                    this->logDensity.emplace_back(
                            logProbValue.get<Decimal::Type>());
                } else {
                    wrongTypeError = true;
                }
            }
        } else {
            wrongTypeError = true;
        }
        if (wrongTypeError) {
            throw std::runtime_error(
                    "ERROR: EmpiricalPrior -- Invalid probability values.");
            
        }
        
        if (json.element_exists(AttrKey::LOW_END)) {
            Json lowEndJson = json[AttrKey::LOW_END];
            if (lowEndJson.is_number()) {
                this->lowEnd = lowEndJson.get<Decimal::Type>();
            } else {
                throw std::runtime_error(
                        "ERROR: EmpiricalPrior -- Invalid low-end value.");
            }
        }
        
        if (json.element_exists(AttrKey::HIGH_END)) {
            Json highEndJson = json[AttrKey::HIGH_END];
            if (highEndJson.is_number()) {
                this->highEnd = highEndJson.get<Decimal::Type>();
            } else {
                throw std::runtime_error(
                        "ERROR: EmpiricalPrior -- Invalid high-end value.");
            }
        }
        
        if (Decimal::isNaN(lowEnd) || Decimal::isNaN(highEnd)) {
            throw std::runtime_error("ERROR: EmpiricalPrior -- Both low-end "
                                     "and high-end must be specified.");
        } else if (this->highEnd <= this->lowEnd) {
            throw std::runtime_error("ERROR: EmpiricalPrior -- Low-end "
                                     "must be smaller than high-end.");
        }
        
        this->numberOfBins = static_cast<Integer::Type>(this->logDensity.size());
        this->stepWidth = (this->highEnd - this->lowEnd) / this->numberOfBins;
        
    } else {
        // The probDensity vector is not specified
        if (this->probDensitySource.empty() || this->numberOfBins <= 0) {
            throw std::runtime_error(
                    "ERROR: EmpiricalPrior -- Both the source ID and the "
                    "number of bins must be specified.");
        }
    }
}

// -----------------------------------------------------------------------------

std::string const &EmpiricalPrior::getTypeName() const {
    return Prior::TypeName::EMPIRICAL;
}

// -----------------------------------------------------------------------------

const Prior::Type EmpiricalPrior::getType() const {
    return Prior::Type::EMPIRICAL;
}

// -----------------------------------------------------------------------------

Decimal::Type
EmpiricalPrior::computeLogProb(Decimal::Type const &paramValue) const {
    if (numberOfBins <= 0 || paramValue < lowEnd || paramValue >= highEnd) {
        return WORST_LOG_PROBABILITY;
    }
    
    auto binIdx = static_cast<Integer::Type>(
            std::floor((paramValue - this->lowEnd) / this->stepWidth)
    );
    return logDensity[binIdx];
}

// -----------------------------------------------------------------------------

Json EmpiricalPrior::toJson() const {
    Json json = Prior::toJson();
    
    if (!this->logDensity.empty() && !Decimal::isNaN(this->lowEnd)
        && !Decimal::isNaN(this->highEnd)) {
        json[AttrKey::LOG_DENSITY] = this->logDensity;
        json[AttrKey::LOW_END] = this->lowEnd;
        json[AttrKey::HIGH_END] = this->highEnd;
    }
    
    if (this->numberOfBins > 0) {
        json[AttrKey::NUMBER_OF_BINS] = this->numberOfBins;
    }
    
    if (!this->probDensitySource.empty()) {
        json[AttrKey::PROB_SOURCE] = this->probDensitySource;
    }
    
    return json;
}

// -----------------------------------------------------------------------------

void
EmpiricalPrior::loadProbDensityFromSource(
        ParameterPool const &parameterPool,
        unsigned long const &burnin,
        std::vector<long double> smoothFactors) {
    
    if (this->probDensitySource.empty() || this->numberOfBins <= 0) {
        return;
    }
    
    Parameter * sourceParam =
            parameterPool.searchParameterById(this->probDensitySource);
    if (sourceParam == nullptr) {
        // Do not throw errors since this prior may never be used.
        // Just stop the loading process.
        return;
    }
    
    if (sourceParam->getValueCount() <= burnin) {
        throw std::runtime_error("ERROR: EmpiricalPrior -- Burn-in value ("
                                 + std::to_string(burnin) + ") is too high.");
    }
    
    std::vector<Decimal::Type> sourceValues = sourceParam->getValues();
    
    this->lowEnd = std::numeric_limits<Decimal::Type>::max();
    this->highEnd = std::numeric_limits<Decimal::Type>::lowest();
    for (auto it = sourceValues.begin() + burnin;
         it != sourceValues.end(); it++) {
        if (this->lowEnd > *it) {
            this->lowEnd = *it;
        }
        if (this->highEnd < *it) {
            this->highEnd = *it;
        }
    }
    if (this->lowEnd >= this->highEnd) {
        throw std::runtime_error("ERROR: EmpiricalPrior -- A source parameter "
                                 "must not be constant.");
    }
    
    this->stepWidth = (this->highEnd - this->lowEnd) / this->numberOfBins;
    auto smoothWindowSize =
            static_cast<Integer::Type>(smoothFactors.size() / 2);
    this->lowEnd -= smoothWindowSize * stepWidth;
    this->highEnd += smoothWindowSize * stepWidth;
    this->numberOfBins += 2 * smoothWindowSize;
    
    // Initialize the frequency vector. Make frequencies start from 1 to avoid
    // having log(0) in later steps.
    std::vector<Decimal::Type>
            freq(static_cast<unsigned long>(this->numberOfBins), 1.0);
    for (auto it = sourceValues.begin() + burnin;
         it != sourceValues.end(); it++) {
        auto binIdx = static_cast<Integer::Type>(
                std::floor((*it - this->lowEnd) / stepWidth) );
        freq[binIdx] += 1.0;
    }
    
    convertFreqToProb(smoothFactors, 1.0, false);
    
    this->logDensity.clear();
    this->logDensity.reserve(static_cast<unsigned long>(this->numberOfBins));
    for (Integer::Type binIdx = 0; binIdx < this->numberOfBins; binIdx++) {
        Decimal::Type weightedFreq = 0.0;
        for (Integer::Type smoothIdx = 0; smoothIdx < smoothFactors.size();
             smoothIdx++) {
            auto extendedBinIdx = binIdx - (smoothWindowSize - smoothIdx);
            if (extendedBinIdx >= 0 && extendedBinIdx < this->numberOfBins) {
                weightedFreq += freq[extendedBinIdx] * smoothFactors[smoothIdx];
            }
        }
        this->logDensity.emplace_back(weightedFreq);
    }
    
    convertFreqToProb(this->logDensity, this->stepWidth, true);
}

// -----------------------------------------------------------------------------

