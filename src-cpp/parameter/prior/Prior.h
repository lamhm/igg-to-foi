/**
 * Created by Ha Minh Lam on 2018-07-19.
 */

#ifndef DENGUEFOI_PRIOR_H
#define DENGUEFOI_PRIOR_H


#include <framework/Serializable.h>
#include <library/Number.h>

class Prior : public Serializable {
private:
    struct AttrKey {
        static const std::string TYPE;
    };

    
protected:
    struct TypeName {
        static const std::string EMPIRICAL;
    };
    
    //explicit Prior();
    //
    //explicit Prior(Json const &json);
    
    virtual std::string const &getTypeName() const = 0;
    

public:
    enum class Type {
        EMPIRICAL
    };

    virtual ~Prior() = default;

    static Prior * newFromJson(Json const &json);
    
    virtual Type const getType() const = 0;
    
    virtual Decimal::Type
    computeLogProb(Decimal::Type const &paramValue) const = 0;
    
    Json toJson() const override;
};


#endif //DENGUEFOI_PRIOR_H
