/**
 * Created by Ha Minh Lam on 2018-07-19.
 */

#include <util/format.h>
#include "Prior.h"
#include "EmpiricalPrior.h"


// -----------------------------------------------------------------------------
// STATIC CONSTANTS & METHODS
// -----------------------------------------------------------------------------

const std::string Prior::AttrKey::TYPE = "type";

const std::string Prior::TypeName::EMPIRICAL = "empirical";

// -----------------------------------------------------------------------------

Prior * Prior::newFromJson(Json const &json) {
    if (!json.is_object()) {
        throw std::runtime_error("ERROR: Invalid prior declaration.");
    }
    
    if (json.element_exists(AttrKey::TYPE)) {
        if (json[AttrKey::TYPE].is_string()) {
            std::string priorType = json[AttrKey::TYPE].get<std::string>();
            // Make the priorType case-insensitive.
            priorType = format::toLowerCase(priorType);
            if (priorType == TypeName::EMPIRICAL) {
                return new EmpiricalPrior(json);
            }
        }
        
        /* Cannot detect prior type - throw error. */
        throw std::runtime_error("ERROR: Invalid prior declaration.");
    }
    
    /* No type declared */
    return nullptr;
}


// -----------------------------------------------------------------------------
// MEMBER METHODS
// -----------------------------------------------------------------------------

Json Prior::toJson() const {
    Json json;
    json[AttrKey::TYPE] = this->getTypeName();
    return json;
}

