/**
 * Created by Ha Minh Lam on 2018-07-19.
 */

#ifndef DENGUEFOI_EMPIRICALPRIOR_H
#define DENGUEFOI_EMPIRICALPRIOR_H


#include <parameter/ParameterPool.h>
#include "Prior.h"


class EmpiricalPrior : public Prior {
private:
    static const Decimal::Type WORST_LOG_PROBABILITY;
    static const std::vector<Decimal::Type> DEFAULT_SMOOTH_FACTORS;
    
    struct AttrKey {
        static const std::string LOG_DENSITY;
        static const std::string LOW_END;
        static const std::string HIGH_END;
        static const std::string PROB_SOURCE;
        static const std::string NUMBER_OF_BINS;
    };
    
    static void convertFreqToProb(std::vector<Decimal::Type> &frequencies,
                                  const Decimal::Type &binWidth,
                                  const bool &saveProbOnLogScale);
    
    std::vector<Decimal::Type> logDensity;
    Decimal::Type lowEnd;
    Decimal::Type highEnd;
    Decimal::Type stepWidth;
    Integer::Type numberOfBins;
    std::string probDensitySource;


protected:
    std::string const &getTypeName() const override;


public:
    explicit EmpiricalPrior();
    
    explicit EmpiricalPrior(Json const &json);
    
    Type const getType() const override;
    
    Decimal::Type
    computeLogProb(Decimal::Type const &paramValue) const override;
    
    Json toJson() const override;
    
    void loadProbDensityFromSource(
            ParameterPool const &parameterPool,
            unsigned long const &burnin,
            std::vector<Decimal::Type> smoothFactors = DEFAULT_SMOOTH_FACTORS);
};


#endif //DENGUEFOI_EMPIRICALPRIOR_H
