/**
 * Created by Ha Minh Lam on 2018-06-04.
 */

#ifndef DENGUEFOI_DECIMALPARAMETER_H
#define DENGUEFOI_DECIMALPARAMETER_H


#include "Parameter.h"

class DecimalParameter : public Parameter {
private:
    std::vector<Decimal::Type> values;
    std::vector<Decimal::Type> presetValues;
    Decimal::Type proposedValue;
    
    const std::string getTypeName() const override;


public:
    explicit DecimalParameter(const std::string &id, bool const &isDummy);
    
    explicit DecimalParameter(const std::string &id,
                              const Decimal::Type &initValue);
    
    explicit DecimalParameter(const Decimal::Type &value, bool const &isDummy);
    
    explicit DecimalParameter(Json const &json);
    
    const Type getType() const override;
    
    Decimal::Type getMeanValue(unsigned long burnin) const override;
    
    const bool hasValues() const override;
    
    const unsigned long getValueCount() const override;
    
    void proposeNewValue() override;
    
    void recordCurrentValue() override;
    
    void rejectNewProposal() override;
    
    Json toJson() const override;
    
    std::vector<Decimal::Type > getValues() const override;
    
    Integer::Type getLastValueAsInt() const override;
    
    Decimal::Type getLastValueAsDecimal() const override;
    
    Integer::Type getIntValueAt(unsigned long const &index) const override;
    
    Decimal::Type getDecimalValueAt(unsigned long const &index) const override;
    
    const unsigned long getPresetSize() const override;
    
    void loadPresetValues(const ParameterPool &parameterPresetPool,
                          const unsigned long &presetBurnin) override;
    
    void usePresetValueAsProposal(Integer::Type presetIndex) override;
    
    void setProposal(Decimal::Type const &newProposal) override;
    
    void setProposal(Integer::Type const &newProposal) override;
};


#endif //DENGUEFOI_DECIMALPARAMETER_H
