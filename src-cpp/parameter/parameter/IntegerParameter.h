/**
 * Created by Ha Minh Lam on 2018-06-04.
 */

#ifndef DENGUEFOI_INTEGERPARAMETER_H
#define DENGUEFOI_INTEGERPARAMETER_H


#include "Parameter.h"


class IntegerParameter : public Parameter {
private:
    std::vector<Integer::Type> values;
    std::vector<Integer::Type> presetValues;
    Integer::Type proposedValue;
    
    const std::string getTypeName() const override;


public:
    explicit IntegerParameter(const std::string &id, bool const &isDummy);
    
    explicit IntegerParameter(const std::string &id,
                              const Integer::Type &initValue);
    
    explicit IntegerParameter(const Integer::Type &value, bool const &isDummy);
    
    explicit IntegerParameter(Json const &json);
    
    const Type getType() const override;
    
    Decimal::Type getMeanValue(unsigned long burnin) const override;
    
    const bool hasValues() const override;
    
    const unsigned long getValueCount() const override;
    
    void proposeNewValue() override;
    
    void recordCurrentValue() override;
    
    void rejectNewProposal() override;
    
    Json toJson() const override;
    
    Integer::Type getLastValueAsInt() const override;
    
    Decimal::Type getLastValueAsDecimal() const override;
    
    Integer::Type getIntValueAt(unsigned long const &index) const override;
    
    Decimal::Type getDecimalValueAt(unsigned long const &index) const override;
    
    const unsigned long getPresetSize() const override;
    
    void loadPresetValues(const ParameterPool &parameterPresetPool,
                          const unsigned long &presetBurnin) override;
    
    void usePresetValueAsProposal(Integer::Type presetIndex) override;
    
    void setProposal(Decimal::Type const &newProposal) override;
    
    void setProposal(Integer::Type const &newProposal) override;
    
    std::vector<Decimal::Type> getValues() const override;
    
};


#endif //DENGUEFOI_INTEGERPARAMETER_H
