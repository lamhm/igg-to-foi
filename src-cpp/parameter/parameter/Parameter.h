#ifndef PARAMETER_H
#define PARAMETER_H

#include <string>
#include <library/Number.h>
#include <parameter/proposer/Proposer.h>
#include <parameter/prior/Prior.h>
#include <framework/Serializable.h>
#include <framework/Observable.h>


class Parameter : public Serializable, public Observable, public Observer {
private:
    Integer::Type acceptedTimes;
    Integer::Type rejectedTimes;

protected:
    std::string id;
    Proposer * proposer;
    Prior * prior;
    
    /**
     * This indicates whether the Parameter object is only a temporary holder
     * before being replaced by another Parameter object that has the same ID.
     * This is useful to link Proposers' boundary to a Parameter.
     */
    bool dummy;
    
    Parameter * presetSampler;
    std::string presetSource;

    /**
     * The following vector store the accept rates of the parameter during the
     * tuning phase of the MCMC. This vector is used for debugging purposes only.
     */
    std::vector<Decimal::Type> acceptRates;
    
    /**
     * The probability that the proposed value of this parameter is
     * auto-accepted regardless of the likelihood of the model.
     * By default, the autoAcceptRate is 0.
     */
    Decimal::Type autoAcceptRate;
    
    Integer::Type samplingFrequency;

    /**
     * This struct contains constants that are needed to serialize Parameter
     * objects.
     */
    struct AttrKey {
        static const std::string TYPE;
        static const std::string ID;
        static const std::string VALUES;
        static const std::string PROPOSER;
        static const std::string PRIOR;
    
        static const std::string AUTO_ACCEPT_RATE;
        static const std::string ACCEPT_RATE;
        static const std::string SAMPLING_FREQUENCY;
        
        static const std::string PRESET_SAMPLER;
        static const std::string PRESET_SOURCE;
        static const std::string PRESET_VALUES;
    };
    
    struct TypeName {
        static const std::string DECIMAL;
        static const std::string INTEGER;
    };

    Decimal::Type getCurrentAcceptRate() const;

    virtual const std::string getTypeName() const = 0;
    
    explicit Parameter(const std::string &id, bool const &isDummy);
    
    explicit Parameter(Json const &json);
    
    virtual void updatePresetSampler() const;


public:
    
    static Parameter * newFromJson(Json const &json);
    
    enum class Type {
        DECIMAL, INTEGER
    };

    explicit Parameter() = delete;

    ~Parameter() override;
    
    const std::string &getId() const;
    
    Decimal::Type getAutoAcceptRate() const;
    
    Integer::Type getSamplingFrequency() const;
    
    const bool hasId() const;
    
    const bool isFixed() const;
    
    const bool isPreset() const;
    
    bool isDummy() const;
    
    Proposer * getProposer() const;
    
    const bool isInUse() const;

    /**
     * Fine-tune the proposal distribution of this parameter.
     * @return TRUE if more tuning may be required; FALSE otherwise.
     */
    bool tuneProposer();

    void update(const Observable * caller) override;
    
    /**
     * Bind the upper bound, the lower bound, and the preset sampler of this
     * parameter to other parameters (if needed).
     * @param parameterPool The parameter pool, from where the binding method
     *                      will look for necessary parameters.
     */
    virtual void linkWithOtherParameters(ParameterPool const &parameterPool);
    
    /**
     * Get the mean value of this Parameter object. 'Burn-in' values will be
     * ignored.
     * @param burnin The number of first values that will be considered as
     *               'burn-in' and be ignored during the calculation of the
     *               mean.
     * @return The mean value of this parameter.
     */
    virtual Decimal::Type getMeanValue(unsigned long burnin) const = 0;
    
    virtual const Type getType() const = 0;
    
    virtual std::vector<Decimal::Type> getValues() const = 0;
    
    virtual const bool hasValues() const = 0;
    
    virtual const unsigned long getValueCount() const = 0;
    
    virtual const unsigned long getPresetSize() const = 0;
    
    /**
     * Copy values of a parameter taken from a given parameter pool to the
     * preset of this parameter.
     * @param parameterPresetPool A parameter pool from where the preset values
     *                            will be looked for.
     * @param presetBurnin        The number of first values in the preset
     *                            values list that will be ignored as burn-in.
     */
    virtual void loadPresetValues(const ParameterPool &parameterPresetPool,
                                  const unsigned long &presetBurnin) = 0;
    
    virtual void loadEmpiricalPrior(const ParameterPool &parameterPool,
                                    const unsigned long &priorBurnin);
    
    virtual void usePresetValueAsProposal(Integer::Type presetIndex) = 0;
    
    /**
     * Propose a new value for the parameter.
     * @note If a new value is successfully proposed, Parameter#newProposal
     *       will be set to that new value. Otherwise, Parameter#newProposal
     *       will be set to NA.
     */
    virtual void proposeNewValue() = 0;
    
    /**
     * Set the proposedValue of this parameter to be the same as newValue.
     * @param newProposal A proposal value.
     * @note  This method does NOT check whether the given proposal is
     *        appropriate for the Parameter.
     */
    virtual void setProposal(Decimal::Type const &newProposal) = 0;
    
    virtual void setProposal(Integer::Type const &newProposal) = 0;

    virtual void recordCurrentValue() = 0;

    virtual void acceptNewProposal();

    virtual void rejectNewProposal();

    virtual Integer::Type getLastValueAsInt() const = 0;
    
    virtual Decimal::Type getLastValueAsDecimal() const = 0;
    
    virtual Integer::Type getIntValueAt(unsigned long const &index) const = 0;
    
    virtual Decimal::Type
    getDecimalValueAt(unsigned long const &index) const = 0;
    
    Json toJson() const override;
    
    Decimal::Type computeLogPrior() const;
};


#endif /* PARAMETER_H */
