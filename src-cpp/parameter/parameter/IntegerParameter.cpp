/**
 * Created by Ha Minh Lam on 2018-06-04.
 */

#include "IntegerParameter.h"


// -----------------------------------------------------------------------------

IntegerParameter::IntegerParameter(const std::string &id, bool const &isDummy)
        : Parameter(id, isDummy), values(), proposedValue(Integer::NaN()) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

IntegerParameter::IntegerParameter(const std::string &id,
                                   const Integer::Type &initValue)
        : IntegerParameter(id, false) {
    this->values.emplace_back(initValue);
}

// -----------------------------------------------------------------------------

IntegerParameter::IntegerParameter(const Integer::Type &value,
                                   bool const &isDummy)
        : IntegerParameter("", isDummy) {
    this->values.emplace_back(value);
}

// -----------------------------------------------------------------------------

IntegerParameter::IntegerParameter(Json const &json)
        : Parameter(json), values(), proposedValue(Integer::NaN()) {
    
    if (json.element_exists(AttrKey::VALUES)) {
        Json valuesJson = json[AttrKey::VALUES];
        
        bool error = false;
        if (valuesJson.is_number_integer()) {
            this->values.emplace_back(valuesJson.get<Integer::Type>());
            
        } else if (valuesJson.is_array()) {
            for (const Json &value : valuesJson) {
                if (value.is_number_integer()) {
                    this->values.emplace_back(value.get<Integer::Type>());
                } else {
                    error = true;
                }
            }
            
        } else {
            error = true;
        }
        
        if (error) {
            throw std::runtime_error(
                    "ERROR: Invalid parameter value [" + this->id + "].");
            
        }
    }
    
    if (json.element_exists(AttrKey::PRESET_VALUES)) {
        Json presetValuesJson = json[AttrKey::PRESET_VALUES];
        
        bool error = false;
        if (presetValuesJson.is_number_integer()) {
            this->presetValues.emplace_back(
                    presetValuesJson.get<Integer::Type>());
            
        } else if (presetValuesJson.is_array()) {
            for (const Json &presetValue : presetValuesJson) {
                if (presetValue.is_number_integer()) {
                    this->presetValues.emplace_back(
                            presetValue.get<Integer::Type>());
                } else {
                    error = true;
                }
            }
            
        } else {
            error = true;
        }
        
        if (error) {
            throw std::runtime_error(
                    "ERROR: Invalid parameter preset [" + this->id + "].");
        } else {
            this->updatePresetSampler();
        }
    }
}

// -----------------------------------------------------------------------------

const Parameter::Type IntegerParameter::getType() const {
    return Parameter::Type::INTEGER;
}

// -----------------------------------------------------------------------------

const std::string IntegerParameter::getTypeName() const {
    return TypeName::INTEGER;
}

// -----------------------------------------------------------------------------

Decimal::Type IntegerParameter::getMeanValue(unsigned long burnin) const {
    auto nVals = values.size();
    
    if (this->isFixed()) {
        if (nVals >= 1) {
            return static_cast<Decimal::Type>(this->values[0]);
        }
        
    } else {
        if (burnin >= nVals) {
            throw std::runtime_error(
                    "ERROR: The number of burn-in values must be less than" +
                    std::to_string(nVals));
        }
        
        Decimal::Type mean = 0.0l;
        for (auto i = burnin; i < nVals; i++) {
            mean += static_cast<Decimal::Type>(this->values[i]);
        }
        mean /= static_cast<long double>(nVals - burnin);
        
        return mean;
    }
    
    return Decimal::NaN();
}

// -----------------------------------------------------------------------------

const bool IntegerParameter::hasValues() const {
    return !(this->values.empty());
}

// -----------------------------------------------------------------------------

const unsigned long IntegerParameter::getValueCount() const {
    return this->values.size();
}

// -----------------------------------------------------------------------------

void IntegerParameter::proposeNewValue() {
    // #proposedValue must be set to NaN first.
    this->proposedValue = Integer::NaN();
    
    if (this->isFixed()) {
        return;
        
    } else if (this->isPreset()) {
        Integer::Type presetIndex = this->presetSampler->getLastValueAsInt();
        if (!Integer::isNaN(presetIndex)) {
            if (presetIndex >= 0 && presetIndex < this->presetValues.size()) {
                this->proposedValue = this->presetValues[presetIndex];
            } else {
                throw std::runtime_error(
                        "ERROR: Parameter [" + this->id +
                        "] - Proposed preset index is out of bound (" +
                        std::to_string(presetIndex) + ").");
            }
        }
        
    } else if (this->proposer != nullptr) {
        auto nVals = values.size();
        this->proposedValue =
                this->proposer->proposeNewValue(values[nVals - 1l]);
    }
    
    if (!Integer::isNaN(this->proposedValue)) {
        this->notifyObservers();
    }
}

// -----------------------------------------------------------------------------

void IntegerParameter::recordCurrentValue() {
    if (!isFixed()) {
        if (!Integer::isNaN(this->proposedValue)) {
            this->values.emplace_back(this->proposedValue);
        } else {
            auto currentValCount = values.size();
            values.emplace_back(values[currentValCount - 1]);
        }
    }
}

// -----------------------------------------------------------------------------

void IntegerParameter::rejectNewProposal() {
    Parameter::rejectNewProposal();

    this->proposedValue = Integer::NaN();
    if (!isFixed()) {
        this->notifyObservers();
    }
}

// -----------------------------------------------------------------------------

Json IntegerParameter::toJson() const {
    Json json = Parameter::toJson();
    json[AttrKey::VALUES] = this->values;
    
    if (!this->presetValues.empty()) {
        json[AttrKey::PRESET_VALUES] = this->presetValues;
    }
    
    return json;
}

// -----------------------------------------------------------------------------

Integer::Type IntegerParameter::getLastValueAsInt() const {
    unsigned long nVals = this->values.size();
    
    if (!Integer::isNaN(this->proposedValue)) {
        return this->proposedValue;
    } else if (nVals > 0) {
        return this->values[nVals - 1];
    } else {
        return Integer::NaN();
    }
}

// -----------------------------------------------------------------------------

Decimal::Type IntegerParameter::getLastValueAsDecimal() const {
    Integer::Type lastValue = getLastValueAsInt();
    if (Integer::isNaN(lastValue)) {
        return Decimal::NaN();
    } else {
        return static_cast<Decimal::Type>(lastValue);
    }
}

// -----------------------------------------------------------------------------

Integer::Type
IntegerParameter::getIntValueAt(unsigned long const &index) const {
    if (index < this->values.size()) {
        return this->values.at(index);
    } else {
        return Integer::NaN();
    }
}

// -----------------------------------------------------------------------------

Decimal::Type
IntegerParameter::getDecimalValueAt(unsigned long const &index) const {
    Integer::Type value = getIntValueAt(index);
    if (Integer::isNaN(value)) {
        return Decimal::NaN();
    } else {
        return static_cast<Decimal::Type>(value);
    }
}

// -----------------------------------------------------------------------------

const unsigned long IntegerParameter::getPresetSize() const {
    return this->presetValues.size();
}

// -----------------------------------------------------------------------------

void
IntegerParameter::loadPresetValues(const ParameterPool &parameterPresetPool,
                                   const unsigned long &presetBurnin) {
    if (presetSource.empty() || !presetValues.empty()) {
        return;
    }
    
    Parameter * sourceParam =
            parameterPresetPool.searchParameterById(presetSource);
    
    if (sourceParam != nullptr) {
        if (sourceParam->getType() != Parameter::Type::INTEGER) {
            throw std::runtime_error(
                    "ERROR: Parameter [" + this->getId() +
                    "] - Preset values must be of integral type.");
        }
        
        auto sourceValueCount = sourceParam->getValueCount();
        this->presetValues.clear();
        
        if (sourceParam->isFixed() && sourceValueCount >= 1) {
            this->presetValues.emplace_back(sourceParam->getIntValueAt(0));
            
        } else if (!sourceParam->isFixed() && sourceValueCount > presetBurnin) {
            this->presetValues.reserve(sourceValueCount - presetBurnin);
            for (auto i = presetBurnin; i < sourceValueCount; i++) {
                this->presetValues.emplace_back(sourceParam->getIntValueAt(i));
            }
        }
        
        this->updatePresetSampler();
    }
}

// -----------------------------------------------------------------------------

void IntegerParameter::usePresetValueAsProposal(Integer::Type presetIndex) {
    if (presetIndex >= 0 && presetIndex < this->presetValues.size()) {
        this->proposedValue = this->presetValues[presetIndex];
    } else {
        this->proposedValue = Integer::NaN();
    }
    
}

// -----------------------------------------------------------------------------

void IntegerParameter::setProposal(Decimal::Type const &newProposal) {
    this->proposedValue = Integer::NaN();
    if (!Decimal::isNaN(newProposal)) {
        this->proposedValue =
                static_cast<Integer::Type>(std::round(newProposal));
    }
}

// -----------------------------------------------------------------------------

void IntegerParameter::setProposal(Integer::Type const &newProposal) {
    this->proposedValue = newProposal;
}

// -----------------------------------------------------------------------------

std::vector<Decimal::Type> IntegerParameter::getValues() const {
    std::vector<Decimal::Type> valuesAsDecimals;
    valuesAsDecimals.reserve(this->values.size());
    
    for (auto value : this->values) {
        valuesAsDecimals.emplace_back(static_cast<Decimal::Type>(value));
    }
    
    return valuesAsDecimals;
}

// -----------------------------------------------------------------------------

