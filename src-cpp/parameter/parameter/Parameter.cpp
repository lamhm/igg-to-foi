#include <parameter/proposer/UniformProposer.h>
#include <parameter/prior/EmpiricalPrior.h>
#include "util/format.h"
#include "Parameter.h"
#include "library/stats.h"
#include "IntegerParameter.h"
#include "DecimalParameter.h"


// /////////////////////////////////////////////////////////////////////////////
// STATIC CONSTANTS
// /////////////////////////////////////////////////////////////////////////////

const std::string Parameter::AttrKey::TYPE = "type";
const std::string Parameter::AttrKey::ID = "id";
const std::string Parameter::AttrKey::VALUES = "values";
const std::string Parameter::AttrKey::PROPOSER = "proposer";
const std::string Parameter::AttrKey::PRIOR = "prior";

const std::string Parameter::AttrKey::AUTO_ACCEPT_RATE = "auto_accept_rate";
const std::string Parameter::AttrKey::SAMPLING_FREQUENCY = "sampling_frequency";
const std::string Parameter::AttrKey::ACCEPT_RATE = "accept_rate";

const std::string Parameter::AttrKey::PRESET_SAMPLER = "preset_sampler";
const std::string Parameter::AttrKey::PRESET_SOURCE = "preset_source";
const std::string Parameter::AttrKey::PRESET_VALUES = "preset_values";

const std::string Parameter::TypeName::DECIMAL = "decimal";
const std::string Parameter::TypeName::INTEGER = "integer";


// /////////////////////////////////////////////////////////////////////////////
// STATIC METHODS
// /////////////////////////////////////////////////////////////////////////////

Parameter *Parameter::newFromJson(const Json &json) {
    if (!json.is_object()) {
        throw std::runtime_error("ERROR: Invalid parameter declaration.");
    }

    if (json.element_exists(AttrKey::TYPE)) {
        if (json[AttrKey::TYPE].is_string()) {
            std::string paramType = json[AttrKey::TYPE].get<std::string>();
            // Make the paramType case-insensitive.
            paramType = format::toLowerCase(paramType);
            if (paramType == TypeName::INTEGER) {
                return new IntegerParameter(json);
            }
        }

    }

    /* Create a decimal parameter by default */
    return new DecimalParameter(json);
}


// /////////////////////////////////////////////////////////////////////////////
// METHODS OF PARAMETER CLASS
// /////////////////////////////////////////////////////////////////////////////

Parameter::Parameter(const std::string &id, bool const &isDummy)
        : acceptedTimes(0), rejectedTimes(0),
          id(id), proposer(nullptr), prior(nullptr), dummy(isDummy),
          autoAcceptRate(0.0), presetSampler(nullptr), presetSource(),
          samplingFrequency(1), acceptRates() {
    // Do nothing.
}

// -----------------------------------------------------------------------------

Parameter::Parameter(Json const &json) : Parameter("", false) {

    if (!json.is_object()) {
        throw std::runtime_error("ERROR: Invalid JSON object "
                                 "(expecting a Parameter structure).");
    }

    if (json.element_exists(AttrKey::ID)
        && json[AttrKey::ID].is_string()) {
        this->id = json[AttrKey::ID];
    } else {
        throw std::runtime_error("ERROR: Invalid parameter (no ID).");
    }

    if (json.element_exists(AttrKey::AUTO_ACCEPT_RATE)) {
        Json autoAcceptRateJson = json[AttrKey::AUTO_ACCEPT_RATE];
        if (autoAcceptRateJson.is_number()) {
            this->autoAcceptRate = autoAcceptRateJson.get<Decimal::Type>();
        } else {
            throw std::runtime_error("ERROR: Parameter [" + this->id +
                                     "] - Auto-accept rate must be a number.");
        }
    }

    if (json.element_exists(AttrKey::SAMPLING_FREQUENCY)) {
        Json samplingFreqJson = json[AttrKey::SAMPLING_FREQUENCY];
        this->samplingFrequency = 0;
        if (samplingFreqJson.is_number_integer()) {
            this->samplingFrequency = samplingFreqJson.get<Integer::Type>();
        }
        if (this->samplingFrequency < 1) {
            throw std::runtime_error(
                    "ERROR: Parameter [" + this->id +
                    "] - Sampling frequency must be a positive integer.");
        }
    }

    if (json.element_exists(AttrKey::PRESET_SOURCE)) {
        Json presetSourceJson = json[AttrKey::PRESET_SOURCE];
        if (presetSourceJson.is_string()) {
            this->presetSource = presetSourceJson.get<std::string>();
        } else {
            throw std::runtime_error("ERROR: Parameter -- Invalid preset "
                                     "source (a string is expected).");
        }
    }

    if (json.element_exists(AttrKey::PRESET_SAMPLER)) {
        Json presetSamplerJson = json[AttrKey::PRESET_SAMPLER];
        if (presetSamplerJson.is_string()) {
            std::string presetSamplerId = presetSamplerJson.get<std::string>();
            this->presetSampler = new IntegerParameter(presetSamplerId, true);
        } else {
            throw std::runtime_error("ERROR: Parameter -- Invalid preset "
                                     "sampler ID (a string is expected).");
        }
    }

    if (json.element_exists(AttrKey::PROPOSER)) {
        this->proposer = Proposer::newFromJson(json[AttrKey::PROPOSER]);
    }

    if (json.element_exists(AttrKey::PRIOR)) {
        this->prior = Prior::newFromJson(json[AttrKey::PRIOR]);
    }
}

// -----------------------------------------------------------------------------

Parameter::~Parameter() {
    if (this->proposer != nullptr) {
        delete this->proposer;
        this->proposer = nullptr;
    }
    if (this->prior != nullptr) {
        delete this->prior;
        this->prior = nullptr;
    }
}

// -----------------------------------------------------------------------------

const std::string &Parameter::getId() const {
    return id;
}

// -----------------------------------------------------------------------------

const bool Parameter::hasId() const {
    return !(this->id.empty());
}

// -----------------------------------------------------------------------------

Json Parameter::toJson() const {
    Json json;

    json[AttrKey::TYPE] = this->getTypeName();
    json[AttrKey::ID] = this->id;

    if (this->autoAcceptRate > 0.0) {
        json[AttrKey::AUTO_ACCEPT_RATE] = this->autoAcceptRate;
    }

    if (this->samplingFrequency != 1) {
        json[AttrKey::SAMPLING_FREQUENCY] = this->samplingFrequency;
    }

    if (!this->presetSource.empty()) {
        json[AttrKey::PRESET_SOURCE] = this->presetSource;
    }

    if (this->presetSampler != nullptr && this->presetSampler->hasId()) {
        json[AttrKey::PRESET_SAMPLER] = this->presetSampler->getId();
    }

    if (this->proposer != nullptr) {
        json[AttrKey::PROPOSER] = this->proposer->toJson();
    }

    if (this->prior != nullptr) {
        json[AttrKey::PRIOR] = this->prior->toJson();
    }

    if (!this->acceptRates.empty()) {
        json[AttrKey::ACCEPT_RATE] = this->acceptRates;
    }
//    Decimal::Type currentAcceptRate = getCurrentAcceptRate();
//    if (!Decimal::isNaN(currentAcceptRate)) {
//        json[AttrKey::ACCEPT_RATE] = currentAcceptRate;
//    }

    return json;
}

// -----------------------------------------------------------------------------

const bool Parameter::isFixed() const {
    return (this->proposer == nullptr && this->presetSampler == nullptr);
}

// -----------------------------------------------------------------------------

const bool Parameter::isPreset() const {
    return (this->presetSampler != nullptr);
}

// -----------------------------------------------------------------------------

//void Parameter::putValueAtIteration(unsigned long iteration) {
//    unsigned long getValueCount = this->values.size();
//    if (getValueCount <= 0) {
//        this->proposedValue = Parameter::NA;
//    } else if (this->isFixed() && getValueCount == 1) {
//        this->proposedValue = this->values[0];
//    } else {
//        this->proposedValue = this->values[iteration];
//    }
//}

// -----------------------------------------------------------------------------

Proposer *Parameter::getProposer() const {
    return proposer;
}

// -----------------------------------------------------------------------------

const bool Parameter::isInUse() const {
    return (this->countObservers() > 0);
}

// -----------------------------------------------------------------------------

bool Parameter::isDummy() const {
    return dummy;
}

// -----------------------------------------------------------------------------

void Parameter::linkWithOtherParameters(ParameterPool const &parameterPool) {
    if (this->proposer != nullptr) {
        this->proposer->linkBoundariesToParameters(parameterPool);
    }

    if (this->presetSampler != nullptr && this->presetSampler->hasId() &&
        this->presetSampler->isDummy()) {
        Parameter *linkedPresetSampler =
                parameterPool.searchParameterById(this->presetSampler->getId());

        if (linkedPresetSampler == nullptr) {
            throw std::runtime_error(
                    "ERROR: Parameter - Cannot link to the preset sampler "
                    "(parameter " + this->presetSampler->getId() +
                    " not found).");

        } else {
            if (this->presetSampler->getType() != Parameter::Type::INTEGER) {
                throw std::runtime_error("ERROR: Parameter - Preset sampler "
                                         "must be of integral type.");
            }
            delete this->presetSampler;
            this->presetSampler = linkedPresetSampler;
            linkedPresetSampler->attachObserver(this);
            this->updatePresetSampler();
        }
    }
}

// -----------------------------------------------------------------------------

void Parameter::updatePresetSampler() const {
    if (this->presetSampler != nullptr) {
        auto currentPresetSize =
                static_cast<Integer::Type>(this->getPresetSize());

        if (currentPresetSize > 0
            && this->presetSampler->getProposer() != nullptr) {
            this->presetSampler->getProposer()->setLowBound(
                    new IntegerParameter(0, true));
            this->presetSampler->getProposer()->setUpBound(
                    new IntegerParameter(currentPresetSize - 1, true));
        }
    }
}

// -----------------------------------------------------------------------------

void Parameter::update(const Observable *caller) {
    if (this->isPreset()) {
        Integer::Type presetIndex = this->presetSampler->getLastValueAsInt();

        if (Integer::isNaN(presetIndex)) {
            throw std::runtime_error("ERROR: Parameter [" + this->id +
                                     "] - Preset sampler returned NA.");

        } else {
            if (presetIndex < 0 || presetIndex >= this->getPresetSize()) {
                throw std::runtime_error(
                        "ERROR: Parameter [" + this->id +
                        "] - Proposed preset index is out of bound (" +
                        std::to_string(presetIndex) + ").");

            } else {
                this->usePresetValueAsProposal(presetIndex);
                this->notifyObservers();
            }
        }
    }
}

// -----------------------------------------------------------------------------

Decimal::Type Parameter::getAutoAcceptRate() const {
    return autoAcceptRate;
}

// -----------------------------------------------------------------------------

Decimal::Type Parameter::computeLogPrior() const {
    if (this->prior == nullptr) {
        return 0;
    } else {
        return this->prior->computeLogProb(this->getLastValueAsDecimal());
    }
}

// -----------------------------------------------------------------------------

void Parameter::loadEmpiricalPrior(const ParameterPool &parameterPool,
                                   const unsigned long &priorBurnin) {
    if (this->prior == nullptr ||
        this->prior->getType() != Prior::Type::EMPIRICAL) {
        return;
    }

    auto empiricalPrior = dynamic_cast<EmpiricalPrior *>(this->prior);
    empiricalPrior->loadProbDensityFromSource(parameterPool, priorBurnin);
}

// -----------------------------------------------------------------------------

Integer::Type Parameter::getSamplingFrequency() const {
    return samplingFrequency;
}

// -----------------------------------------------------------------------------

void Parameter::acceptNewProposal() {
    this->acceptedTimes++;
}

// -----------------------------------------------------------------------------

void Parameter::rejectNewProposal() {
    this->rejectedTimes++;
}

// -----------------------------------------------------------------------------

bool Parameter::tuneProposer() {
    if (this->proposer == nullptr) {
        return false;
    }

    Decimal::Type currentAcceptRate = getCurrentAcceptRate();
    if (Decimal::isNaN(currentAcceptRate)) {
        /* Accept rate cannot be calculated at the moment.
         * Simply ask for more tuning next time. */
        return true;
    }

    // Store the current accept rate (for debugging purposes)
    this->acceptRates.push_back(currentAcceptRate);

    if (this->proposer->tune(currentAcceptRate)) {
        /* More tuning may be required; reset the counting for accept rate. */
        acceptedTimes = 0;
        rejectedTimes = 0;
        return true;
    } else {
        /* No more tuning is required */
        return false;
    }
}

// -----------------------------------------------------------------------------

Decimal::Type Parameter::getCurrentAcceptRate() const {
    if (acceptedTimes == 0 && rejectedTimes == 0) {
        return Decimal::NaN();
    }

    Decimal::Type currentAcceptRate =
            static_cast<Decimal::Type>(acceptedTimes) /
            static_cast<Decimal::Type>(acceptedTimes + rejectedTimes);

    return currentAcceptRate;
}

// -----------------------------------------------------------------------------

