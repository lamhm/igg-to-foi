/**
 * Created by Ha Minh Lam on 2018-06-04.
 */

#include "DecimalParameter.h"


// -----------------------------------------------------------------------------

DecimalParameter::DecimalParameter(const std::string &id, bool const &isDummy)
        : Parameter(id, isDummy), values(), proposedValue(Decimal::NaN()) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

DecimalParameter::DecimalParameter(const std::string &id,
                                   const Decimal::Type &initValue)
        : DecimalParameter(id, false) {
    this->values.push_back(initValue);
}

// -----------------------------------------------------------------------------

DecimalParameter::DecimalParameter(const Decimal::Type &value,
                                   bool const &isDummy)
        : DecimalParameter("", isDummy) {
    this->values.push_back(value);
}

// -----------------------------------------------------------------------------

DecimalParameter::DecimalParameter(Json const &json)
        : Parameter(json), values(), proposedValue(Decimal::NaN()) {
    
    if (json.element_exists(AttrKey::VALUES)) {
        Json valuesJson = json[AttrKey::VALUES];
        
        bool error = false;
        if (valuesJson.is_number()) {
            this->values.emplace_back(valuesJson.get<Decimal::Type>());
            
        } else if (valuesJson.is_array()) {
            for (const Json &value : valuesJson) {
                if (value.is_number()) {
                    this->values.emplace_back(value.get<Decimal::Type>());
                } else {
                    error = true;
                }
            }
            
        } else {
            error = true;
        }
        
        if (error) {
            throw std::runtime_error(
                    "ERROR: Invalid parameter value [" + this->id + "].");
        }
    }
    
    if (json.element_exists(AttrKey::PRESET_VALUES)) {
        Json presetValuesJson = json[AttrKey::PRESET_VALUES];
        
        bool error = false;
        if (presetValuesJson.is_number()) {
            this->presetValues.emplace_back(
                    presetValuesJson.get<Decimal::Type>());
            
        } else if (presetValuesJson.is_array()) {
            for (const Json &presetValue : presetValuesJson) {
                if (presetValue.is_number()) {
                    this->presetValues.emplace_back(
                            presetValue.get<Decimal::Type>());
                } else {
                    error = true;
                }
            }
            
        } else {
            error = true;
        }
        
        if (error) {
            throw std::runtime_error(
                    "ERROR: Invalid parameter preset [" + this->id + "].");
        } else {
            this->updatePresetSampler();
        }
    }
}

// -----------------------------------------------------------------------------

const Parameter::Type DecimalParameter::getType() const {
    return Parameter::Type::DECIMAL;
}

// -----------------------------------------------------------------------------

Decimal::Type DecimalParameter::getMeanValue(unsigned long burnin) const {
    auto nVals = values.size();
    
    if (this->isFixed()) {
        if (nVals >= 1) {
            return this->values[0];
        }
        
    } else {
        if (burnin >= nVals) {
            throw std::runtime_error(
                    "ERROR: The number of burn-in values must be less than" +
                    std::to_string(nVals));
        }
        
        Decimal::Type mean = 0.0l;
        for (auto i = burnin; i < nVals; i++) {
            mean += this->values[i];
        }
        mean /= static_cast<long double>(nVals - burnin);
        
        return mean;
    }
    
    return Decimal::NaN();
}

// -----------------------------------------------------------------------------

const bool DecimalParameter::hasValues() const {
    return !(this->values.empty());
}

// -----------------------------------------------------------------------------

const unsigned long DecimalParameter::getValueCount() const {
    return this->values.size();
}

// -----------------------------------------------------------------------------

void DecimalParameter::proposeNewValue() {
    // #proposedValue must be set to NaN first.
    this->proposedValue = Decimal::NaN();
    
    if (this->isFixed()) {
        return;
        
    } else if (this->isPreset()) {
        Integer::Type presetIndex = this->presetSampler->getLastValueAsInt();
        if (!Integer::isNaN(presetIndex)) {
            if (presetIndex >= 0 && presetIndex < this->presetValues.size()) {
                this->proposedValue = this->presetValues[presetIndex];
            } else {
                throw std::runtime_error(
                        "ERROR: Parameter [" + this->id +
                        "] - Proposed preset index is out of bound (" +
                        std::to_string(presetIndex) + ").");
            }
        }
        
    } else if (this->proposer != nullptr) {
        auto nVals = values.size();
        this->proposedValue =
                this->proposer->proposeNewValue(values[nVals - 1l]);
    }
    
    if (!Decimal::isNaN(this->proposedValue)) {
        this->notifyObservers();
    }
}

// -----------------------------------------------------------------------------

void DecimalParameter::recordCurrentValue() {
    if (!isFixed()) {
        if (!Decimal::isNaN(this->proposedValue)) {
            this->values.emplace_back(this->proposedValue);
        } else {
            auto currentValCount = values.size();
            values.emplace_back(values[currentValCount - 1]);
        }
    }
}

// -----------------------------------------------------------------------------

void DecimalParameter::rejectNewProposal() {
    Parameter::rejectNewProposal();

    this->proposedValue = Decimal::NaN();
    if (!isFixed()) {
        this->notifyObservers();
    }
}

// -----------------------------------------------------------------------------

const std::string DecimalParameter::getTypeName() const {
    return TypeName::DECIMAL;
}

// -----------------------------------------------------------------------------

Json DecimalParameter::toJson() const {
    Json json = Parameter::toJson();
    
    json[AttrKey::VALUES] = this->values;
    
    if (!this->presetValues.empty()) {
        json[AttrKey::PRESET_VALUES] = this->presetValues;
    }
    
    return json;
}

// -----------------------------------------------------------------------------

Decimal::Type DecimalParameter::getLastValueAsDecimal() const {
    unsigned long nVals = this->values.size();
    
    if (!Decimal::isNaN(this->proposedValue)) {
        return this->proposedValue;
        
    } else if (nVals > 0) {
        return this->values[nVals - 1];
        
    } else {
        return Decimal::NaN();
    }
}

// -----------------------------------------------------------------------------

long long int DecimalParameter::getLastValueAsInt() const {
    Decimal::Type lastValue = getLastValueAsDecimal();
    if (Decimal::isNaN(lastValue)) {
        return Integer::NaN();
    } else {
        return static_cast<Integer::Type>(std::round(lastValue));
    }
}

// -----------------------------------------------------------------------------

Decimal::Type
DecimalParameter::getDecimalValueAt(unsigned long const &index) const {
    if (index < this->values.size()) {
        return this->values.at(index);
    } else {
        return Decimal::NaN();
    }
}

// -----------------------------------------------------------------------------

Integer::Type
DecimalParameter::getIntValueAt(unsigned long const &index) const {
    Decimal::Type value = getDecimalValueAt(index);
    if (Decimal::isNaN(value)) {
        return Integer::NaN();
    } else {
        return static_cast<Integer::Type>(std::round(value));
    }
}

// -----------------------------------------------------------------------------

const unsigned long DecimalParameter::getPresetSize() const {
    return this->presetValues.size();
}

// -----------------------------------------------------------------------------

void
DecimalParameter::loadPresetValues(const ParameterPool &parameterPresetPool,
                                   const unsigned long &presetBurnin) {
    if (presetSource.empty() || !presetValues.empty()) {
        return;
    }
    
    Parameter * sourceParam =
            parameterPresetPool.searchParameterById(presetSource);
    
    if (sourceParam != nullptr) {
        if (sourceParam->getType() != Parameter::Type::DECIMAL) {
            throw std::runtime_error(
                    "ERROR: Parameter [" + this->getId() +
                    "] - Preset values must be of decimal type.");
        }
        
        auto sourceValueCount = sourceParam->getValueCount();
        this->presetValues.clear();
        
        if (sourceParam->isFixed() && sourceValueCount >= 1) {
            this->presetValues.emplace_back(sourceParam->getDecimalValueAt(0));
            
        } else if (!sourceParam->isFixed() && sourceValueCount > presetBurnin) {
            this->presetValues.reserve(sourceValueCount - presetBurnin);
            for (auto i = presetBurnin; i < sourceValueCount; i++) {
                this->presetValues.emplace_back(
                        sourceParam->getDecimalValueAt(i));
            }
        }
        
        this->updatePresetSampler();
    }
}

// -----------------------------------------------------------------------------

void DecimalParameter::usePresetValueAsProposal(Integer::Type presetIndex) {
    if (presetIndex >= 0 && presetIndex < this->presetValues.size()) {
        this->proposedValue = this->presetValues[presetIndex];
    } else {
        this->proposedValue = Decimal::NaN();
    }
}

// -----------------------------------------------------------------------------

void DecimalParameter::setProposal(Decimal::Type const &newProposal) {
    this->proposedValue = newProposal;
}

// -----------------------------------------------------------------------------

void DecimalParameter::setProposal(Integer::Type const &newProposal) {
    if (Integer::isNaN(newProposal)) {
        this->proposedValue = Decimal::NaN();
    } else {
        this->proposedValue = static_cast<Decimal::Type>(newProposal);
    }
}

// -----------------------------------------------------------------------------

std::vector<Decimal::Type> DecimalParameter::getValues() const {
    return values;
}

// -----------------------------------------------------------------------------


