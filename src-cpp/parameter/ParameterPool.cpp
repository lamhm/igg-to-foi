/**
 * Created by Ha Minh Lam on 2018-01-22.
 */

#include "ParameterPool.h"
#include "parameter/parameter/Parameter.h"


// -----------------------------------------------------------------------------

const std::string ParameterPool::JSON_KEY = "PARAMETERS";

// -----------------------------------------------------------------------------

ParameterPool::ParameterPool(const Json &jsonNode) : paramPtrs() {
    Json rootNode = jsonNode;
    
    if (rootNode.element_exists(ParameterPool::JSON_KEY)) {
        rootNode = rootNode[ParameterPool::JSON_KEY];
    }
    
    if (rootNode.is_array()) {
        for (const Json &childNode : rootNode) {
            Parameter * newParameter = Parameter::newFromJson(childNode);
            this->addParameter(newParameter);
        }
        
    } else {
        Parameter * newParameter = Parameter::newFromJson(rootNode);
        this->addParameter(newParameter);
    }
}

// -----------------------------------------------------------------------------

ParameterPool::~ParameterPool() {
    for (Parameter * paramPtr : this->paramPtrs) {
        if (paramPtr != nullptr) {
            delete paramPtr;
            paramPtr = nullptr;
        }
    }
    this->paramPtrs.clear();
}

// -----------------------------------------------------------------------------

void ParameterPool::addParameter(Parameter * parameter) {
    if (parameter == nullptr) {
        return;
    }
    
    if (this->searchParameterById(parameter->getId()) != nullptr) {
        throw std::runtime_error(
                "ERROR: Cannot add parameters of the same ID into a parameter "
                "pool [ID: " + parameter->getId() + "].");
    }
    
    this->paramPtrs.emplace_back(parameter);
}

// -----------------------------------------------------------------------------

void ParameterPool::deleteUnusedParameters() {
    std::vector<Parameter *> usedParameters;
    
    for (Parameter * parameter : this->paramPtrs) {
        if (parameter != nullptr) {
            if (parameter->isInUse()) {
                usedParameters.emplace_back(parameter);
            } else {
                delete parameter;
            }
        }
    }
    
    this->paramPtrs = usedParameters;
}

// -----------------------------------------------------------------------------

void ParameterPool::linkParameters() const {
    for (Parameter * parameter : this->paramPtrs) {
        parameter->linkWithOtherParameters(*this);
    }
}

// -----------------------------------------------------------------------------

void ParameterPool::loadPresetValues(const ParameterPool &parameterPresetPool,
                                     const unsigned long &presetBurnin) const {
    
    for (Parameter * parameter : this->paramPtrs) {
        parameter->loadPresetValues(parameterPresetPool, presetBurnin);
    }
}

// -----------------------------------------------------------------------------

void ParameterPool::loadPriorDistributions(
        const ParameterPool &parameterPool,
        const unsigned long &priorBurnin) const {
    
    for (Parameter * parameter : this->paramPtrs) {
        parameter->loadEmpiricalPrior(parameterPool, priorBurnin);
    }
}

// -----------------------------------------------------------------------------

Decimal::Type ParameterPool::computeLogPrior() const {
    Decimal::Type logPrior = 0.0;
    
    for (const auto &parameter : this->paramPtrs) {
        if (parameter->isInUse() &&
            !parameter->isFixed() && !parameter->isDummy()) {
            logPrior += parameter->computeLogPrior();
        }
    }
    
    return logPrior;
}

// -----------------------------------------------------------------------------

Parameter *
ParameterPool::searchParameterById(const std::string &parameterId) const {
    for (Parameter * param : this->paramPtrs) {
        if (param != nullptr && param->getId() == parameterId) {
            return param;
        }
    }
    
    return nullptr;
}

// -----------------------------------------------------------------------------

Json ParameterPool::toJson() const {
    Json json = Json::array();
    
    for (Parameter * parameter : this->paramPtrs) {
        if (parameter != nullptr) {
            json.emplace_back(parameter->toJson());
        }
    }
    
    return json;
}

// -----------------------------------------------------------------------------

unsigned long ParameterPool::size() const {
    return this->paramPtrs.size();
}

// -----------------------------------------------------------------------------

std::vector<Parameter *> ParameterPool::getFreeParameters() const {
    std::vector<Parameter *> freeParams;
    for (Parameter * parameter : this->paramPtrs) {
        if (!parameter->isFixed() && !parameter->isPreset() &&
            !parameter->isDummy()) {
            freeParams.emplace_back(parameter);
        }
    }
    return freeParams;
}

// -----------------------------------------------------------------------------

std::vector<Parameter *> ParameterPool::getPresetParameters() const {
    std::vector<Parameter *> presetParams;
    for (Parameter * parameter : this->paramPtrs) {
        if (parameter->isPreset()) {
            presetParams.emplace_back(parameter);
        }
    }
    return presetParams;
}

// -----------------------------------------------------------------------------

//std::vector<Parameter *> ParameterPool::getNonFixedParameters() const {
//    std::vector<Parameter *> nonFixedParams;
//    for (Parameter * parameter : this->paramPtrs) {
//        if (!parameter->isFixed()) {
//            nonFixedParams.emplace_back(parameter);
//        }
//    }
//    return nonFixedParams;
//}

// -----------------------------------------------------------------------------
