/**
 * Created by Ha Minh Lam on 2018-01-22.
 */

#ifndef PARAMETERPOOL_H
#define PARAMETERPOOL_H


#include <framework/Serializable.h>
#include <library/Number.h>


class Parameter;


class ParameterPool : public Serializable {
private:
    
    std::vector<Parameter *> paramPtrs;
    
    void addParameter(Parameter * parameter);

public:
    
    static const std::string JSON_KEY;
    
    explicit ParameterPool(const Json &jsonNode);
    
    virtual ~ParameterPool();
    
    void deleteUnusedParameters();
    
    void linkParameters() const;
    
    void loadPresetValues(const ParameterPool &parameterPresetPool,
                          const unsigned long &presetBurnin) const;
    
    void loadPriorDistributions(const ParameterPool &parameterPool,
                                const unsigned long &priorBurnin) const ;
    
    unsigned long size() const;
    
    
    /**
     * Retrieve a list containing pointers to all non-fixed (free or preset)
     * parameters in this pool.
     * @return A vector containing pointers to the non-fixed parameters.
     */
    //std::vector<Parameter *> getNonFixedParameters() const;
    
    
    /**
     * Retrieve a list containing pointers to all free parameters in this pool.
     * @note A parameter is free if its value can be freely varied during an
     *       MCMC run (meaning it has a Parameter#proposer and the proposer
     *       is not a PresetProposer).
     * @return A vector containing pointers to free parameters.
     */
    std::vector<Parameter *> getFreeParameters() const;
    
    
    /**
     * Retrive a list containing pointer to all preset parameters in this
     * pool. Preset parameters is semi-free, i.e. their values are altered
     * during an MCMC run, but the values are restricted by a preset
     * distribution (often taken from a posterior of a previous run).
     * @return A vector containing pointers to all preset parameters.
     */
    std::vector<Parameter *> getPresetParameters() const;
    
    
    /**
     * Calculate the sum of log prior of all the parameters.
     * @return The sum of log prior of all the parameters.
     * @note This method should only be called after <i>deleteUnusedParams()<i>
     *       has been called. Otherwise, the log prior of unused parameters will
     *       also be included in the final sum.
     */
    Decimal::Type computeLogPrior() const;
    
    Parameter * searchParameterById(const std::string &parameterId) const;
    
    Json toJson() const override;
};


#endif //PARAMETERPOOL_H
