/**
 * Created by Ha Minh Lam on 2018-05-29.
 */

#ifndef DENGUEFOI_NUMBER_H
#define DENGUEFOI_NUMBER_H

#include <limits>


// /////////////////////////////////////////////////////////////////////////////
// DECLARATION
// /////////////////////////////////////////////////////////////////////////////

template<typename NumberType = long double>
class Number {
public:
    using Type = NumberType;
    
    static NumberType const NaN();
    
    static NumberType const smallest();
    
    static NumberType const biggest();
    
    static bool isNaN(NumberType const &number);
};

// -----------------------------------------------------------------------------

using Decimal = Number<long double>;
using Integer = Number<long long>;
using UInt = Number<unsigned long long>;


// /////////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION
// /////////////////////////////////////////////////////////////////////////////

template<typename NumberType>
NumberType const Number<NumberType>::NaN() {
    NumberType _NaN = std::numeric_limits<NumberType>::quiet_NaN();
    if (_NaN == static_cast<NumberType>(0)) {
        return std::numeric_limits<NumberType>::max();
    } else {
        return _NaN;
    }
}

// -----------------------------------------------------------------------------

template<typename NumberType>
bool Number<NumberType>::isNaN(NumberType const &number) {
    // According to the IEEE standard, if number is a floating point and NaN,
    // then (number != number) = TRUE. But compilers may not always obey this.
    return (number == Number<NumberType>::NaN()) ||
           (number != number); // NOLINT
}

// -----------------------------------------------------------------------------

template<typename NumberType>
NumberType const Number<NumberType>::smallest() {
    return std::numeric_limits<NumberType>::lowest();
}

// -----------------------------------------------------------------------------

template<typename NumberType>
NumberType const Number<NumberType>::biggest() {
    NumberType _max = std::numeric_limits<NumberType>::max();
    if (Number<NumberType>::NaN() == _max) {
        return _max - static_cast<NumberType>(1);
    } else {
        return _max;
    }
}

// -----------------------------------------------------------------------------


#endif //DENGUEFOI_NUMBER_H
