/**
 * Created by Ha Minh Lam on 2018-02-06.
 */

#include "RandomEngine.h"


// -----------------------------------------------------------------------------

RandomEngine * RandomEngine::defaultEngine = nullptr;
Decimal::Type const RandomEngine::NORMAL_TRUNCATED_MAX_STD_DEV = 1000.0;

// -----------------------------------------------------------------------------

RandomEngine::RandomEngine(unsigned long seed)
        : seed(seed), randomAlgorithm(seed) {
    // Do nothing
}

// -----------------------------------------------------------------------------

RandomEngine::RandomEngine()
        : RandomEngine(std::random_device{}()) {
    // Do nothing
}

// -----------------------------------------------------------------------------

RandomEngine * RandomEngine::singleton() {
    if (defaultEngine == nullptr) {
        defaultEngine = new RandomEngine();
    }
    return defaultEngine;
}

// -----------------------------------------------------------------------------

unsigned long RandomEngine::getSeed() const {
    return seed;
}

// -----------------------------------------------------------------------------

void RandomEngine::setSeed(unsigned long seed) {
    RandomEngine::randomAlgorithm.seed(seed);
    RandomEngine::seed = seed;
}

// -----------------------------------------------------------------------------
