/**
 * Created by Ha Minh Lam on 2018-02-06.
 */

#ifndef DENGUEFOI_RANDOMENGINE_H
#define DENGUEFOI_RANDOMENGINE_H


#include <random>
#include <algorithm>
#include <stdexcept>
#include "Number.h"


class RandomEngine {
private:
    static Decimal::Type const NORMAL_TRUNCATED_MAX_STD_DEV;

    static RandomEngine * defaultEngine;
    
    std::mt19937_64 randomAlgorithm;
    
    unsigned long seed;


public:
    static RandomEngine * singleton();
    
    explicit RandomEngine(unsigned long seed);
    
    explicit RandomEngine();
    
    unsigned long getSeed() const;
    
    void setSeed(unsigned long seed);
    
    
    /**
     * Produces a random floating-point value, that is uniformly distributed on
     * the open interval (lowBound, upBound)
     * @tparam RealType
     * @param lowBound
     * @param upBound
     * @return A floating-point number.
     */
    template<class RealType = long double>
    RealType uniformReal(RealType lowBound, RealType upBound);
    
    
    /**
     * Produces random integer values i, uniformly distributed on the closed
     * interval [lowBound, upBound].
     * @tparam IntType
     * @param lowBound
     * @param upBound
     * @return An integer number.
     */
    template<class IntType = long>
    IntType uniformInt(IntType lowBound, IntType upBound);
    

    /**
     * Draw a random number from a given Normal(mean, stdDev) distribution.
     * @tparam RealType The returned type of the method (should be a
     *                  floating-point type)
     * @param mean   The mean of the normal distribution.
     * @param stdDev The standard deviation of the normal distribution.
     * @return A random number drawn from the given normal distribution.
     */
    template<class RealType = long double>
    RealType normal(const RealType &mean, const RealType &stdDev);


    /**
     * Draw a random number from a given truncated Normal(mean, stdDev)
     * distribution.
     * @tparam RealType The returned type of the method (should be a
     *                  floating-point type)
     * @param mean   The mean of the parent normal distribution.
     * @param stdDev The standard deviation of the parent normal distribution.
     * @param lowBound The lower bound of the sampling space.
     * @param upBound  The upper bound of the sampling space.
     * @return A random number drawn from the given truncated normal
     *         distribution.
     */
    template<class RealType = long double>
    RealType normalTruncated(
            const RealType &mean, const RealType &stdDev,
            RealType lowBound, RealType upBound);


    /**
     * Draw a random number that from a Gamma(shape, scale) distribution.
     * @tparam RealType The returned type of the method (should be a
     *                  floating-point type)
     * @param shape The shape parameter of the Gamma distribution.
     * @param scale The scale parameter of the Gamma distribution.
     * @return A random number.
     */
    template<class RealType = long double>
    RealType gamma(const RealType &shape, const RealType &scale);
    
    
    /**
    * Draw a real number from a Beta distribution.
    * @tparam RealType
    * @param alpha  The alpha parameter of the distribution.
    * @param beta   The beta parameter of the distribution.
    * @return A number that belongs to the closed interval [0, 1].
    */
    template<class RealType = long double>
    RealType beta(const RealType &alpha, const RealType &beta);
    
    
    /**
     * Shuffle a vector.
     * @tparam AnyType
     * @param data A vector.
     */
    template<class AnyType>
    void shuffle(std::vector<AnyType> &data);
    
    
    /**
     * Sample data from a given vector
     * @tparam AnyType The data type
     * @param n    The number of elements that will be sampled from the original
     *             vector
     * @param data The original vector of data
     * @param withReplacement This boolean parameter indicates whether the
     *                        sampling will be done with replacement or not.
     * @return A new vector containing samples of the original vector
     */
    template<class AnyType>
    std::vector<AnyType> sample(const unsigned long &n,
                                std::vector<AnyType> data,
                                const bool &withReplacement);
};

#endif //DENGUEFOI_RANDOMENGINE_H


// Link to the implementation of the template methods
#include "RandomEngine.inl"
