#ifndef STATS_H
#define STATS_H


#include <random>
#include <cmath>
#include <vector>
#include <limits>
#include <stdexcept>


#define PI_CONST 3.141592653589793238462643383279502884L /* pi constant */


namespace stats {

    template<class RealType = long double>
    RealType uniformDensity(const RealType &x, const RealType &lowBound,
                            const RealType &upBound, const bool &logResult) {

        RealType result = 0.0;
        if (x >= lowBound && x <= upBound && upBound > lowBound) {
            result = 1.0l / (upBound - lowBound);
        }

        if (logResult) {
            result = std::log(result);
        }
        return result;
    }


    template<class RealType = long double>
    RealType normalDensity(const RealType &x, const RealType &mean,
                           const RealType &stdDev, const bool &logResult) {
        RealType xMeanDiff = x - mean;
        RealType expCore = (xMeanDiff * xMeanDiff) / (2.0l * stdDev * stdDev);
        RealType sqrt2PiSigmaSquared = sqrtl(2.0l * PI_CONST) * stdDev;

        if (logResult) {
            return (-std::log(sqrt2PiSigmaSquared) - expCore);

        } else {
            RealType oneOverResult = sqrt2PiSigmaSquared * std::exp(expCore);
            return (1.0l / oneOverResult);
        }
    }


    /**
     * Calculate the area under a given normal curve between -INF and a given
     * x value.
     * @tparam RealType The returned type of the method (should be a
     *                  floating-point type)
     * @param x      A value to which the area is truncated
     * @param mean   The mean of the normal distribution
     * @param stdDev The standard deviation of the normal distribution
     * @return The cumulative probability between -INF and x.
     * @note   The calculation is implemented as described in: <br>
     *         A G Adams, Areas Under the Normal Curve, Algorithm 39,
     *         Computer j., Volume 12, pages 197-198, 1969.
     * @authors  John Burkardt (original C++ code),
     *           Ha Minh Lam (optimised C++ code)
     */
    template<class RealType = long double>
    RealType normalCdf(const RealType &x,
                       const RealType &mean, const RealType &stdDev) {

        const RealType zscore = (x - mean) / stdDev;
        const RealType zscoreAbs = std::abs(zscore);

        const RealType halfZscoreSquared = 0.5 * zscore * zscore;

        const RealType A01 = 0.398942280444;
        const RealType A02 = 0.399903438504;
        const RealType A03 = 5.75885480458;
        const RealType A04 = 29.8213557808;
        const RealType A05 = 2.62433121679;
        const RealType A06 = 48.6959930692;
        const RealType A07 = 5.92885724438;

        const RealType B00 = 0.398942280385;
        const RealType B01 = 3.8052E-08;
        const RealType B02 = 1.00000615302;
        const RealType B03 = 3.98064794E-04;
        const RealType B04 = 1.98615381364;
        const RealType B05 = 0.151679116635;
        const RealType B06 = 5.29330324926;
        const RealType B07 = 4.8385912808;
        const RealType B08 = 15.1508972451;
        const RealType B09 = 0.742380924027;
        const RealType B10 = 30.789933034;
        const RealType B11 = 3.99019417011;

        RealType cumulativeProb;

        if (zscoreAbs <= 1.28) {
            /* |zscore| <= 1.28 */
            cumulativeProb =
                    0.5 - zscoreAbs *
                          (A01 - A02 * halfZscoreSquared /
                                 (halfZscoreSquared + A03 -
                                  A04 / (halfZscoreSquared + A05 +
                                         A06 / (halfZscoreSquared + A07))));

        } else if (zscoreAbs <= 12.7) {
            /* 1.28 < |X| <= 12.7 */
            cumulativeProb =
                    std::exp(-halfZscoreSquared) * B00 /
                    (zscoreAbs - B01 +
                     B02 / (zscoreAbs + B03 +
                            B04 / (zscoreAbs - B05
                                   + B06 / (zscoreAbs + B07 -
                                            B08 / (zscoreAbs + B09 +
                                                   B10 / (zscoreAbs + B11))))));

        } else {
            /* 12.7 < |zscore| */
            cumulativeProb = 0.0;
        }

        if (zscore < 0.0) {
            return cumulativeProb;
        } else {
            return static_cast<RealType>(1.0) - cumulativeProb;
        }

    }


    /**
     * Evaluate a polynomial (using Horner's method).
     * @tparam RealType  The returned type of the method (should be a
     *                   floating-point type)
     * @param degree  The degree of the polynomial.
     * @param coefficients  The list of coefficients of the polynomial, arranged
     *                      from the lowest (0) to the highest degree of x.
     * @param x The main variable of the polynomial.S
     * @return  The result of the polynomial.
     */
    template<class RealType = long double>
    RealType evaluatePolynomial(const long &degree,
                                const RealType coefficients[],
                                const RealType &x) {
        RealType result;

        result = coefficients[degree];

        for (long coefficientIdx = degree - 1;
             0 <= coefficientIdx; coefficientIdx--) {
            result = result * x + coefficients[coefficientIdx];
        }

        return result;
    }


    /**
     * Calculate the value of a normally distributed random variable, x, given
     * the cumulative probability of the distribution at x.
     * @tparam RealType The returned type of the method (should be a
     *                  floating-point type)
     * @param cdf    The cumulative probability of the normal distribution
     *               between -INF and x.
     * @param mean   The mean of the normal distribution.
     * @param stdDev The standard deviation of the distribution.
     * @return  The value of the random variable x.
     * @note The calculation is implemented as described in: <br>
     *       Michael Wichura,
     *       The Percentage Points of the Normal Distribution, Algorithm AS 241,
     *       Applied Statistics, Volume 37, Number 3, pages 477-484, 1988.
     * @authors Michael Wichura (original implementation in FORTRAN77),
     *          John Burkardt (original C++ code),
     *          Ha Minh Lam (optimised C++ code)
     */
    template<class RealType = long double>
    RealType normalCdfInvert(const RealType &cdf,
                             const RealType &mean, const RealType &stdDev) {

        if (cdf < 0.0 || cdf > 1.0) {
            throw std::logic_error(
                    "ERROR: Cumulative probabilities must be between 0 and 1.");
        }

        const RealType polyA[8] = {
                3.3871328727963666080, 1.3314166789178437745E+2,
                1.9715909503065514427E+3, 1.3731693765509461125E+4,
                4.5921953931549871457E+4, 6.7265770927008700853E+4,
                3.3430575583588128105E+4, 2.5090809287301226727E+3};
        const RealType polyB[8] = {
                1.0, 4.2313330701600911252E+1,
                6.8718700749205790830E+2, 5.3941960214247511077E+3,
                2.1213794301586595867E+4, 3.9307895800092710610E+4,
                2.8729085735721942674E+4, 5.2264952788528545610E+3};
        const RealType polyC[8] = {
                1.42343711074968357734, 4.63033784615654529590,
                5.76949722146069140550, 3.64784832476320460504,
                1.27045825245236838258, 2.41780725177450611770E-1,
                2.27238449892691845833E-2, 7.74545014278341407640E-4};
        const RealType polyD[8] = {
                1.0, 2.05319162663775882187,
                1.67638483018380384940, 6.89767334985100004550E-1,
                1.48103976427480074590E-1, 1.51986665636164571966E-2,
                5.47593808499534494600E-4, 1.05075007164441684324E-9};
        const RealType polyE[8] = {
                6.65790464350110377720, 5.46378491116411436990,
                1.78482653991729133580, 2.96560571828504891230E-1,
                2.65321895265761230930E-2, 1.24266094738807843860E-3,
                2.71155556874348757815E-5, 2.01033439929228813265E-7};
        const RealType polyF[8] = {
                1.0, 5.99832206555887937690E-1,
                1.36929880922735805310E-1, 1.48753612908506148525E-2,
                7.86869131145613259100E-4, 1.84631831751005468180E-5,
                1.42151175831644588870E-7, 2.04426310338993978564E-15};

        const RealType const1 = 0.180625;
        const RealType const2 = 1.6;

        const RealType split1 = 0.425;
        const RealType split2 = 5.0;

        if (cdf <= 0.0) {
            return -std::numeric_limits<RealType>::infinity();

        } else if (cdf >= 1.0) {
            return std::numeric_limits<RealType>::infinity();

        } else {
            RealType probDiffFromMean = cdf - 0.5;
            RealType zscore;

            if (std::abs(probDiffFromMean) <= split1) {
                RealType r = const1 - probDiffFromMean * probDiffFromMean;
                zscore = probDiffFromMean *
                         evaluatePolynomial(7, polyA, r) /
                         evaluatePolynomial(7, polyB, r);

            } else {
                RealType r = cdf;
                if (probDiffFromMean > 0.0) {
                    r = 1.0 - cdf;
                }

                r = std::sqrt(-std::log(r));

                if (r <= split2) {
                    r = r - const2;
                    zscore = evaluatePolynomial(7, polyC, r) /
                             evaluatePolynomial(7, polyD, r);

                } else {
                    r = r - split2;
                    zscore = evaluatePolynomial(7, polyE, r) /
                             evaluatePolynomial(7, polyF, r);
                }

                if (probDiffFromMean < 0.0) {
                    zscore = -zscore;
                }
            }

            return mean + stdDev * zscore;
        }
    }


    template<class RealType = long double>
    RealType betaDensity(const RealType &x, const RealType &shapeA,
                         const RealType &shapeB, const bool &logResult) {

        if (shapeA <= 0.0l || shapeB <= 0.0) {
            return std::numeric_limits<RealType>::quiet_NaN();
        }

        if (logResult) {
            if (x < 0.0l || x > 1.0l) {
                return -std::numeric_limits<RealType>::infinity();
            }
            return (shapeA - 1.0l) * std::log(x) +
                   (shapeB - 1.0l) * std::log1p(-x)
                   + std::lgamma(shapeA + shapeB) - std::lgamma(shapeA) -
                   std::lgamma(shapeB);

        } else {
            if (x < 0.0l || x > 1.0l) {
                return 0.0l;
            }
            RealType oneOverBetaFunc = std::tgamma<RealType>(shapeA + shapeB) /
                                       (std::tgamma<RealType>(shapeA) *
                                        std::tgamma<RealType>(shapeB));
            return std::pow(x, shapeA - 1.0l) *
                   std::pow(1.0l - x, shapeB - 1.0l) * oneOverBetaFunc;
        }
    }


    template<class RealType = long double>
    std::pair<RealType, RealType> computeBetaMeanNVarFromShapes(
            const RealType &distShapeA, const RealType &distShapeB) {

        std::pair<RealType, RealType> meanNVar;
        RealType sumDistShapes = distShapeA + distShapeB;
        meanNVar.first = distShapeA / sumDistShapes;
        meanNVar.second = (meanNVar.first * distShapeB)
                          / (sumDistShapes * (sumDistShapes + 1));
        return meanNVar;
    }


    template<class RealType = long double>
    std::pair<RealType, RealType> computeBetaShapesFromMeanNVar(
            const RealType &distMean, const RealType &distVar) {

        std::pair<RealType, RealType> shapes;
        RealType meanSqr = distMean * distMean;
        RealType oneSubMean = 1.0l - distMean;
        shapes.first = (oneSubMean * meanSqr / distVar) - distMean;
        shapes.second = shapes.first * oneSubMean / distMean;
        return shapes;
    }


    template<class RealType = long double>
    RealType gammaDensity(const RealType &x, const RealType &shape,
                          const RealType &rate, const bool &logResult) {

        if (shape <= 0.0l || rate <= 0.0) {
            return std::numeric_limits<RealType>::quiet_NaN();
        }

        if (logResult) {
            if (x < 0.0l) {
                return -std::numeric_limits<RealType>::infinity();
            }
            return shape * std::log(rate) + (shape - 1.0l) * std::log(x)
                   - rate * x - std::lgamma(shape);

        } else {
            if (x < 0.0l) {
                return 0.0l;
            }
            return std::pow(rate, shape) * std::pow(x, shape - 1.0l)
                   / (std::exp(rate * x) * std::tgamma(shape));
        }
    }


    /**
     * Calculate the shape and the rate parameters of the Gamma distribution,
     * given its mean and variance.
     * @tparam RealType The default type is long double.
     * @param distMean  The mean of the Gamma distribution.
     * @param distVar   The variance of the Gamma distribution.
     * @return
     */
    template<class RealType = long double>
    std::pair<RealType, RealType> computeGammaShapeNRateFromMeanNVar(
            const RealType &distMean, const RealType &distVar) {

        std::pair<RealType, RealType> shapeNRate;
        shapeNRate.second =
                distMean / distVar;              // rate  = mean / var
        shapeNRate.first =
                distMean * shapeNRate.second;    // shape = mean * rate
        return shapeNRate;
    }


    template<class RealType = long double>
    std::pair<RealType, RealType> computeGammaMeanNVarFromShapeNRate(
            const RealType &distShape, const RealType &distRate) {

        std::pair<RealType, RealType> meanNVar;
        meanNVar.first = distShape / distRate;         // mean = shape / rate
        meanNVar.second = meanNVar.first / distRate;    // var  = mean / rate
        return meanNVar;
    }


    /**
     * Calculate the probability Pr(k) and (1 - sum[Pr(k)]) with
     * k ~ Poisson(k; rate) and k = [0 : (maxK - 1)]
     * @param rate  The rate parameter of the Poisson distribution.
     * @param maxK  The upper bound value of k in Pr(k).
     * @param resultStorage  The vector that will be used to store the result.
     *                       After executing this method, the vector will have
     *                       (maxK + 1) elements, each of which represents Pr(k)
     *                       with k = [0 : (maxK - 1)]. The last number of the
     *                       vector represents (1 - sum[Pr(k)]) for
     *                       k = [0 : (maxK - 1)].
     */
    inline void poissonDensity(const long double &rate,
                               const unsigned long &maxK,
                               std::vector<long double> &resultStorage) {
        resultStorage.resize(maxK + 1);

        // Calculate: (rate ^ k) / k!
        //   + When k = 0, the result is 1
        resultStorage[0] = 1.0l;
        //   + When k = [1 : (maxK - 1)]
        for (long k = 1; k < maxK; k++) {
            resultStorage[k] = resultStorage[k - 1] * rate / k;
        }

        // Calculate P(k) = (rate ^ k) * exp(-rate) / k!
        //                = [(rate ^ k) / k!]  /  exp(rate)
        // The loop must run from k = 0 to (maxK - 1).
        long double sumProb = 0.0l;
        for (long k = 0; k < maxK; k++) {
            resultStorage[k] /= std::exp(rate);
            sumProb += resultStorage[k];
        }
        resultStorage[maxK] = 1.0l - sumProb;
    }

}


#endif /* STATS_H */

