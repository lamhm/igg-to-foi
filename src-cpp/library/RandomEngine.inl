/**
 * Created by Ha Minh Lam on 2018-08-24.
 */

#ifndef DENGUEFOI_RANDOMENGINE_INL
#define DENGUEFOI_RANDOMENGINE_INL


#include "RandomEngine.h"
#include "stats.h"


// -----------------------------------------------------------------------------

template<class IntType>
IntType RandomEngine::uniformInt(IntType lowBound, IntType upBound) {
    if (lowBound > upBound) {
        IntType temp = lowBound;
        lowBound = upBound;
        upBound = temp;
    } else if (lowBound == upBound) {
        return lowBound;
    }

    std::uniform_int_distribution<IntType> uniformDist(lowBound, upBound);

    return uniformDist(RandomEngine::randomAlgorithm);
}

// -----------------------------------------------------------------------------

template<class RealType>
RealType RandomEngine::uniformReal(RealType lowBound, RealType upBound) {
    if (lowBound > upBound) {
        RealType temp = lowBound;
        lowBound = upBound;
        upBound = temp;
    } else if (lowBound == upBound) {
        throw std::logic_error("SAMPLING ERROR: Lower bound must be"
                               " smaller than upper bound.");
    }

    std::uniform_real_distribution<RealType> uniformDist(lowBound, upBound);
    RealType randNumber = uniformDist(RandomEngine::randomAlgorithm);

    // uniformDist(randEngine) generates values in [lowBound, upBound)
    // but we want the interval to be open on both sides, i.e. (lowBound, upBound)
    while (randNumber == lowBound || randNumber == upBound) {
        randNumber = uniformDist(RandomEngine::randomAlgorithm);
    }

    return randNumber;
}

// -----------------------------------------------------------------------------

template<class RealType>
RealType RandomEngine::normal(const RealType &mean, const RealType &stdDev) {
    std::normal_distribution<RealType> normalDist(mean, stdDev);
    return normalDist(RandomEngine::randomAlgorithm);
}

// -----------------------------------------------------------------------------

template<class RealType>
RealType RandomEngine::normalTruncated(
        const RealType &mean, const RealType &stdDev,
        RealType lowBound, RealType upBound) {

    if (lowBound > upBound) {
        RealType temp = lowBound;
        lowBound = upBound;
        upBound = temp;
    }
    if (lowBound == upBound) {
        throw std::logic_error("SAMPLING ERROR: Lower bound and upper bound "
                               "must receive different values.");
    }
    if (lowBound > mean || upBound < mean) {
        throw std::logic_error("SAMPLING ERROR: The mean of the distribution "
                               "must be in between the truncating boundaries.");
    }

    auto samplingRange = upBound - lowBound;
    if (stdDev / samplingRange > NORMAL_TRUNCATED_MAX_STD_DEV) {
        // If the standard deviation is too large,
        // simply sample from a uniform distribution.
        return uniformReal<RealType>(lowBound, upBound);
    }

    RealType lowBoundCdf =
            stats::normalCdf<RealType>(lowBound, mean, stdDev);
    RealType upBoundCdf =
            stats::normalCdf<RealType>(upBound, mean, stdDev);

    RealType sample;
    do {
        /* This piece of code is put in a do-while loop to make sure that the
         * generated number is not out of the given bounds (occurring due to
         * loss of precision in arithmetic operations). */
        RealType randUniformNumber = uniformReal<RealType>(0.0, 1.0);
        RealType randomCdf =
                lowBoundCdf + randUniformNumber * (upBoundCdf - lowBoundCdf);

        sample = stats::normalCdfInvert(randomCdf, mean, stdDev);
    } while (sample < lowBound || sample > upBound);

    return sample;
}

// -----------------------------------------------------------------------------

template<class RealType>
RealType RandomEngine::gamma(const RealType &shape, const RealType &scale) {
    std::gamma_distribution<RealType> gammaDist(shape, scale);
    return gammaDist(RandomEngine::randomAlgorithm);
}

// -----------------------------------------------------------------------------

template<class RealType>
RealType RandomEngine::beta(const RealType &alpha, const RealType &beta) {
    const RealType X = gamma<RealType>(alpha, 1.0);
    const RealType Y = gamma<RealType>(beta, 1.0l);
    return X / (X + Y);
}

// -----------------------------------------------------------------------------

template<class AnyType>
void RandomEngine::shuffle(std::vector<AnyType> &data) {
    std::shuffle(data.begin(), data.end(), randomAlgorithm);
}

// -----------------------------------------------------------------------------

template<class AnyType>
std::vector<AnyType> RandomEngine::sample(const unsigned long &n,
                                          std::vector<AnyType> data,
                                          const bool &withReplacement) {
    if (data.empty()) {
        throw std::runtime_error(
                "ERROR: The original data vector must not be empty");
    }

    auto dataSize = data.size();
    auto maxIndex = dataSize - 1;

    if (withReplacement) {
        // Sampling with replacement
        if (maxIndex == 0) {
            std::vector<AnyType> results(n, data[0]);
            return results;
        } else {
            std::vector<AnyType> results;
            results.reserve(n);
            for (unsigned long i = 0; i < n; i++) {
                unsigned long randIndex;
                randIndex = uniformInt<unsigned long>(0, maxIndex);
                results.emplace_back(data[randIndex]);
            }
            return results;
        }

    } else {
        // Sampling without replacement
        if (n > dataSize) {
            throw std::runtime_error(
                    "ERROR: Sampling without replacement -- N must not be "
                    "greater than the length of the input vector");
        } else {
            shuffle(data);
            if (n < dataSize) {
                data.resize(n);
            }
            return data;
        }
    }
}

#endif // DENGUEFOI_RANDOMENGINE_INL