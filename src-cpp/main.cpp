#include "cli/cli.h"
#include "util/util.h"
#include "mcmc/MarkovChain.h"
#include "library/csv.h"


// -----------------------------------------------------------------------------

//const std::string MESSAGE_SEPARATOR = "========================================"
//                                      "==============================\n";

const int LINE_CUT_THRESHOLD = 30;

// -----------------------------------------------------------------------------

/**
 * Create and initialize an MCMC.
 * @param paramPoolFilePath      The path to the json file containing model
 *                               parameters.
 * @param populationPoolFilePath The path to the json file containing model
 *                               populations.
 * @return A newly created MCMC.
 */
MarkovChain initMarkovChain(const std::string &paramPoolFilePath,
                            const std::string &populationPoolFilePath,
                            const std::string &paramPresetFilePath,
                            const unsigned long &presetBurnin,
                            const std::string &empiricalPriorFilePath,
                            const unsigned long &priorBurnin) {
    
    std::ifstream paramPoolFile(paramPoolFilePath);
    std::ifstream populationPoolFile(populationPoolFilePath);
    Json paramPoolJson;
    Json populationPoolJson;
    paramPoolFile >> paramPoolJson;
    populationPoolFile >> populationPoolJson;
    
    util::outputStream << "Model populations loaded from :";
    if (populationPoolFilePath.size() > LINE_CUT_THRESHOLD) {
        util::outputStream << "\n  ";
    }
    util::outputStream << "  [ " << populationPoolFilePath << " ]\n\n";
    
    util::outputStream << "Model parameters loaded from  :";
    if (paramPoolFilePath.size() > LINE_CUT_THRESHOLD) {
        util::outputStream << "\n  ";
    }
    util::outputStream << "  [ " << paramPoolFilePath << " ]\n\n";
    
    MarkovChain markovChain(paramPoolJson, populationPoolJson);
    
    if (!paramPresetFilePath.empty()) {
        util::outputStream << "Param. preset values loaded from :";
        if (paramPresetFilePath.size() > LINE_CUT_THRESHOLD) {
            util::outputStream << "\n  ";
        }
        util::outputStream << "  [ " << paramPresetFilePath << " ]\n\n";
        
        util::outputStream << "Preset burn-in length:  " << presetBurnin
                           << "\n\n";
        
        std::ifstream paramPresetFile(paramPresetFilePath);
        Json paramPresetJson;
        paramPresetFile >> paramPresetJson;
        markovChain.loadParameterPresets(paramPresetJson, presetBurnin);
    }
    
    if (!empiricalPriorFilePath.empty()) {
        util::outputStream << "Empirical priors loaded from     :";
        if (empiricalPriorFilePath.size() > LINE_CUT_THRESHOLD) {
            util::outputStream << "\n  ";
        }
        util::outputStream << "  [ " << empiricalPriorFilePath << " ]\n\n";
        
        util::outputStream << "Prior burn-in length:  " << priorBurnin
                           << "\n\n";
        
        std::ifstream empiricalPriorFile(empiricalPriorFilePath);
        Json priorParamPoolJson;
        empiricalPriorFile >> priorParamPoolJson;
        markovChain.loadEmpiricalPriorDistributions(priorParamPoolJson,
                                                    priorBurnin);
    }
    
    return markovChain;
}

// -----------------------------------------------------------------------------

void addDataToMarkovChain(MarkovChain &markovChain,
                          const std::string &dataFilePath) {
    csv::CSVReader<3> dataFile(dataFilePath);
    dataFile.read_header(csv::ignore_extra_column,
                         "Population", "Age", "PanbioUnit");
    std::string populationId;
    long double age;
    long double titre;
    while (dataFile.read_row(populationId, age, titre)) {
        markovChain.addSample(populationId, age, titre);
    }
    
    util::outputStream << "Samples loaded from  :";
    if (dataFilePath.size() > LINE_CUT_THRESHOLD) {
        util::outputStream << "\n  ";
    }
    util::outputStream << "  [ " << dataFilePath << " ]\n\n";
}

// -----------------------------------------------------------------------------

void saveMarkovChain(const MarkovChain &markovChain,
                     const std::string &outputFilePath) {
    
    std::ofstream mcmcOutputFile(outputFilePath);
    Json mcmcOutputJson = markovChain.toJson();
    mcmcOutputFile << std::setw(4) << mcmcOutputJson << std::endl;
    
    util::outputStream << "MCMC output saved as :";
    if (outputFilePath.size() > LINE_CUT_THRESHOLD) {
        util::outputStream << "\n  ";
    }
    util::outputStream << "  [ " << outputFilePath << " ]\n\n";
}

// -----------------------------------------------------------------------------

int main(int argCount, char * argVector[]) {
    util::outputStream << std::endl;
    
    cli::App app("InfEst -- Estimating the force of infection of diseases from "
                 "antibody titres.");
    //app.require_subcommand(1);
    //app.set_help_all_flag("--help-all", "Expand all help.");
    
    //cli::App * mcmcCommand =
    //        app.add_subcommand("mcmc",
    //                           "Run an MCMC analysis to estimate the forces of "
    //                           "infection and the titre distributions.");
    
    
    std::string paramPoolFile;
    app.add_option("--param,--parameters", paramPoolFile,
                   "A json file containing the declaration of all the "
                   "parameters that will be used in the model.")
            ->required()
            ->check(cli::ExistingFile);
    
    std::string populationPoolFile;
    app.add_option("--pop,--populations", populationPoolFile,
                   "A json file containing the declaration of all the "
                   "populations that will be analysed in the MCMC run. "
                   "If this file is not specified, the program will look "
                   "into the parameter file (specified by the "
                   "--param option) for population declaration.")
            ->check(cli::ExistingFile);
    
    std::string paramPresetFile;
    auto paramPresetFileOpt =
            app.add_option("--preset,--param-preset", paramPresetFile,
                           "A json file, structured similarly to the parameter "
                           "file (specified by the --param option), that contains "
                           "the preset values of all the preset parameters "
                           "(declared in the parameter file).")
                    ->check(cli::ExistingFile);
    
    unsigned long presetBurnin = 0;
    app.add_option("--preset-burnin", presetBurnin,
                   "The number of first values that will be ignored "
                   "when loading the preset values into the preset variable.",
                   true)
            ->needs(paramPresetFileOpt)
            ->check(cli::Range(0l, std::numeric_limits<long>::max()));
    
    
    std::string empiricalPriorDistFile;
    auto empiricalPriorDistFileOpt =
            app.add_option("--prior", empiricalPriorDistFile,
                           "A json file, structured similarly to the parameter "
                           "file (specified by the --param option), that contains "
                           "empirical prior distributions.")
                    ->check(cli::ExistingFile);
    
    unsigned long priorBurnin = 0;
    app.add_option("--prior-burnin", priorBurnin,
                   "The number of first values that will be ignored  when "
                   "loading the empirical prior distributions from the prior file.",
                   true)
            ->needs(empiricalPriorDistFileOpt)
            ->check(cli::Range(0l, std::numeric_limits<long>::max()));
    
    
    std::string sampleDataFile;
    app.add_option("-s,--sample", sampleDataFile,
                   "A csv file containing the sample data. This csv file "
                   "must have 3 columns: \"Population\", \"Age\", and "
                   "\"PanbioUnit\". Other columns will be ignored.")
            ->required()
            ->check(cli::ExistingFile);
    
    std::string outputFile;
    app.add_option("-o,--output", outputFile,
                   "A file path where the output of the MCMC will be "
                   "stored.")
            ->required()
            ->check(cli::NonexistentPath);
    
    unsigned long chainLength = 0;
    app.add_option("-l,--length", chainLength,
                   "The chain length of the MCMC. If this is set at 0, "
                   "the MCMC analysis will not be run, but a few "
                   "descriptive statistics about the populations and the "
                   "samples will still be printed out.", true)
            ->check(cli::Range(0l, std::numeric_limits<long>::max()));
    
    unsigned long burnin = 0;
    app.add_option("-b,--burnin", burnin,
                   "The number of iterations that will be used for "
                   "burnin. Changing the burnin will only affects the "
                   "calculation of the WBIC.", true)
            ->check(cli::Range(0l, std::numeric_limits<long>::max()));
    
    
    CLI11_PARSE(app, argCount, argVector);
    
    
    //if (mcmcCommand->parsed()) {
    //}
    
    if (populationPoolFile.empty()) {
        populationPoolFile = paramPoolFile;
    }
    
    MarkovChain markovChain = initMarkovChain(
            paramPoolFile, populationPoolFile,
            paramPresetFile, presetBurnin,
            empiricalPriorDistFile, priorBurnin);
    
    addDataToMarkovChain(markovChain, sampleDataFile);
    markovChain.run(chainLength, burnin);
    
    saveMarkovChain(markovChain, outputFile);
    
    
    return 0;
}

// -----------------------------------------------------------------------------
