/**
 * Created by Ha Minh Lam on 2018-01-24.
 */

#include <limits>
#include "util.h"

#ifdef RCPP_BUILD
#include <Rcpp.h>
#endif /* RCPP_BUILD */


#ifdef RCPP_BUILD
std::ostream &utill::outputStream = Rcpp::Rcout;
#else
std::ostream &util::outputStream = std::cout;
#endif /* RCPP_BUILD */


// -----------------------------------------------------------------------------

bool util::strEqualIgnoreCase(const std::string &strA,
                              const std::string &strB) {
    if (strA.length() != strB.length()) {
        return false;
    }
    auto strLen = strA.length();
    for (unsigned long i = 0; i < strLen; i++) {
        if (std::tolower(strA[i]) != std::tolower(strB[i])) {
            return false;
        }
    }
    return true;
}

// -----------------------------------------------------------------------------

util::Progress::Progress(std::string message, const long &totalIterations,
                         const long &updateRateInSeconds)
        : message(std::move(message)),
          totalIterations(totalIterations),
          dTotalIterations(static_cast<long double>(this->totalIterations)),
          startTime(time(nullptr)) /* Get the current time */,
          updateRateInSeconds(updateRateInSeconds) {
    
    this->lastUpdateTime = std::numeric_limits<long>::min();
    
    this->printingPrecision = 0;
    long precisionThresholds[] = {50000, 500000, 5000000};
    while (this->totalIterations >= precisionThresholds[this->printingPrecision]
           && this->printingPrecision < 3) {
        this->printingPrecision++;
    }
}

// -----------------------------------------------------------------------------

long util::Progress::getElapsedTimeInSeconds() const {
    time_t curTime = time(nullptr);
    return static_cast<long>( difftime(curTime, this->startTime) );
}

// -----------------------------------------------------------------------------

void util::Progress::print(const long &currentIteration,
                           const bool &printOnNewLine,
                           const bool &forcePrint) {
    
    long elapsedTimeInSeconds = getElapsedTimeInSeconds();
    
    if (!forcePrint &&
        (elapsedTimeInSeconds - lastUpdateTime) < updateRateInSeconds) {
        return;
    }
    
    long double progress = (static_cast<long double>(currentIteration) * 100.0l)
                           / this->dTotalIterations;
    
    if (printOnNewLine) {
        outputStream << "\n";
    } else {
        outputStream << "\r";
    }
    
    outputStream << "[" << format::formatTime(elapsedTimeInSeconds)
                 << "] " << message
                 << format::formatNumber(progress, 3, printingPrecision) << "%"
                 << std::flush;
    
    this->lastUpdateTime = elapsedTimeInSeconds;
}

// -----------------------------------------------------------------------------
