/**
 * Created by Ha Minh Lam on 2018-01-12.
 */

#ifndef DENGUEELISA_DATA_H
#define DENGUEELISA_DATA_H

//#include <Rcpp.h>
#include <vector>
#include "library/RandomEngine.h"


namespace data {
    
    template<class AnyType>
    void swap(AnyType &refA, AnyType &refB) {
        AnyType valA = refA;
        refA = refB;
        refB = valA;
    }
    
    // -------------------------------------------------------------------------
    
    //template<class NumberType = long double>
    //std::vector<NumberType> toVector(const Rcpp::NumericVector &rVector) {
    //    long size = rVector.size();
    //    std::vector<NumberType> cVector;
    //    cVector.reserve(size);
    //    for (long i = 0l; i < size; i++) {
    //        cVector.push_back(rVector[i]);
    //    }
    //    return cVector;
    //}
    
    // -------------------------------------------------------------------------
    
    template<class NumberType = long double>
    std::vector<NumberType>
    sumVectors(const std::vector<NumberType> &firstVector,
               const std::vector<NumberType> &secondVector) {
        
        unsigned long size = firstVector.size();
        if (size != secondVector.size()) {
            throw std::runtime_error(
                    "ERROR: Cannot sum vectors of different sizes.");
        }
        
        std::vector<NumberType> sum;
        sum.reserve(size);
        for (unsigned long i = 0; i < size; i++) {
            sum.push_back(firstVector[i] + secondVector[i]);
        }
        
        return sum;
    }
    
    // -------------------------------------------------------------------------
    
    inline long double
    sumAllElements(const std::vector<long double> &numberVector) {
        long double sum = 0.0l;
        for (const long double &number : numberVector) {
            sum += number;
        }
        return sum;
    }
    
    // -------------------------------------------------------------------------
    
    template<class NumberType = long double>
    std::vector<NumberType>
    multiplyVectors(const std::vector<NumberType> &firstVector,
                    const std::vector<NumberType> &secondVector) {
        
        unsigned long size = firstVector.size();
        if (size != secondVector.size()) {
            throw std::runtime_error(
                    "ERROR: Cannot multiply vectors of different sizes.");
        }
        
        std::vector<NumberType> product;
        product.reserve(size);
        for (unsigned long i = 0; i < size; i++) {
            product.push_back(firstVector[i] * secondVector[i]);
        }
        
        return product;
    }
    
}

#endif //DENGUEELISA_DATA_H
