/**
 * Created by Ha Minh Lam on 2018-01-16.
 */

#include "transmission.h"
#include <cmath>


long double
transmission::convertAtkRateToInfectionFreq(const long double &attackRate) {
    return -logl(1.0l - attackRate);
}

// -----------------------------------------------------------------------------

//DoubleList transmission::convertAtkRateToInfectionFreq(
//        const DoubleList &attackRates) {
//
//    long nAttackRates = attackRates.size();
//    DoubleList results(nAttackRates);
//
//    for (long i = 0l; i < nAttackRates; i++) {
//        results[i] = convertAtkRateToInfectionFreq(attackRates[i]);
//    }
//
//    return results;
//}

// -----------------------------------------------------------------------------

long double
transmission::convertInfectionFreqToAtkRate(const long double &infectionFreq) {
    return -expm1l(-infectionFreq);     // = 1 - exp(-infectionFreq)
}

// -----------------------------------------------------------------------------

//DoubleList transmission::convertInfectionFreqToAtkRate(
//        const DoubleList &infectionFreqs) {
//
//    long nInfectionFreqs = infectionFreqs.size();
//    DoubleList results(nInfectionFreqs);
//
//    for (long i = 0l; i < nInfectionFreqs; i++) {
//        results[i] = convertInfectionFreqToAtkRate(infectionFreqs[i]);
//    }
//
//    return results;
//}
