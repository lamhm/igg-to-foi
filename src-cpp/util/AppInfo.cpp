/**
 * Created by Ha Minh Lam on 2017-01-19.
 */


#include "AppInfo.h"
#include <sstream>


// /////////////////////////////////////////////////////////////////////////////
// Set static constants
// /////////////////////////////////////////////////////////////////////////////

const std::string AppInfo::APP_NAME = "FoI Estimator";
const std::string AppInfo::DESCRIPTION =
        "Software for Estimating Force of Infection of Diseases from Serological Data";

const std::string AppInfo::AUTHOR_NAME = "Ha Minh Lam";
const std::string AppInfo::AUTHOR_CONTACT = "minhlam.ha@gmail.com";

const int AppInfo::VERSION_MAJOR = 1;
const int AppInfo::VERSION_MINOR = 0;

/**
 * This date can be set to a fixed value of the following format:
 *     "Jan 19 2017"    <br/>
 * If this string is empty, the build date will be read from the __DATE__ macro.
 */
//const std::string AppInfo::BUILD_DATE = "Jun 12 2017";
const std::string AppInfo::BUILD_DATE;

/**
 * Specify whether the full version string includes the build time.
 * The value means:
 *      0 : Build time is not included at all;
 *      1 : Include build year ;
 *      2 : Include build year and month;
 *      3 : Include build year, month, and day;
 *      4 : Include build year, month, day, and hour;
 *      5 : Include build year, month, day, hour, and minute;
 *      over 5 : Include build year, month, day, hour, minute, and second;
 */
const int AppInfo::VERSION_DETAIL_LEVEL = 5;

/**
 * The status of the current version (e.g. beta, alpha). If the development
 * is completed, this string should be empty ("").
 */
const std::string AppInfo::DEVELOPMENT_STATUS = "beta 1";


const std::string AppInfo::COPYRIGHT = ""
        "Copyright © 2018 Ha Minh Lam. All Rights Reserved.\n"
        "Licensed for non-commercial use only.\n";

const std::string AppInfo::CITATION;


// /////////////////////////////////////////////////////////////////////////////
// Singleton implementation
// /////////////////////////////////////////////////////////////////////////////

AppInfo * AppInfo::instancePtr = nullptr;

AppInfo * AppInfo::instance() {
    if (AppInfo::instancePtr == nullptr) {
        AppInfo::instancePtr = new AppInfo();
    }
    return AppInfo::instancePtr;
}


// /////////////////////////////////////////////////////////////////////////////
// Definition of private methods
// /////////////////////////////////////////////////////////////////////////////

AppInfo::AppInfo() {
    appName = AppInfo::APP_NAME;
    description = AppInfo::DESCRIPTION;
    authorName = AppInfo::AUTHOR_NAME;
    authorContact = AppInfo::AUTHOR_CONTACT;
    
    copyright = AppInfo::COPYRIGHT;
    citation = AppInfo::CITATION;
    
    /* Example of __DATE__ string: "Jan 19 2017" */
    buildDate = AppInfo::BUILD_DATE;
    if (AppInfo::BUILD_DATE.length() <= 0) {
        buildDate = __DATE__;
    }
    
    /* Example of __TIME__ string: "21:06:19" */
    buildTime = __TIME__;
    
    /* Presume that the executable path is the same as the application name.
     * This should be changed after the program starts. */
    executablePath = appName;
    
    /* Initialize version strings */
    std::stringstream shortVersionStream;
    shortVersionStream << AppInfo::VERSION_MAJOR << "."
                       << AppInfo::VERSION_MINOR;
    if (AppInfo::DEVELOPMENT_STATUS.length() > 0) {
        shortVersionStream << " " << AppInfo::DEVELOPMENT_STATUS;
    }
    shortVersionString = shortVersionStream.str();
    
    std::stringstream fullVersionStream;
    fullVersionStream << shortVersionString;
    if (AppInfo::VERSION_DETAIL_LEVEL > 0) {
        fullVersionStream << " " << getBuildDetails();
    }
    fullVersionString = fullVersionStream.str();
}


const std::string AppInfo::getBuildDetails() const {
    std::string buildDetails;
    
    if (AppInfo::VERSION_DETAIL_LEVEL > 0) {
        buildDetails = "build " + getBuildYear();
    }
    if (AppInfo::VERSION_DETAIL_LEVEL > 1) {
        buildDetails += getBuildMonth();
    }
    if (AppInfo::VERSION_DETAIL_LEVEL > 2) {
        buildDetails += getBuildDay();
    }
    if (AppInfo::VERSION_DETAIL_LEVEL > 3) {
        buildDetails += "." + getBuildHour();
    }
    if (AppInfo::VERSION_DETAIL_LEVEL > 4) {
        buildDetails += getBuildMinute();
    }
    if (AppInfo::VERSION_DETAIL_LEVEL > 5) {
        buildDetails += getBuildSecond();
    }
    
    return buildDetails;
}


// /////////////////////////////////////////////////////////////////////////////
// Definition of public methods
// /////////////////////////////////////////////////////////////////////////////

const std::string &AppInfo::getApplicationName() const {
    return appName;
}

const std::string &AppInfo::getDescription() const {
    return description;
}

const std::string &AppInfo::getAuthorName() const {
    return authorName;
}

const std::string &AppInfo::getAuthorContact() const {
    return authorContact;
}

const std::string &AppInfo::getCopyright() const {
    return copyright;
}

const std::string &AppInfo::getCitation() const {
    return citation;
}

const std::string &AppInfo::getVersionShortString() const {
    return shortVersionString;
}

const std::string &AppInfo::getVersionFullString() const {
    return fullVersionString;
}

const std::string &AppInfo::getExecutablePath() const {
    return executablePath;
}

const std::string &AppInfo::getBuildDate() const {
    return buildDate;
}

const std::string &AppInfo::getBuildTime() const {
    return buildTime;
}

std::string AppInfo::getBuildYear() const {
    std::string buildYear = getBuildDate().substr(9, 2);
    return buildYear;
}

std::string AppInfo::getBuildMonth() const {
    std::string buildMonth = getBuildDate().substr(0, 3);
    buildMonth =
            buildMonth == "Jan" ? "01" :
            buildMonth == "Feb" ? "02" :
            buildMonth == "Mar" ? "03" :
            buildMonth == "Apr" ? "04" :
            buildMonth == "May" ? "05" :
            buildMonth == "Jun" ? "06" :
            buildMonth == "Jul" ? "07" :
            buildMonth == "Aug" ? "08" :
            buildMonth == "Sep" ? "09" :
            buildMonth == "Oct" ? "10" :
            buildMonth == "Nov" ? "11" :
            buildMonth == "Dec" ? "12" : "??";
    return buildMonth;
}

std::string AppInfo::getBuildDay() const {
    std::string buildDay = getBuildDate().substr(4, 2);
    if (buildDay[0] == ' ') {
        buildDay[0] = '0';
    }
    return buildDay;
}

std::string AppInfo::getBuildHour() const {
    std::string buildHour = getBuildTime().substr(0, 2);
    return buildHour;
}

std::string AppInfo::getBuildMinute() const {
    std::string buildMinute = getBuildTime().substr(3, 2);
    return buildMinute;
}

std::string AppInfo::getBuildSecond() const {
    std::string buildSecond = getBuildTime().substr(6, 2);
    return buildSecond;
}

void AppInfo::setExecutablePath(const std::string &executablePath) {
    if (executablePath.length() > 0 && executablePath[0] != '\0') {
        this->executablePath = executablePath;
    }
}

void AppInfo::setExecutablePath(const char * executablePath) {
    if (executablePath != nullptr && executablePath[0] != '\0') {
        setExecutablePath(std::string(executablePath));
    }
}
