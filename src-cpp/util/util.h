#ifndef UTIL_H
#define UTIL_H


#include <vector>
#include <cstdio>
#include <iostream>

#include "format.h"


/**
 * Utility methods.
 */
namespace util {
    
    extern std::ostream &outputStream;
    
    class Progress {
    private:
        std::string message;
        
        const long totalIterations;
        const long double dTotalIterations;
        
        const time_t startTime;
        const long updateRateInSeconds;
        
        long lastUpdateTime;
        int printingPrecision;  // The type of this variable must be "int".
        
        long getElapsedTimeInSeconds() const;
    
        
    public:
        explicit Progress() = delete;
        
        explicit Progress(std::string message, const long &totalIterations,
                          const long &updateRateInSeconds);
        
        void print(const long &currentIteration,
                   const bool &printOnNewLine,
                   const bool &forcePrint = false);
    };
    
    
    bool strEqualIgnoreCase(const std::string &strA, const std::string &strB);
}


#endif /* UTIL_H */
