/**
 * Created by Ha Minh Lam on 2018-01-12.
 */

#ifndef DENGUEELISA_NUMBER_H
#define DENGUEELISA_NUMBER_H

#include <string>
#include <sstream>
#include <ctime>


namespace format {
    template<class NumberType = long double>
    std::string formatNumber(const NumberType &number, const char *format,
                             const char thousandSep = '\0') {
        std::string strNumber;

        if (format != nullptr) {
            char buffer[50];
            sprintf(buffer, format, number);
            strNumber = buffer;
        } else {
            strNumber = std::to_string(number);
        }

        if (thousandSep != '\0') {
            long insertPos = strNumber.find_last_of('.');
            if (insertPos == std::string::npos) {
                insertPos = strNumber.find_last_of(',');
            }
            if (insertPos == std::string::npos) {
                insertPos = strNumber.length();
            }
            insertPos -= 3;
            while (insertPos > 0) {
                strNumber.insert(static_cast<unsigned int>(insertPos),
                                 1, thousandSep);
                insertPos -= 3;
            }
        }

        return strNumber;
    }


    inline std::string formatNumber(const long double &number,
                                    const int &integralWidth,
                                    const int &fractionalWidth = 0,
                                    const bool &filledWithZeros = false) {

        int width = integralWidth + fractionalWidth;
        if (fractionalWidth > 0) {
            width++;
        }

        std::stringstream format;
        format << '%';
        if (filledWithZeros) {
            format << '0';
        }
        format << width << '.' << fractionalWidth << "Lf";

        char buffer[50];
        std::sprintf(buffer, format.str().c_str(), number);
        std::string strNumber(buffer);

        return strNumber;
    }


    inline std::string toLowerCase(std::string aString) {
        unsigned long stringLen = aString.length();
        for (unsigned long i = 0; i < stringLen; i++) {
            aString[i] = std::tolower(aString[i]);
        }
        return aString;
    }


    inline std::string formatTime(const long &timeInSeconds) {
        long seconds = timeInSeconds;
        long hours = seconds / 3600;
        long minutes = (seconds / 60) % 60;
        seconds = seconds % 60;

        char buffer[50];
        sprintf(buffer, "%02ld:%02ld:%02ld", hours, minutes,
                seconds);
        std::string strElapsedTime(buffer);

        return strElapsedTime;
    }


    inline std::string formatDateTime(time_t timeInSeconds) {
        struct tm *timeStruct;
        timeStruct = localtime(&timeInSeconds);

        char buffer[250];
        strftime(buffer, sizeof(buffer), "%Y-%m-%d %I:%M:%S", timeStruct);
        std::string strDateTime(buffer);
        return strDateTime;
    }


    /**
     * Get the current date and time in the following format:
     * "yyyy-mm-dd HH:MM:SS".
     * @return A string containing the current date and time.
     */
    inline std::string formatDateTime() {
        return formatDateTime(time(nullptr));
    }
}

#endif //DENGUEELISA_NUMBER_H
