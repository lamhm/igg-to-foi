/**
 * Created by Ha Minh Lam on 2018-01-16.
 */

#ifndef DENGUEELISA_TRANSMISSION_H
#define DENGUEELISA_TRANSMISSION_H


namespace transmission {
    /**
     * Convert attack rates (betas) to infection frequencies (lambdas).
     * The infection frequencies will be used in Poisson distributions to
     * calculate Pr(I = i)
     */
    long double convertAtkRateToInfectionFreq(const long double &attackRate);
    
    //DoubleList convertAtkRateToInfectionFreq(const DoubleList &attackRates);
    
    
    long double convertInfectionFreqToAtkRate(const long double &infectionFreq);
    
    //DoubleList convertInfectionFreqToAtkRate(const DoubleList &infectionFreqs);
};


#endif //DENGUEELISA_TRANSMISSION_H
