/**
 * Created by Ha Minh Lam on 2018-01-23.
 */

#include "PopulationPool.h"


// -----------------------------------------------------------------------------

const std::string PopulationPool::JSON_KEY = "POPULATIONS";

// -----------------------------------------------------------------------------

PopulationPool::PopulationPool(const Json &json,
                               const ParameterPool &parameterPool) {
    Json rootNode = json;
    
    if (rootNode.element_exists(PopulationPool::JSON_KEY)) {
        rootNode = rootNode[PopulationPool::JSON_KEY];
    }
    
    if (rootNode.is_array()) {
        for (const Json &element : rootNode) {
            Population * newPopulation = new Population(element, parameterPool);
            this->addPopulation(newPopulation);
        }
        
    } else {
        Population * newPopulation = new Population(rootNode, parameterPool);
        this->addPopulation(newPopulation);
    }
}

// -----------------------------------------------------------------------------

void PopulationPool::addPopulation(Population * population) {
    if (population == nullptr) {
        return;
    }
    
    if (this->searchPopulationById(population->getId()) != nullptr) {
        throw std::runtime_error(
                "ERROR: Cannot add populations of the same ID into a population"
                " pool [ID: " + population->getId() + "].");
    }
    
    this->populationPtrs.push_back(population);
}

// -----------------------------------------------------------------------------

PopulationPool::~PopulationPool() {
    for (Population * population : this->populationPtrs) {
        delete population;
    }
    this->populationPtrs.clear();
}

// -----------------------------------------------------------------------------

Population *
PopulationPool::searchPopulationById(const std::string &populationId) const {
    for (Population * population : this->populationPtrs) {
        if (population != nullptr && population->getId() == populationId) {
            return population;
        }
    }
    
    return nullptr;
}

// -----------------------------------------------------------------------------

long double PopulationPool::computeLoglikelihood() const {
    long double logLikelihood = 0.0l;
    
    for (Population * population : this->populationPtrs) {
        logLikelihood += population->computeLogLikelihood();
    }
    
    return logLikelihood;
}

// -----------------------------------------------------------------------------

Json PopulationPool::toJson() const {
    Json json = Json::array();
    
    for (Population * population : this->populationPtrs) {
        if (population != nullptr) {
            json.push_back(population->toJson());
        }
    }
    
    return json;
}

// -----------------------------------------------------------------------------

void PopulationPool::addSample(const std::string &populationId,
                               const long double &age,
                               const long double &titre) {
    
    Population * populationPtr = this->searchPopulationById(populationId);
    if (populationPtr != nullptr) {
        populationPtr->addSample(age, titre);
    }
}

// -----------------------------------------------------------------------------

unsigned long PopulationPool::size() const {
    return this->populationPtrs.size();
}

// -----------------------------------------------------------------------------

const std::vector<Population *> &PopulationPool::getPopulations() const {
    return populationPtrs;
}

// -----------------------------------------------------------------------------

void PopulationPool::deletePopulation(Population * const population) {
    if (population != nullptr) {
        populationPtrs.erase(
                std::remove(populationPtrs.begin(), populationPtrs.end(),
                            population),
                populationPtrs.end());
        delete population;
    }
}

// -----------------------------------------------------------------------------

