/**
 * Created by Ha Minh Lam on 2018-01-22.
 */

#ifndef DENGUEFOI_PERSON_H
#define DENGUEFOI_PERSON_H


#include <vector>
#include "population/titre-distributions/TitreDistributions.h"


class Person {
    friend class Population;

private:
    static const long double ZERO_TITRE_VALUE;
    
    long double titre;
    long double age;
    
    long double expectedExposures;
    
    std::vector<long double> probInfectedNTimes;
    
    std::vector<long double> probTitrePerInfectClass;
    
    /**
     * Calculate P(I = n | age, S) -- the probability of a person at a given age
     * being exposed to n infection without knowing her titre. All the results
     * for n = 0 .. (#infectionClassCount - 2) will be calculated.
     * Furthermore, P(I <= #infectionClassCount - 1 | age, S) will also be
     * computed.
     * @param infectionClassCount The number of infection classes.
     */
    void updateProbInfectedNTimes(const unsigned long &infectionClassCount,
                                  const bool &isTitreConsidered);


    void updateProbTitrePerInfectClass(
            const TitreDistributions &titreDistributions);
    
    
public:
    
    explicit Person() = delete;
    
    /**
     * Contruct a Person object.
     * @param age    The age of the person.
     * @param titre  The titre of the person.
     * @note If the given titre is zero, this constructor will replace it by a
     *       very small number (1.0E-15) to avoid problems during likelihood
     *       calculation.
     */
    explicit Person(const long double &age, const long double &titre);
    
    long double getTitre() const;
    
    long double getAge() const;
    
};


#endif //DENGUEFOI_PERSON_H
