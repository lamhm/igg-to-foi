/**
 * Created by Ha Minh Lam on 2018-01-22.
 */

#include <stdexcept>
#include "library/stats.h"
#include "util/data.h"
#include "Person.h"


// -----------------------------------------------------------------------------

const long double Person::ZERO_TITRE_VALUE = 1.0e-15;

// -----------------------------------------------------------------------------

Person::Person(const long double &age, const long double &titre)
        : age(age),
          titre(titre > ZERO_TITRE_VALUE ? titre : ZERO_TITRE_VALUE),
          expectedExposures(-1.0l),
          probInfectedNTimes(),
          probTitrePerInfectClass() {

    /* If the given titre is zero, this constructor will replace it by a very
     * small number to avoid problems during likelihood calculation. */
}

// -----------------------------------------------------------------------------

long double Person::getTitre() const {
    return titre;
}

// -----------------------------------------------------------------------------

long double Person::getAge() const {
    return age;
}

// -----------------------------------------------------------------------------

void Person::updateProbInfectedNTimes(const unsigned long &infectionClassCount,
                                      const bool &isTitreConsidered) {

    if (infectionClassCount <= 0l) {
        throw std::logic_error(
                "ERROR: The number of infection classes must be positive.");
    }

    // Modify the #probInfectedNTimes vector. The vector will have
    // #infectionClassCount elements after this.
    // These are the probabilities of being infected N times given the person
    // age only (the titre value is not considered here).
//    stats::poissonDensity(expectedExposures, infectionClassCount - 1l,
//                          this->probInfectedNTimes);
    this->probInfectedNTimes.resize(infectionClassCount);

    if (infectionClassCount == 2) {
        this->probInfectedNTimes[0] = std::exp(-this->expectedExposures);
        this->probInfectedNTimes[1] = 1.0l - this->probInfectedNTimes[0];

    } else if (infectionClassCount == 3) {
        this->probInfectedNTimes[0] = std::exp(-this->expectedExposures);
        this->probInfectedNTimes[1] =
                4.0l * (std::exp(-this->expectedExposures * 0.75l)
                        - this->probInfectedNTimes[0]);
        this->probInfectedNTimes[2] =
                1.0l
                - (this->probInfectedNTimes[0] + this->probInfectedNTimes[1]);

    } else {
        throw std::logic_error(
                "ERROR: The number of infection classes must be either 2 or 3.");
    }


    // The following part will add the titre component into the probabilities.
    // The #probTitrePerInfectClass vector must be calculated before hand.
    // This part is only used for simulation, and should never be executed
    // during likelihood calculation.
    if (isTitreConsidered) {
        if (this->probTitrePerInfectClass.size() != infectionClassCount) {
            throw std::logic_error(
                    "ERROR: The given number of infection classes does not match"
                    " what is observed from the #probTitrePerInfectClass"
                    " vector.");
        }

        this->probInfectedNTimes = data::multiplyVectors(
                this->probTitrePerInfectClass, this->probInfectedNTimes);

        long double sumProb = data::sumAllElements(this->probInfectedNTimes);
        for (long double &probability : this->probInfectedNTimes) {
            probability /= sumProb;
        }
    }
}

// -----------------------------------------------------------------------------

void Person::updateProbTitrePerInfectClass(
        const TitreDistributions &titreDistributions) {

    titreDistributions.computeProbTitrePerExposureClass(
            this->titre, this->probTitrePerInfectClass);
}

// -----------------------------------------------------------------------------
