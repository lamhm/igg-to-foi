/**
 * Created by Ha Minh Lam on 2018-01-22.
 */

#ifndef DENGUEFOI_FOITABLE_H
#define DENGUEFOI_FOITABLE_H

#include <vector>

#include "parameter/parameter/Parameter.h"


class Population;


class ForceOfInfection : public Observer {
private:
    std::vector<Parameter *> baseAtkFrequencies;
    std::vector<Parameter *> atkScalingFactors;
    std::vector<Parameter *> ageGroupCutoffs;
    
    Population * population;
    
    bool isFinalized;
    

public:
    
    explicit ForceOfInfection(Population * population);
    
    void finalize();
    
    /**
     * Calculate expected number of infections that have occurred on a typical
     * resident of a given age.
     * @param age  The age of the resident.
     */
    Decimal::Type calculateExpectedExposures(Decimal::Type const &age);
    
    void addBaseAtkFrequency(Parameter * baseAtkFrequency);
    
    void addAtkScalingFactors(Parameter * atkScalingFactor);
    
    void addAgeGroupCutOff(Parameter * ageGroupCutOff);
    
    void update(const Observable * caller) override;
    
    std::vector<std::string> getAtkFreqIds() const;
    
    std::vector<std::string> getAtkScalingFactorIds() const;
    
    std::vector<std::string> getAgeGroupCutoffIds() const;
};


#endif //DENGUEFOI_FOITABLE_H
