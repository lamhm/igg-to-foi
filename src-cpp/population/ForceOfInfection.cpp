/**
 * Created by Ha Minh Lam on 2018-01-22.
 */

#include "ForceOfInfection.h"
#include "Population.h"


// -----------------------------------------------------------------------------

ForceOfInfection::ForceOfInfection(Population * population)
        : population(population), baseAtkFrequencies(), atkScalingFactors(),
          ageGroupCutoffs(), isFinalized(false) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

void ForceOfInfection::finalize() {
    if (this->isFinalized) {
        return;
    }
    
    unsigned long nFrequencies = baseAtkFrequencies.size();
    unsigned long nScalingFactors = atkScalingFactors.size();
    unsigned long nAgeCutOffs = ageGroupCutoffs.size();
    
    unsigned long nAgeGroups = nFrequencies;
    if (nAgeGroups < nScalingFactors) {
        nAgeGroups = nScalingFactors;
    }

    if (nFrequencies <= 0) {
        throw std::runtime_error("ERROR: Base attack frequencies "
                                         "are not specified.");
    }
    if (nAgeCutOffs != nAgeGroups - 1) {
        throw std::runtime_error("ERROR: " + std::to_string(nAgeCutOffs) +
                                 " age cut-offs declared; expected " +
                                 std::to_string(nAgeGroups - 1) + ".");
    }
    
    this->isFinalized = true;
}

// -----------------------------------------------------------------------------

Decimal::Type
ForceOfInfection::calculateExpectedExposures(Decimal::Type const &age) {
    auto nFrequencies = baseAtkFrequencies.size();
    auto nScalingFactors = atkScalingFactors.size();
    auto nAgeCutOffs = ageGroupCutoffs.size();
    
    if (nAgeCutOffs <= 0) {
        // Only one age group
        Decimal::Type atkFreqVal =
                baseAtkFrequencies[0]->getLastValueAsDecimal();
        Decimal::Type scalingFactor = 1.0l;
        if (nScalingFactors > 0) {
            scalingFactor = atkScalingFactors[0]->getLastValueAsDecimal();
        }
        return atkFreqVal * scalingFactor * age;
    }
    
    
    // If there are more than one age group...
    auto nAgeGroups = nFrequencies;
    if (nAgeGroups < nScalingFactors) {
        nAgeGroups = nScalingFactors;
    }
    
    Decimal::Type expectedExposures = 0.0l;
    Decimal::Type prevAgeCutOff = 0.0l;
    for (unsigned long ageGroupIdx = 0;
         ageGroupIdx < nAgeGroups; ageGroupIdx++) {
    
        Decimal::Type baseAtkFreq;
        if (ageGroupIdx < nFrequencies) {
            baseAtkFreq =
                    baseAtkFrequencies[ageGroupIdx]->getLastValueAsDecimal();
        } else {
            baseAtkFreq = baseAtkFrequencies[nFrequencies - 1ul]
                    ->getLastValueAsDecimal();
        }
    
        Decimal::Type scalingFactor = 1.0l;
        if (ageGroupIdx < nScalingFactors) {
            scalingFactor =
                    atkScalingFactors[ageGroupIdx]->getLastValueAsDecimal();
        }
    
        Decimal::Type newAgeCutOff = age;
        if (ageGroupIdx < nAgeCutOffs) {
            newAgeCutOff =
                    ageGroupCutoffs[ageGroupIdx]->getLastValueAsDecimal();
            if (newAgeCutOff > age) {
                newAgeCutOff = age;
            }
        }
        
        expectedExposures +=
                (newAgeCutOff - prevAgeCutOff) * scalingFactor * baseAtkFreq;
        prevAgeCutOff = newAgeCutOff;
        
        if (prevAgeCutOff >= age) {
            break;
        }
    }
    
    return expectedExposures;
}

// -----------------------------------------------------------------------------

void ForceOfInfection::update(const Observable * caller) {
    if (this->population != nullptr) {
        this->population->updateProbInfectNTimes();
    }
}

// -----------------------------------------------------------------------------

void ForceOfInfection::addBaseAtkFrequency(Parameter * baseAtkFrequency) {
    if (this->isFinalized) {
        throw std::logic_error("ERROR: Modifying a finalized object is not "
                                       "allowed (ForceOfInfection class).");
    }
    
    if (baseAtkFrequency != nullptr) {
        this->baseAtkFrequencies.push_back(baseAtkFrequency);
        baseAtkFrequency->attachObserver(this);
    }
}

// -----------------------------------------------------------------------------

void ForceOfInfection::addAtkScalingFactors(Parameter * atkScalingFactor) {
    if (this->isFinalized) {
        throw std::logic_error("ERROR: Modifying a finalized object is not "
                                       "allowed (ForceOfInfection class).");
    }
    
    if (atkScalingFactor != nullptr) {
        this->atkScalingFactors.push_back(atkScalingFactor);
        atkScalingFactor->attachObserver(this);
    }
}

// -----------------------------------------------------------------------------

void ForceOfInfection::addAgeGroupCutOff(Parameter * ageGroupCutOff) {
    if (this->isFinalized) {
        throw std::logic_error("ERROR: Modifying a finalized object is not "
                                       "allowed (ForceOfInfection class).");
    }
    
    if (ageGroupCutOff != nullptr) {
        this->ageGroupCutoffs.push_back(ageGroupCutOff);
        ageGroupCutOff->attachObserver(this);
    }
}

// -----------------------------------------------------------------------------

std::vector<std::string> ForceOfInfection::getAtkFreqIds() const {
    std::vector<std::string> stringVector;
    for (Parameter * atkFreqParam : this->baseAtkFrequencies) {
        if (atkFreqParam != nullptr) {
            stringVector.push_back(atkFreqParam->getId());
        }
    }
    return stringVector;
}

// -----------------------------------------------------------------------------

std::vector<std::string>
ForceOfInfection::getAtkScalingFactorIds() const {
    std::vector<std::string> stringVector;
    for (Parameter * atkScalingFactorParam : this->atkScalingFactors) {
        if (atkScalingFactorParam != nullptr) {
            stringVector.push_back(atkScalingFactorParam->getId());
        }
    }
    return stringVector;
}

// -----------------------------------------------------------------------------

std::vector<std::string> ForceOfInfection::getAgeGroupCutoffIds() const {
    std::vector<std::string> stringVector;
    for (Parameter * ageGroupCutoffParam : this->ageGroupCutoffs) {
        if (ageGroupCutoffParam != nullptr) {
            stringVector.push_back(ageGroupCutoffParam->getId());
        }
    }
    return stringVector;
}

// -----------------------------------------------------------------------------
