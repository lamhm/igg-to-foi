/**
 * Created by Ha Minh Lam on 2018-01-23.
 */

#ifndef DENGUEFOI_POPULATION_H
#define DENGUEFOI_POPULATION_H


#include <vector>
#include "parameter/ParameterPool.h"
#include "Person.h"
#include "ForceOfInfection.h"
#include "titre-distributions/TitreDistributions.h"


class Population : public Serializable {
private:
    
    std::string id;
    
    std::vector<Person> persons;
    
    ForceOfInfection forceOfInfection;
    
    TitreDistributions * titreDistributions;
    
    explicit Population();
    
    void addAtkFrequency(Json stringTypedJsonNode,
                         const ParameterPool &parameterPool);
    
    void addAtkScalingFactor(Json stringTypedJsonNode,
                             const ParameterPool &parameterPool);
    
    void addAgeGroupCutoff(Json stringTypedJsonNode,
                           const ParameterPool &parameterPool);
    
    /**
     * This struct contains constants that are needed to serialize Population
     * objects.
     */
    struct AttrKey {
        static const std::string ID;
        static const std::string TITRE_DISTRIBUTIONS;
        static const std::string ATTACK_FREQUENCIES;
        static const std::string ATTACK_SCALES;
        static const std::string AGE_GROUP_CUTOFFS;
    };

    /**
     * The worst log likelihood value. This is used in place of -INF when any
     * parameters in the model receive in valid values.
     * @see Population::computeLogLikelihood() method.
     */
    static const long double WORST_LOG_LIKELIHOOD;

public:
    
    explicit Population(Json json, const ParameterPool &parameterPool);
    
    virtual ~Population();
    
    const std::string &getId() const;
    
    void addSample(const long double &age, const long double &titre);
    
    void updateProbInfectNTimes();
    
    void updateProbTitrePerInfectClass();
    
    /**
     * Update the state of all the samples in this population and make them
     * ready for a new MCMC run.
     * @note This method should only be called at the beginnig of an MCMC run.
     */
    void readyForMcmc();
    
    long double computeLogLikelihood() const;
    
    Json toJson() const override;
    
    unsigned long getPersonCount() const;
};


#endif //DENGUEFOI_POPULATION_H
