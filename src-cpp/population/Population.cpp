/**
 * Created by Ha Minh Lam on 2018-01-23.
 */

#include "Population.h"

#include <limits>
#include "titre-distributions/TitreDistributions.h"


// -----------------------------------------------------------------------------

const std::string Population::AttrKey::ID = "id";
const std::string Population::AttrKey::TITRE_DISTRIBUTIONS = "titre_distributions";
const std::string Population::AttrKey::ATTACK_FREQUENCIES = "attack_frequencies";
const std::string Population::AttrKey::ATTACK_SCALES = "attack_scaling_factors";
const std::string Population::AttrKey::AGE_GROUP_CUTOFFS = "age_group_cut_offs";

const long double Population::WORST_LOG_LIKELIHOOD =
        static_cast<long double>(std::numeric_limits<long double>::min_exponent)
        - 1.0l;

// -----------------------------------------------------------------------------

Population::Population() : id(""), persons(), forceOfInfection(this),
                           titreDistributions(nullptr) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

Population::Population(Json json, const ParameterPool &parameterPool)
        : Population() {
    
    if (!json.is_object()) {
        throw std::runtime_error("ERROR: Invalid population structure.");
    }
    
    if (json.element_exists(AttrKey::ID) && json[AttrKey::ID].is_string()) {
        this->id = json[AttrKey::ID].get<std::string>();
    } else {
        throw std::runtime_error("ERROR: Invalid population ID.");
    }
    
    if (json.element_exists(AttrKey::TITRE_DISTRIBUTIONS)) {
        Json titreDistJson = json[AttrKey::TITRE_DISTRIBUTIONS];
        this->titreDistributions =
                TitreDistributions::newFromJson(titreDistJson, this,
                                                parameterPool);
    }
    
    if (json.element_exists(AttrKey::ATTACK_FREQUENCIES)) {
        this->addAtkFrequency(json[AttrKey::ATTACK_FREQUENCIES], parameterPool);
    }
    
    if (json.element_exists(AttrKey::ATTACK_SCALES)) {
        this->addAtkScalingFactor(json[AttrKey::ATTACK_SCALES], parameterPool);
    }
    
    if (json.element_exists(AttrKey::AGE_GROUP_CUTOFFS)) {
        this->addAgeGroupCutoff(json[AttrKey::AGE_GROUP_CUTOFFS],
                                parameterPool);
    }
    
    // Lock the FoI-related parameters.
    this->forceOfInfection.finalize();
    
    // Lock the titre distributions.
    //this->titreDistributions.finalize();
}

// -----------------------------------------------------------------------------

void Population::addSample(const long double &age, const long double &titre) {
    this->persons.emplace_back(age, titre);
}

// -----------------------------------------------------------------------------

void Population::addAtkFrequency(Json stringTypedJsonNode,
                                 const ParameterPool &parameterPool) {
    
    bool error = false;
    
    if (stringTypedJsonNode.is_array()) {
        for (const Json &element : stringTypedJsonNode) {
            if (element.is_string()) {
                addAtkFrequency(element, parameterPool);
            } else {
                error = true;
                break;
            }
        }
        
    } else if (stringTypedJsonNode.is_string()) {
        std::string paramId = stringTypedJsonNode.get<std::string>();
        Parameter * baseAtkFreqParameter =
                parameterPool.searchParameterById(paramId);
        
        if (baseAtkFreqParameter != nullptr) {
            this->forceOfInfection.addBaseAtkFrequency(baseAtkFreqParameter);
        } else {
            throw std::runtime_error(
                    "ERROR: Parameter not found [ID: " + paramId + "].");
        }
        
    } else {
        error = true;
    }
    
    if (error) {
        throw std::runtime_error(
                "ERROR: The list of base attack frequencies must contain"
                " (string-typed) parameter IDs only.");
    }
}

// -----------------------------------------------------------------------------

void Population::addAtkScalingFactor(Json stringTypedJsonNode,
                                     const ParameterPool &parameterPool) {
    
    bool error = false;
    
    if (stringTypedJsonNode.is_array()) {
        for (const Json &element : stringTypedJsonNode) {
            if (element.is_string()) {
                addAtkScalingFactor(element, parameterPool);
            } else {
                error = true;
                break;
            }
        }
        
    } else if (stringTypedJsonNode.is_string()) {
        std::string paramId = stringTypedJsonNode.get<std::string>();
        Parameter * atkScalingFactorParameter =
                parameterPool.searchParameterById(paramId);
        
        if (atkScalingFactorParameter != nullptr) {
            this->forceOfInfection.addAtkScalingFactors(
                    atkScalingFactorParameter);
        } else {
            throw std::runtime_error(
                    "ERROR: Parameter not found [ID: " + paramId + "].");
        }
        
    } else {
        error = true;
    }
    
    if (error) {
        throw std::runtime_error(
                "ERROR: The list of attack scaling factors must contain"
                " (string-typed) parameter IDs only.");
    }
}

// -----------------------------------------------------------------------------

void Population::addAgeGroupCutoff(Json stringTypedJsonNode,
                                   const ParameterPool &parameterPool) {
    
    bool error = false;
    
    if (stringTypedJsonNode.is_array()) {
        for (const Json &element : stringTypedJsonNode) {
            if (element.is_string()) {
                addAgeGroupCutoff(element, parameterPool);
            } else {
                error = true;
                break;
            }
        }
        
    } else if (stringTypedJsonNode.is_string()) {
        std::string paramId = stringTypedJsonNode.get<std::string>();
        Parameter * ageGroupCutoffParameter =
                parameterPool.searchParameterById(paramId);
        
        if (ageGroupCutoffParameter != nullptr) {
            this->forceOfInfection.addAgeGroupCutOff(ageGroupCutoffParameter);
        } else {
            throw std::runtime_error(
                    "ERROR: Parameter not found [ID: " + paramId + "].");
        }
        
    } else {
        error = true;
    }
    
    if (error) {
        throw std::runtime_error(
                "ERROR: The list of age-group cut-offs must contain"
                " (string-typed) parameter IDs only.");
    }
}

// -----------------------------------------------------------------------------

void Population::updateProbInfectNTimes() {
    for (Person &person : this->persons) {
        person.expectedExposures =
                this->forceOfInfection.calculateExpectedExposures(person.age);
        person.updateProbInfectedNTimes(
                this->titreDistributions->getNInfectionClasses(), false);
    }
}

// -----------------------------------------------------------------------------

void Population::updateProbTitrePerInfectClass() {
    for (Person &person : this->persons) {
        person.updateProbTitrePerInfectClass(*titreDistributions);
    }
}

// -----------------------------------------------------------------------------

long double Population::computeLogLikelihood() const {
    if (!this->titreDistributions->isValid()) {
        /* Invalid titre distributions. Return worst log-likelihood. */
        const auto personCount
                = static_cast<long double>(this->persons.size());
        return Population::WORST_LOG_LIKELIHOOD * personCount;
    }
    
    long double sumLogLikelihood = 0.0l;
    unsigned long nInfectClasses =
            this->titreDistributions->getNInfectionClasses();
    
    for (const Person &person : this->persons) {
        long double personLikelihood = 0.0;
        for (unsigned long nExposed = 0l;
             nExposed < nInfectClasses; nExposed++) {
            personLikelihood += person.probTitrePerInfectClass[nExposed] *
                                person.probInfectedNTimes[nExposed];
        }
        
        sumLogLikelihood += std::log(personLikelihood);
    }
    
    return sumLogLikelihood;
}

// -----------------------------------------------------------------------------

Json Population::toJson() const {
    Json json;
    
    if (this->id.length() > 0) {
        json[AttrKey::ID] = this->id;
    }
    
    json[AttrKey::TITRE_DISTRIBUTIONS] = this->titreDistributions->toJson();
    
    json[AttrKey::ATTACK_FREQUENCIES] = this->forceOfInfection.getAtkFreqIds();
    json[AttrKey::ATTACK_SCALES] =
            this->forceOfInfection.getAtkScalingFactorIds();
    json[AttrKey::AGE_GROUP_CUTOFFS] =
            this->forceOfInfection.getAgeGroupCutoffIds();
    
    return json;
}

// -----------------------------------------------------------------------------

const std::string &Population::getId() const {
    return id;
}

// -----------------------------------------------------------------------------

void Population::readyForMcmc() {
    // Make sure FoI-related parameters are locked.
    this->forceOfInfection.finalize();
    
    // Make sure titre distributions are locked.
    //this->titreDistributions.finalize();
    
    // Force updating the state of all the samples in this population.
    updateProbInfectNTimes();
    updateProbTitrePerInfectClass();
}

// -----------------------------------------------------------------------------

unsigned long Population::getPersonCount() const {
    return this->persons.size();
}

// -----------------------------------------------------------------------------

Population::~Population() {
    if (titreDistributions != nullptr) {
        delete this->titreDistributions;
        this->titreDistributions = nullptr;
    }
}

// -----------------------------------------------------------------------------

