/**
 * Created by Ha Minh Lam on 2018-06-19.
 */

#include "CategoricalTitreDistributions.h"


// /////////////////////////////////////////////////////////////////////////////
// STATIC CONSTANTS
// /////////////////////////////////////////////////////////////////////////////

const std::string CategoricalTitreDistributions::AttrKey::PROB_MATRIX
        = "probability_matrix";
const std::string CategoricalTitreDistributions::AttrKey::TITRE_CUTOFFS
        = "titre_cutoffs";


// /////////////////////////////////////////////////////////////////////////////
// MEMBER METHODS
// /////////////////////////////////////////////////////////////////////////////

CategoricalTitreDistributions::
CategoricalTitreDistributions(Population * population)
        : TitreDistributions(population), probMatrix(), titreCutOffs() {
    // Do nothing
}

// -----------------------------------------------------------------------------

CategoricalTitreDistributions::CategoricalTitreDistributions(
        Population * population, Json const &json,
        ParameterPool const &parameterPool)
        : CategoricalTitreDistributions(population) {
    
    if (json.element_exists(AttrKey::PROB_MATRIX)) {
        Json probMatrixJson = json[AttrKey::PROB_MATRIX];
        if (probMatrixJson.is_string()) {
            this->addProbabilityParameter(probMatrixJson.get<std::string>(),
                                          parameterPool);
            
        } else if (probMatrixJson.is_array()) {
            for (Json const &probParam : probMatrixJson) {
                this->addProbabilityParameter(probParam.get<std::string>(),
                                              parameterPool);
            }
            
        } else {
            throw std::runtime_error(
                    "ERROR: Categorical Titre Distributions -- "
                    "Invalid probability parameters.");
        }
    }
    
    if (json.element_exists(AttrKey::TITRE_CUTOFFS)) {
        Json titreCutoffsJson = json[AttrKey::TITRE_CUTOFFS];
        if (titreCutoffsJson.is_string()) {
            this->addTitreCutoffParameter(titreCutoffsJson.get<std::string>(),
                                          parameterPool);
            
        } else if (titreCutoffsJson.is_array()) {
            for (Json const &titreCutoff : titreCutoffsJson) {
                this->addTitreCutoffParameter(titreCutoff.get<std::string>(),
                                              parameterPool);
            }
            
        } else {
            throw std::runtime_error(
                    "ERROR: Categorical Titre Distributions -- "
                    "Invalid titre cut-offs.");
        }
    }
    
    finalize();
}

// -----------------------------------------------------------------------------

void CategoricalTitreDistributions::finalize() {
    unsigned long nProbParams = probMatrix.size();
    unsigned long nTitreCutoffs = titreCutOffs.size();
    
    this->nInfectionClasses = nTitreCutoffs + 1;
    unsigned long expectedNProbParams = nInfectionClasses * nTitreCutoffs;
    
    if (this->nInfectionClasses <= 1) {
        throw std::runtime_error(
                "ERROR: Categorical Titre Distributions -- "
                "The number of infection classes must be greater than 1.");
    }
    
    if (nProbParams != expectedNProbParams) {
        throw std::runtime_error("ERROR: Categorical Titre Distributions -- "
                                 "Invalid number of probability parameters (" +
                                 std::to_string(expectedNProbParams) +
                                 " expected, " +
                                 std::to_string(nProbParams) + " observed.");
    }
    
    /* When the number of exposure classes is greater than the number of titre
     * classes (i.e. one cut-off C may appear twice in the cut-off list),
     * make sure the probability of the corresponding "null" titre class
     * P(C <= Y < C | E)  is always 0 (zero). */
    for (auto cutoffIdx = 1l; cutoffIdx < nTitreCutoffs; cutoffIdx++) {
        if (titreCutOffs[cutoffIdx] == titreCutOffs[cutoffIdx - 1]) {
            auto titreClass = cutoffIdx;
            for (auto eClass = 0l; eClass < nInfectionClasses; eClass++) {
                auto zeroProbParamIdx = eClass * nTitreCutoffs + titreClass;
                auto zeroProbParam = probMatrix[zeroProbParamIdx];
                if (!zeroProbParam->isFixed() ||
                    zeroProbParam->getLastValueAsDecimal() != 0.0) {
                    throw std::runtime_error(
                            "ERROR: Categorical Titre Distributions -- 'Null' "
                            "titre classes must have a fixed zero probability.");
                }
            }
        }
    }
    
}

// -----------------------------------------------------------------------------

std::string const &CategoricalTitreDistributions::getTypeName() const {
    return TitreDistributions::TypeName::CATEGORICAL;
}

// -----------------------------------------------------------------------------

void CategoricalTitreDistributions::addProbabilityParameter(
        std::string const &paramName, ParameterPool const &parameterPool) {
    
    Parameter * probParam = parameterPool.searchParameterById(paramName);
    if (probParam != nullptr) {
        this->probMatrix.emplace_back(probParam);
        probParam->attachObserver(this);
        
    } else {
        throw std::runtime_error(
                "ERROR: Parameter [" + paramName + "] not found.");
    }
}

// -----------------------------------------------------------------------------

void CategoricalTitreDistributions::addTitreCutoffParameter(
        std::string const &paramName, ParameterPool const &parameterPool) {
    
    Parameter * titreCutoffParam = parameterPool.searchParameterById(paramName);
    if (titreCutoffParam != nullptr) {
        this->titreCutOffs.emplace_back(titreCutoffParam);
        titreCutoffParam->attachObserver(this);
        
    } else {
        throw std::runtime_error(
                "ERROR: Parameter [" + paramName + "] not found.");
    }
}

// -----------------------------------------------------------------------------

bool CategoricalTitreDistributions::isValid() const {
    /* Check  0.0 <= sum[ Pr(Y|E) ] <= 1.0 */
    auto nInfClassesSub1 = nInfectionClasses - 1;
    for (auto eClass = 0l; eClass < nInfectionClasses; eClass++) {
        Decimal::Type sumProb = 0.0;
        for (auto yClass = 0l; yClass < nInfClassesSub1; yClass++) {
            auto probParamIndex = eClass * nInfClassesSub1 + yClass;
            sumProb += probMatrix[probParamIndex]->getLastValueAsDecimal();
        }
        if (sumProb > 1.0 || sumProb < 0.0) {
            return false;
        }
    }
    
    /* Make sure that the titre cut-offs are in strictly ascending order */
    auto nTitreCutoffs = this->titreCutOffs.size();
    for (auto cutoffIdx = 1l; cutoffIdx < nTitreCutoffs; cutoffIdx++) {
        auto prevCutoff = titreCutOffs[cutoffIdx - 1];
        auto currCutoff = titreCutOffs[cutoffIdx];
        
        if (prevCutoff != currCutoff
            && prevCutoff->getLastValueAsDecimal() >=
               currCutoff->getLastValueAsDecimal()) {
            return false;
        }
    }
    
    /* No problems found. Return TRUE. */
    return true;
}

// -----------------------------------------------------------------------------

void CategoricalTitreDistributions::computeProbTitrePerExposureClass(
        const Decimal::Type &titreValue,
        std::vector<Decimal::Type> &resultStorage) const {
    
    resultStorage.resize(this->nInfectionClasses);
    
    /* Find the exposure class that the given titre belongs to */
    unsigned long titreClass = 0;
    auto nCutoffs = this->titreCutOffs.size();
    while (titreClass < nCutoffs &&
           titreValue >= titreCutOffs[titreClass]->getLastValueAsDecimal()) {
        titreClass++;
    }
    
    for (auto exposureClass = 0l;
         exposureClass < nInfectionClasses; exposureClass++) {
        
        if (titreClass < nCutoffs) {
            auto probParamIdx = exposureClass * nCutoffs + titreClass;
            resultStorage[exposureClass] =
                    probMatrix[probParamIdx]->getLastValueAsDecimal();
            
        } else {
            /* titreClass == nCutoffs */
            Decimal::Type sumOtherProbs = 0.0;
            for (auto yClass = 0l; yClass < nCutoffs; yClass++) {
                auto probParamIndex = exposureClass * nCutoffs + yClass;
                sumOtherProbs += probMatrix[probParamIndex]->getLastValueAsDecimal();
            }
            resultStorage[exposureClass] = 1.0l - sumOtherProbs;
        }
    }
}

// -----------------------------------------------------------------------------

Json CategoricalTitreDistributions::toJson() const {
    Json json = TitreDistributions::toJson();
    
    std::vector<std::string> probParamIds;
    for (Parameter * probParam : this->probMatrix) {
        if (probParam != nullptr) {
            probParamIds.emplace_back(probParam->getId());
        }
    }
    json[AttrKey::PROB_MATRIX] = probParamIds;
    
    std::vector<std::string> titreCutoffIds;
    for (Parameter * cutoffParam : this->titreCutOffs) {
        if (cutoffParam != nullptr) {
            titreCutoffIds.emplace_back(cutoffParam->getId());
        }
    }
    json[AttrKey::TITRE_CUTOFFS] = titreCutoffIds;
    
    return json;
}

// -----------------------------------------------------------------------------
