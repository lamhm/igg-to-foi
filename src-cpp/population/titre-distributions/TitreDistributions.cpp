/**
 * Created by Ha Minh Lam on 2018-01-23.
 */

#include "util/format.h"
#include "library/stats.h"
#include "TitreDistributions.h"
#include "population/Population.h"
#include "GammaTitreDistributions.h"
#include "CategoricalTitreDistributions.h"


// /////////////////////////////////////////////////////////////////////////////
// STATIC CONSTANTS & METHODS
// /////////////////////////////////////////////////////////////////////////////

const std::string TitreDistributions::AttrKey::TYPE = "type";

const std::string TitreDistributions::TypeName::GAMMA = "gamma";
const std::string TitreDistributions::TypeName::CATEGORICAL = "categorical";

// -----------------------------------------------------------------------------

TitreDistributions * TitreDistributions::newFromJson(
        Json const &json, Population * population,
        ParameterPool const &parameterPool) {
    
    if (!json.is_object()) {
        throw std::runtime_error("ERROR: Invalid titre distributions.");
    }
    
    if (json.element_exists(AttrKey::TYPE) && json[AttrKey::TYPE].is_string()) {
        std::string titreDistType = json[AttrKey::TYPE].get<std::string>();
        // Make the titreDistType case-insensitive.
        titreDistType = format::toLowerCase(titreDistType);
        if (titreDistType == TypeName::CATEGORICAL) {
            return new CategoricalTitreDistributions(population, json,
                                                     parameterPool);
        }
    }
    
    /* Create gamma titre distributions by default */
    return new GammaTitreDistributions(population, json, parameterPool);
}


// /////////////////////////////////////////////////////////////////////////////
// MEMBER METHODS
// /////////////////////////////////////////////////////////////////////////////

TitreDistributions::TitreDistributions(Population * population)
        : nInfectionClasses(0), population(population) {
    // Do nothing.
}

// -----------------------------------------------------------------------------

unsigned long TitreDistributions::getNInfectionClasses() const {
    return nInfectionClasses;
}

// -----------------------------------------------------------------------------

void TitreDistributions::update(const Observable * caller) {
    this->population->updateProbTitrePerInfectClass();
}

// -----------------------------------------------------------------------------

Json TitreDistributions::toJson() const {
    Json json;
    
    json[AttrKey::TYPE] = this->getTypeName();
    
    return json;
}

// -----------------------------------------------------------------------------
