/**
 * Created by Ha Minh Lam on 2018-01-23.
 */

#ifndef DENGUEFOI_TITREDISTRIBUTIONS_H
#define DENGUEFOI_TITREDISTRIBUTIONS_H


#include <vector>
#include <parameter/parameter/Parameter.h>


class Population;


class TitreDistributions : public Observer, public Serializable {
protected:
    unsigned long nInfectionClasses;
    
    Population * population;
    
    struct AttrKey {
        static const std::string TYPE;
    };
    
    struct TypeName {
        static const std::string GAMMA;
        static const std::string CATEGORICAL;
    };
    
    explicit TitreDistributions(Population * population);
    
    virtual std::string const &getTypeName() const = 0;
    

public:
    
    static TitreDistributions* newFromJson(Json const &json,
                                           Population * population,
                                           ParameterPool const &parameterPool);
    
    /**
     * Get the number of infection classes covered in this mixture distribution.
     * @return A non-negative integer indicating the number of titre
     *         distributions.
     */
    unsigned long getNInfectionClasses() const;
    
    virtual bool isValid() const = 0;
    
    /**
     * Compute a vector of #distributionCount elements containing the
     * probabilities P(Y = titreValue | I = 0), ... , P(Y = titreValue | I = n-2),
     * and P(Y = titreValue | I >= n-1); where I is the number of infections
     * that the person has been exposed to, and n is he number of infection
     * classes.
     * @param titreValue     The value of the observed titre.
     * @param resultStorage  The vector that will be used to store the result
     *                       probabilities.
     */
    virtual void computeProbTitrePerExposureClass(
            const Decimal::Type &titreValue,
            std::vector<Decimal::Type> &resultStorage) const = 0;
    
    void update(const Observable * caller) override;
    
    Json toJson() const override;
};


#endif //DENGUEFOI_TITREDISTRIBUTIONS_H
