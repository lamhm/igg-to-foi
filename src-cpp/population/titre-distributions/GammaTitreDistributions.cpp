/**
 * Created by Ha Minh Lam on 2018-06-18.
 */

#include <library/stats.h>
#include "GammaTitreDistributions.h"
#include "population/Population.h"


// /////////////////////////////////////////////////////////////////////////////
// STATIC CONSTANTS
// /////////////////////////////////////////////////////////////////////////////

const std::string GammaTitreDistributions::AttrKey::TITRE_MEANS = "titre_means";
const std::string GammaTitreDistributions::AttrKey::TITRE_STD_DEVIATIONS
        = "titre_std_deviations";


// /////////////////////////////////////////////////////////////////////////////
// MEMBER METHODS
// /////////////////////////////////////////////////////////////////////////////

GammaTitreDistributions::GammaTitreDistributions(Population * population)
        : TitreDistributions(population), meanParameters(), stdDevParameters() {
    // Do nothing.
}

// -----------------------------------------------------------------------------

GammaTitreDistributions::GammaTitreDistributions(
        Population * population, Json const &json,
        const ParameterPool &parameterPool)
        : GammaTitreDistributions(population) {
    
    if (json.element_exists(AttrKey::TITRE_MEANS)) {
        Json titreMeansJson = json[AttrKey::TITRE_MEANS];
        if (titreMeansJson.is_string()) {
            this->addMeanParameter(titreMeansJson.get<std::string>(),
                                   parameterPool);
            
        } else if (titreMeansJson.is_array()) {
            for (Json const &meanParamId : titreMeansJson) {
                this->addMeanParameter(meanParamId.get<std::string>(),
                                       parameterPool);
            }
            
        } else {
            throw std::runtime_error("ERROR: Invalid mean parameter.");
        }
    }
    
    if (json.element_exists(AttrKey::TITRE_STD_DEVIATIONS)) {
        Json titreStdDevsJson = json[AttrKey::TITRE_STD_DEVIATIONS];
        if (titreStdDevsJson.is_string()) {
            this->addStdDevParameter(titreStdDevsJson.get<std::string>(),
                                     parameterPool);
            
        } else if (titreStdDevsJson.is_array()) {
            for (Json const &stdDevParamId : titreStdDevsJson) {
                this->addStdDevParameter(stdDevParamId.get<std::string>(),
                                         parameterPool);
            }
            
        } else {
            throw std::runtime_error(
                    "ERROR: Invalid standard-deviation parameter.");
        }
    }
    
    finalize();
}

// -----------------------------------------------------------------------------

void GammaTitreDistributions::finalize() {
    unsigned long nMeanParams = meanParameters.size();
    unsigned long nStdDevParams = stdDevParameters.size();
    
    if (nMeanParams <= 0 || nStdDevParams <= 0) {
        throw std::runtime_error(
                "ERROR: At least 1 mean parameter and 1 standard-deviation "
                "parameter are required for the titre distributions.");
    }
    
    this->nInfectionClasses = nMeanParams;
    if (this->nInfectionClasses < nStdDevParams) {
        this->nInfectionClasses = nStdDevParams;
    }
    
    Parameter * lastMeanParam = this->meanParameters[nMeanParams - 1];
    for (unsigned long i = nMeanParams; i < this->nInfectionClasses; i++) {
        this->meanParameters.emplace_back(lastMeanParam);
    }
    
    Parameter * lastStdDevParam = this->stdDevParameters[nStdDevParams - 1];
    for (unsigned long i = nStdDevParams; i < this->nInfectionClasses; i++) {
        this->stdDevParameters.emplace_back(lastStdDevParam);
    }
}

// -----------------------------------------------------------------------------

bool GammaTitreDistributions::isValid() const {
    /* The following loop starts from 1 (one). */
//    for (auto distIdx = 1l; distIdx < this->nInfectionClasses; distIdx++) {
//        if (meanParameters[distIdx]->getLastValueAsDecimal()
//            < meanParameters[distIdx - 1]->getLastValueAsDecimal()) {
//            return false;
//        }
//    }
    
    return true;
}

// -----------------------------------------------------------------------------

void GammaTitreDistributions::computeProbTitrePerExposureClass(
        const Decimal::Type &titreValue,
        std::vector<Decimal::Type> &resultStorage) const {
    
    resultStorage.resize(this->nInfectionClasses);
    
    for (auto exposureClass = 0l;
         exposureClass < this->nInfectionClasses; exposureClass++) {
        auto distMean =
                this->meanParameters[exposureClass]->getLastValueAsDecimal();
        auto distStdDev =
                this->stdDevParameters[exposureClass]->getLastValueAsDecimal();
        auto distRate = distMean / (distStdDev * distStdDev);
        auto distShape = distMean * distRate;
        resultStorage[exposureClass] =
                stats::gammaDensity(titreValue, distShape, distRate, false);
    }
}

// -----------------------------------------------------------------------------

std::string const &GammaTitreDistributions::getTypeName() const {
    return TitreDistributions::TypeName::GAMMA;
}

// -----------------------------------------------------------------------------

Json GammaTitreDistributions::toJson() const {
    Json json = TitreDistributions::toJson();
    
    std::vector<std::string> meanParamIds;
    for (Parameter * meanParam : this->meanParameters) {
        if (meanParam != nullptr) {
            meanParamIds.emplace_back(meanParam->getId());
        }
    }
    json[AttrKey::TITRE_MEANS] = meanParamIds;
    
    std::vector<std::string> stdDevParamIds;
    for (Parameter * stdDevParam : this->stdDevParameters) {
        if (stdDevParam != nullptr) {
            stdDevParamIds.emplace_back(stdDevParam->getId());
        }
    }
    json[AttrKey::TITRE_STD_DEVIATIONS] = stdDevParamIds;
    
    return json;
}

// -----------------------------------------------------------------------------

void GammaTitreDistributions::addMeanParameter(
        std::string const &paramName, ParameterPool const &parameterPool) {
    
    Parameter * meanParam = parameterPool.searchParameterById(paramName);
    if (meanParam != nullptr) {
        this->meanParameters.emplace_back(meanParam);
        meanParam->attachObserver(this);
        
    } else {
        throw std::runtime_error(
                "ERROR: Parameter [" + paramName + "] not found.");
    }
}

// -----------------------------------------------------------------------------

void GammaTitreDistributions::addStdDevParameter(
        std::string const &paramName, ParameterPool const &parameterPool) {
    
    Parameter * stdDevParam = parameterPool.searchParameterById(paramName);
    if (stdDevParam != nullptr) {
        this->stdDevParameters.emplace_back(stdDevParam);
        stdDevParam->attachObserver(this);
        
    } else {
        throw std::runtime_error(
                "ERROR: Parameter [" + paramName + "] not found.");
    }
}

// -----------------------------------------------------------------------------
