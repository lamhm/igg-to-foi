/**
 * Created by Ha Minh Lam on 2018-06-18.
 */

#ifndef DENGUEFOI_GAMMATITREDISTRIBUTIONS_H
#define DENGUEFOI_GAMMATITREDISTRIBUTIONS_H


#include "TitreDistributions.h"


class GammaTitreDistributions : public TitreDistributions {
private:
    std::vector<Parameter *> meanParameters;
    std::vector<Parameter *> stdDevParameters;
    
    std::string const &getTypeName() const override;
    
    struct AttrKey {
        static const std::string TITRE_MEANS;
        static const std::string TITRE_STD_DEVIATIONS;
    };
    
    void addMeanParameter(std::string const &paramName,
                          ParameterPool const &parameterPool);
    
    void addStdDevParameter(std::string const &paramName,
                            ParameterPool const &parameterPool);
    
    void finalize();


public:
    explicit GammaTitreDistributions(Population * population);
    
    explicit GammaTitreDistributions(Population * population, Json const &json,
                                     ParameterPool const &parameterPool);
    
    /**
     * Check to see whether the distribution mean of negative titres is not
     * greater than that of positive titres, and the distribution mean of
     * primary titres is not greater than that of secondary titres.
     * @return TRUE if the distribution mean of negative titres is not
     *         greater than that of positive titres, and the distribution mean
     *         of primary titres is not greater than that of secondary titres;
     *         FALSE, otherwise.
     */
    bool isValid() const override;
    
    void computeProbTitrePerExposureClass(
            const Decimal::Type &titreValue,
            std::vector<Decimal::Type> &resultStorage) const override;
    
    Json toJson() const override;
};


#endif //DENGUEFOI_GAMMATITREDISTRIBUTIONS_H
