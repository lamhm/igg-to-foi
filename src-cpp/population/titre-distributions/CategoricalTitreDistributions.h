/**
 * Created by Ha Minh Lam on 2018-06-19.
 */

#ifndef DENGUEFOI_CATEGORICALTITREDISTRIBUTIONS_H
#define DENGUEFOI_CATEGORICALTITREDISTRIBUTIONS_H


#include "TitreDistributions.h"


class CategoricalTitreDistributions : public TitreDistributions {
private:
    
    /**
     * This vector represent a 2-dimensional matrix containing the probabilities
     * Pr(Y belongs to category y | E = x), where Y is the titre and E is the
     * number of exposures.
     * For instance, if there are 3 titre categories (with 3 exposure classes),
     * the vector will contain the following probabilities:
     *     { Pr(0|0), Pr(1|0),
     *       Pr(0|1), Pr(1|1),
     *       Pr(0|2), Pr(1|2) }
     * Note that Pr(2|0), Pr(2|1), and Pr(2|2) are not in the list since:
     *     Pr(2|0) = 1 - [Pr(0|0) + Pr(1|0)]
     *     Pr(2|1) = 1 - [Pr(0|1) + Pr(1|1)]
     *     Pr(2|2) = 1 - [Pr(0|2) + Pr(1|2)]
     * Similary, if there are 2 titre categories (with 2 exposure classes),
     * the vector will contain the following probabilities:
     *     { Pr(0|0),
     *       Pr(0|1) }
     */
    std::vector<Parameter *> probMatrix;
    
    /**
     * Titre cut-offs that separate titre categories. The cut-offs must be
     * arranged in ascending order. The number of cut-offs is fewer than the
     * number of categories by 1.
     */
    std::vector<Parameter *> titreCutOffs;
    
    std::string const &getTypeName() const override;
    
    struct AttrKey {
        static const std::string PROB_MATRIX;
        static const std::string TITRE_CUTOFFS;
    };
    
    void addProbabilityParameter(std::string const &paramName,
                                 ParameterPool const &parameterPool);
    
    void addTitreCutoffParameter(std::string const &paramName,
                                 ParameterPool const &parameterPool);
    
    void finalize();


public:
    explicit CategoricalTitreDistributions(Population * population);
    
    explicit CategoricalTitreDistributions(Population * population,
                                           Json const &json,
                                           ParameterPool const &parameterPool);
    
    bool isValid() const override;
    
    void computeProbTitrePerExposureClass(
            const Decimal::Type &titreValue,
            std::vector<Decimal::Type> &resultStorage) const override;
    
    Json toJson() const override;
};


#endif //DENGUEFOI_CATEGORICALTITREDISTRIBUTIONS_H
