/**
 * Created by Ha Minh Lam on 2018-01-23.
 */

#ifndef DENGUEFOI_POPULATIONPOOL_H
#define DENGUEFOI_POPULATIONPOOL_H

#include <string>
#include "framework/Serializable.h"
#include "Population.h"


class PopulationPool : public Serializable {
private:
    std::vector<Population *> populationPtrs;
    
    void addPopulation(Population * population);


public:
    
    static const std::string JSON_KEY;
    
    explicit PopulationPool(const Json &json, const ParameterPool &parameterPool);
    
    virtual ~PopulationPool();
    
    Population * searchPopulationById(const std::string &populationId) const;
    
    /**
     * Calculate the sum of log-likelihoods contributed by all the populations
     * in this population pool.
     * @note To improve performance, this method does not perform any fail-safe
     *       check.
     * @return The sum of log-likelihoods of all the populations in this pool.
     */
    long double computeLoglikelihood() const;
    
    Json toJson() const override;
    
    void addSample(const std::string &populationId,
                   const long double &age, const long double &titre);
    
    unsigned long size() const;
    
    const std::vector<Population *> &getPopulations() const;
    
    void deletePopulation(Population * population);
    
};


#endif //DENGUEFOI_POPULATIONPOOL_H
