#pragma once

// Distributed under the 3-Clause BSD License.  See accompanying
// file LICENSE or https://github.com/CLIUtils/CLI11 for details.

// cli Library includes
// Order is important for combiner script

#include "cli/Version.h"

#include "cli/Macros.h"

#include "cli/Optional.h"

#include "cli/StringTools.h"

#include "cli/Error.h"

#include "cli/TypeTools.h"

#include "cli/Split.h"

#include "cli/ConfigFwd.h"

#include "cli/Validators.h"

#include "cli/FormatterFwd.h"

#include "cli/Option.h"

#include "cli/App.h"

#include "cli/Config.h"

#include "cli/Formatter.h"
