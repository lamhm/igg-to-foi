/**
 * Created by Ha Minh Lam on 2018-01-17.
 */

#ifndef DENGUEELISA_MARKOVCHAIN_H
#define DENGUEELISA_MARKOVCHAIN_H


#include "population/PopulationPool.h"
#include "parameter/ParameterPool.h"
#include "Description.h"


class MarkovChain : public Serializable {
    friend class Description;

private:

    ParameterPool parameterPool;

    PopulationPool populationPool;

    std::vector<Decimal::Type> logLikelihood;

    Description description;

    struct AttrKey {
        static const std::string LOG_LIKELIHOOD;
    };

    void postRunProcess();

    void buildChain(const unsigned long &nIterations,
                    const unsigned long &burnin,
                    const unsigned long &maxTuningTimes = 50,
                    const unsigned long &minTuningCycleLength = 200);


public:

    explicit MarkovChain(const Json &paramPoolJson,
                         const Json &populationPoolJson);

    void loadParameterPresets(const Json &presetParamPoolJson,
                              const unsigned long &presetBurnin);

    void loadEmpiricalPriorDistributions(const Json &priorParamPoolJson,
                                         const unsigned long &priorBurnin);

    /**
     * The implementation of the Metropolis algorithm.
     * @param nIterations Number of iterations of the MCMC chain.
     */
    void run(const unsigned long &nIterations, const unsigned long &burnin);


    void addSample(const std::string &populationId,
                   const long double &age,
                   const long double &titre);


    void loadSamples(const std::vector<std::string> &populationIdList,
                     const std::vector<long double> &ageList,
                     const std::vector<long double> &titreList);


    Json toJson() const override;
};


#endif //DENGUEELISA_MARKOVCHAIN_H
