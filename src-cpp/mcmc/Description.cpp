/**
 * Created by Ha Minh Lam on 2018-01-24.
 */

#include <library/RandomEngine.h>
#include <util/format.h>
#include <util/util.h>
#include "Description.h"
#include "MarkovChain.h"



// -----------------------------------------------------------------------------

const std::string Description::JSON_KEY = "DESCRIPTION";

const std::string Description::AttrKey::MODEL_CODE = "model_code";
const std::string Description::AttrKey::NOTES = "notes";
const std::string Description::AttrKey::CHAIN_LENGTH = "chain_length";

const std::string Description::AttrKey::PARAMETER_COUNT = "parameter_count";
const std::string Description::AttrKey::FREE_PARAMETER_COUNT = "free_parameter_count";
const std::string Description::AttrKey::PRESET_PARAMETER_COUNT = "preset_parameter_count";
const std::string Description::AttrKey::SAMPLE_COUNT = "sample_count";

const std::string Description::AttrKey::MAX_LOG_LIKELIHOOD = "max_log_likelihood";
const std::string Description::AttrKey::AKAIKE_INFO_CRITERION = "akaike_info_criterion";
const std::string Description::AttrKey::BAYESIAN_INFO_CRITERION = "bayesian_info_criterion";
const std::string Description::AttrKey::DEVIANCE_INFO_CRITERION = "deviance_info_criterion";
const std::string Description::AttrKey::BURNIN = "burnin";

const std::string Description::AttrKey::RANDOM_SEED = "random_seed";
const std::string Description::AttrKey::LAST_UPDATE_TIME = "last_update";

// -----------------------------------------------------------------------------

Description::Description()
        : markovChain(nullptr), modelCode(""), notes(), chainLength(0l),
          parameterCount(0l), freeParameterCount(0l), presetParameterCount(0l),
          sampleCount(0l), maxLogLikelihood(0.0l),
          akaikeInfoCriterion(-1.0l), bayesianInfoCriterion(-1.0l),
          devianceInfoCriterion(-1.0l), burnin(0),
          randomSeed(0ul), lastUpdateTime("") {
    // Do nothing.
}

// -----------------------------------------------------------------------------

Description::Description(Json json, MarkovChain * markovChain)
        : Description() {
    
    this->markovChain = markovChain;
    
    if (!json.is_object()) {
        return;
    }
    
    if (json.element_exists(Description::JSON_KEY)) {
        json = json[Description::JSON_KEY];
    }
    
    if (json.element_exists(AttrKey::MODEL_CODE)
        && json[AttrKey::MODEL_CODE].is_string()) {
        this->modelCode = json[AttrKey::MODEL_CODE].get<std::string>();
    }
    
    if (json.element_exists(AttrKey::NOTES)) {
        if (json[AttrKey::NOTES].is_array()) {
            for (const Json &element : json[AttrKey::NOTES]) {
                if (element.is_string()) {
                    this->notes.push_back(element.get<std::string>());
                }
            }
        } else if (json[AttrKey::NOTES].is_string()) {
            this->notes.push_back(json[AttrKey::NOTES].get<std::string>());
        }
    }
    
    if (json.element_exists(AttrKey::CHAIN_LENGTH)
        && json[AttrKey::CHAIN_LENGTH].is_number_integer()) {
        this->chainLength = json[AttrKey::CHAIN_LENGTH].get<long>();
        if (this->chainLength < 0l) {
            this->chainLength = 0l;
        }
    }
    
    if (json.element_exists(AttrKey::PARAMETER_COUNT)
        && json[AttrKey::PARAMETER_COUNT].is_number_integer()) {
        this->parameterCount = json[AttrKey::PARAMETER_COUNT].get<long>();
        if (this->parameterCount < 0l) {
            this->parameterCount = 0l;
        }
    }
    
    if (json.element_exists(AttrKey::FREE_PARAMETER_COUNT)
        && json[AttrKey::FREE_PARAMETER_COUNT].is_number_integer()) {
        this->freeParameterCount =
                json[AttrKey::FREE_PARAMETER_COUNT].get<long>();
        if (this->freeParameterCount < 0.0) {
            this->freeParameterCount = 0.0;
        }
    }
    
    if (json.element_exists(AttrKey::PRESET_PARAMETER_COUNT)
        && json[AttrKey::PRESET_PARAMETER_COUNT].is_number_integer()) {
        this->presetParameterCount =
                json[AttrKey::PRESET_PARAMETER_COUNT].get<long>();
        if (this->presetParameterCount < 0l) {
            this->presetParameterCount = 0l;
        }
    }
    
    if (json.element_exists(AttrKey::SAMPLE_COUNT)
        && json[AttrKey::SAMPLE_COUNT].is_number_integer()) {
        this->sampleCount = json[AttrKey::SAMPLE_COUNT].get<long>();
        if (this->sampleCount < 0l) {
            this->sampleCount = 0l;
        }
    }
    
    if (json.element_exists(AttrKey::MAX_LOG_LIKELIHOOD)
        && json[AttrKey::MAX_LOG_LIKELIHOOD].is_number()) {
        this->maxLogLikelihood =
                json[AttrKey::MAX_LOG_LIKELIHOOD].get<long double>();
        if (this->maxLogLikelihood > 0.0l) {
            this->maxLogLikelihood = 0.0l;
        }
    }
    
    if (json.element_exists(AttrKey::AKAIKE_INFO_CRITERION)
        && json[AttrKey::AKAIKE_INFO_CRITERION].is_number()) {
        this->akaikeInfoCriterion =
                json[AttrKey::AKAIKE_INFO_CRITERION].get<long double>();
        if (this->akaikeInfoCriterion <= 0.0l) {
            this->akaikeInfoCriterion = -1.0l;
        }
    }
    
    if (json.element_exists(AttrKey::BAYESIAN_INFO_CRITERION)
        && json[AttrKey::BAYESIAN_INFO_CRITERION].is_number()) {
        this->bayesianInfoCriterion =
                json[AttrKey::BAYESIAN_INFO_CRITERION].get<long double>();
        if (this->bayesianInfoCriterion <= 0.0l) {
            this->bayesianInfoCriterion = -1.0l;
        }
    }
    
    if (json.element_exists(AttrKey::DEVIANCE_INFO_CRITERION)
        && json[AttrKey::DEVIANCE_INFO_CRITERION].is_number()) {
        this->devianceInfoCriterion =
                json[AttrKey::DEVIANCE_INFO_CRITERION].get<long double>();
        if (this->devianceInfoCriterion <= 0.0l) {
            this->devianceInfoCriterion = -1.0l;
        }
    }
    
    if (json.element_exists(AttrKey::BURNIN)
        && json[AttrKey::BURNIN].is_number_integer()) {
        this->burnin = json[AttrKey::BURNIN].get<unsigned long>();
    }
    
    if (json.element_exists(AttrKey::RANDOM_SEED)
        && json[AttrKey::RANDOM_SEED].is_number_integer()) {
        this->randomSeed = json[AttrKey::RANDOM_SEED].get<unsigned long>();
    }
    
    if (json.element_exists(AttrKey::LAST_UPDATE_TIME)
        && json[AttrKey::LAST_UPDATE_TIME].is_string()) {
        this->lastUpdateTime =
                json[AttrKey::LAST_UPDATE_TIME].get<std::string>();
    }
}

// -----------------------------------------------------------------------------

Json Description::toJson() const {
    Json json;
    
    if (!this->modelCode.empty()) {
        json[AttrKey::MODEL_CODE] = this->modelCode;
    }
    
    if (!this->notes.empty()) {
        json[AttrKey::NOTES] = this->notes;
    }
    
    if (this->chainLength > 0) {
        json[AttrKey::CHAIN_LENGTH] = this->chainLength;
    }
    
    if (this->parameterCount > 0) {
        json[AttrKey::PARAMETER_COUNT] = this->parameterCount;
    }
    
    if (this->freeParameterCount > 0) {
        json[AttrKey::FREE_PARAMETER_COUNT] = this->freeParameterCount;
    }
    
    if (this->presetParameterCount > 0) {
        json[AttrKey::PRESET_PARAMETER_COUNT] = this->presetParameterCount;
    }
    
    if (maxLogLikelihood < 0.0l) {
        json[AttrKey::MAX_LOG_LIKELIHOOD] = this->maxLogLikelihood;
    }
    
    if (sampleCount > 0l) {
        json[AttrKey::SAMPLE_COUNT] = this->sampleCount;
    }
    
    if (akaikeInfoCriterion > 0.0l) {
        json[AttrKey::AKAIKE_INFO_CRITERION] = this->akaikeInfoCriterion;
    }
    
    if (bayesianInfoCriterion > 0.0l) {
        json[AttrKey::BAYESIAN_INFO_CRITERION] = this->bayesianInfoCriterion;
    }
    
    if (devianceInfoCriterion > 0.0l) {
        json[AttrKey::DEVIANCE_INFO_CRITERION] = this->devianceInfoCriterion;
    }
    
    if (burnin > 0) {
        json[AttrKey::BURNIN] = this->burnin;
    }
    
    if (randomSeed > 0) {
        json[AttrKey::RANDOM_SEED] = this->randomSeed;
    }
    
    if (!lastUpdateTime.empty()) {
        json[AttrKey::LAST_UPDATE_TIME] = this->lastUpdateTime;
    }
    
    return json;
}

// -----------------------------------------------------------------------------

void Description::update() {
    randomSeed = RandomEngine::singleton()->getSeed();
    
    sampleCount = 0l;
    for (Population * population : markovChain->populationPool.getPopulations()) {
        sampleCount += population->getPersonCount();
    }
    
    chainLength = markovChain->logLikelihood.size();
    parameterCount = markovChain->parameterPool.size();

    std::vector<Parameter *> freeParameters =
            markovChain->parameterPool.getFreeParameters();
    this->freeParameterCount = static_cast<long double>(freeParameters.size());
    for (auto parameter : freeParameters) {
        this->freeParameterCount -= parameter->getAutoAcceptRate();
    }

    presetParameterCount =
            markovChain->parameterPool.getPresetParameters().size();
    
    if (chainLength > 0) {
        maxLogLikelihood = std::numeric_limits<long double>::lowest();
        for (long double value : markovChain->logLikelihood) {
            if (maxLogLikelihood < value) {
                maxLogLikelihood = value;
            }
        }
    }
    
    lastUpdateTime = format::formatDateTime();
}

// -----------------------------------------------------------------------------

void Description::updateInfoCriteria(const unsigned long &burnin) {
    this->burnin = burnin;
    
    akaikeInfoCriterion = -1.0l;
    bayesianInfoCriterion = -1.0l;
    devianceInfoCriterion = -1.0l;
    
    if (sampleCount > 0l) {
        std::vector<Parameter *> freeParameters =
                markovChain->parameterPool.getFreeParameters();

        /**
         * AIC = 2K  -  2 * max(logL)
         * where K    = number of free parameters;
         *       logL = log-likelihood.
         */
        akaikeInfoCriterion = 2.0l * (freeParameterCount - maxLogLikelihood);
        
        
        /**
         * BIC = log(N) * K  -  2 * max(logL)
         * where N = number of samples.
         */
        bayesianInfoCriterion =
                (std::log(static_cast<long double>(sampleCount)) *
                 freeParameterCount) - (2.0l * maxLogLikelihood);
        
        
        if (burnin < chainLength) {
            long double sumWeight =
                    1.0l / static_cast<long double>(chainLength - burnin);
            long double meanLogLikelihood = 0.0l;
            for (auto i = burnin; i < chainLength; i++) {
                meanLogLikelihood += sumWeight * markovChain->logLikelihood[i];
            }
            
            for (auto param : freeParameters) {
                param->setProposal(param->getMeanValue(burnin));
                param->notifyObservers();
            }
            long double logLikelihoodOfMeanParam =
                    markovChain->populationPool.computeLoglikelihood();
            
            /**
             * DIC = 2 * mean(D)      -  D(meanParam)
             *     = -4 * mean(logL)  +  2 * logL(meanParam)
             * where mean(D)      = -2 * mean(logL)
             *       D(meanParam) = -2 * logL(meanParam)
             *       meanParam    = mean values of parameters.
             */
            devianceInfoCriterion =
                    (-4) * meanLogLikelihood + 2 * logLikelihoodOfMeanParam;
        }
    }
    
    lastUpdateTime = format::formatDateTime();
}

// -----------------------------------------------------------------------------

void Description::print() const {
    util::outputStream << "\nTotal number of samples     : "
                       << sampleCount
                       << "\nTotal number of parameters  : "
                       << parameterCount
                       << "\nNumber of free parameters   : "
                       << freeParameterCount
                       << "\nNumber of preset parameters : "
                       << presetParameterCount;
    
    if (chainLength > 0) {
        util::outputStream << "\nCurrent Markov chain length : " << chainLength;
    }
    
    util::outputStream << "\n";
    
}

// -----------------------------------------------------------------------------
