/**
 * Created by Ha Minh Lam on 2018-01-17.
 */

#include <util/data.h>
#include "MarkovChain.h"
#include "util/util.h"


// -----------------------------------------------------------------------------

const std::string MarkovChain::AttrKey::LOG_LIKELIHOOD = "LOG_LIKELIHOOD";

// -----------------------------------------------------------------------------
MarkovChain::MarkovChain(const Json &paramPoolJson,
                         const Json &populationPoolJson)
        : parameterPool(paramPoolJson),
          populationPool(populationPoolJson, parameterPool),
          description(populationPoolJson, this), logLikelihood() {

    if (!populationPoolJson.is_object()) {
        return;
    }

    // Bind parameters together
    this->parameterPool.linkParameters();

    // Load the log-likelihood vector if it exists.
    if (populationPoolJson.element_exists(AttrKey::LOG_LIKELIHOOD)) {
        bool error = false;
        Json logLikelihoodJson = populationPoolJson[AttrKey::LOG_LIKELIHOOD];

        if (logLikelihoodJson.is_array()) {
            for (const Json &logLikelihoodValue : logLikelihoodJson) {
                if (logLikelihoodValue.is_number()) {
                    this->logLikelihood.emplace_back(
                            logLikelihoodValue.get<Decimal::Type>());
                } else {
                    error = true;
                }
            }
        } else {
            error = true;
        }

        if (error) {
            throw std::runtime_error(
                    "ERROR: Loading JSON file failed. Expecting a "
                    "list of numbers for log-likelihood.");
        }
    }
}

// -----------------------------------------------------------------------------

void MarkovChain::loadParameterPresets(const Json &presetParamPoolJson,
                                       const unsigned long &presetBurnin) {
    if (presetParamPoolJson.is_null() || !presetParamPoolJson.is_object()) {
        throw std::runtime_error(
                "ERROR: Preset parameter pool -- Invalid json structure.");
    }

    ParameterPool presetParamPool(presetParamPoolJson);
    this->parameterPool.loadPresetValues(presetParamPool, presetBurnin);
}

// -----------------------------------------------------------------------------

void MarkovChain::loadEmpiricalPriorDistributions(
        const Json &priorParamPoolJson,
        const unsigned long &priorBurnin) {

    if (priorParamPoolJson.is_null() || !priorParamPoolJson.is_object()) {
        throw std::runtime_error(
                "ERROR: Prior parameter pool -- Invalid json structure.");
    }

    ParameterPool priorParamPool(priorParamPoolJson);
    this->parameterPool.loadPriorDistributions(priorParamPool, priorBurnin);
}

// -----------------------------------------------------------------------------

void MarkovChain::addSample(const std::string &populationId,
                            const long double &age,
                            const long double &titre) {

    Population *populationPtr =
            this->populationPool.searchPopulationById(populationId);

    if (populationPtr != nullptr) {
        populationPtr->addSample(age, titre);
    }
}

// -----------------------------------------------------------------------------

void MarkovChain::loadSamples(const std::vector<std::string> &populationIdList,
                              const std::vector<long double> &ageList,
                              const std::vector<long double> &titreList) {

    long populationCount = this->populationPool.size();
    if (populationCount <= 0) {
        return;
    }

    unsigned long sampleCount = ageList.size();
    if (sampleCount != titreList.size() ||
        sampleCount != populationIdList.size()) {
        throw std::runtime_error(
                "ERROR: The lists of ages, of titres, and of population IDs "
                "are not in the same length.");
    }

    for (unsigned long sampleIdx = 0; sampleIdx < sampleCount; sampleIdx++) {
        this->addSample(populationIdList[sampleIdx],
                        ageList[sampleIdx],
                        titreList[sampleIdx]);
    }
}

// -----------------------------------------------------------------------------

void MarkovChain::postRunProcess() {
    // Prepare all populations for the run
    std::vector<Population *> unusedPopulations;
    util::outputStream << "Population list :\n";
    for (Population *population : this->populationPool.getPopulations()) {
        population->readyForMcmc();
        unsigned long populationSize = population->getPersonCount();
        if (populationSize > 0) {
            util::outputStream << "    " << population->getId() << " : "
                               << populationSize << " samples.\n";
        } else {
            // Mark the population unused if it does not have any samples
            unusedPopulations.emplace_back(population);
        }
    }

    // Delete unused populations
    if (!unusedPopulations.empty()) {
        for (Population *unusedPopulation : unusedPopulations) {
            this->populationPool.deletePopulation(unusedPopulation);
        }
    }

    // Remove unused parameters from the parameter pool.
    // This must be done after deleting unused populations.
    this->parameterPool.deleteUnusedParameters();

    // Throw an error if there is no populations / samples / parameters left
    if (this->populationPool.size() <= 0) {
        throw std::runtime_error("ERROR: No sample found.");
    } else if (this->parameterPool.size() <= 0) {
        throw std::runtime_error("ERROR: No parameter found.");
    }

    // Update and print the statistics of the Markov chain
    this->description.update();
    this->description.print();
}

// -----------------------------------------------------------------------------

void MarkovChain::buildChain(const unsigned long &nIterations,
                             const unsigned long &burnin,
                             const unsigned long &maxTuningTimes,
                             const unsigned long &minTuningCycleLength) {
    if (nIterations <= 0) {
        return;
    }

    // Get the list of free parameters.
    // These parameters will be altered during the run.
    std::vector<Parameter *> freeParameters =
            this->parameterPool.getFreeParameters();

    // Get the list of preset parameters.
    // Initialize those parameters if necessary.
    std::vector<Parameter *> presetParameters =
            this->parameterPool.getPresetParameters();
    for (Parameter *presetParameter : presetParameters) {
        presetParameter->update(nullptr);
        if (!presetParameter->hasValues()) {
            presetParameter->recordCurrentValue();
        }
    }

    // Reserve the log-likelihood vector
    auto preExistingLikelihood = this->logLikelihood.size();
    this->logLikelihood.reserve(preExistingLikelihood + nIterations + 1ul);

    // Retrieve or calculate the log-likelihood of the starting state
    long double lastLogLikelihood;
    if (preExistingLikelihood > 0) {
        lastLogLikelihood = this->logLikelihood[preExistingLikelihood - 1];
    } else {
        lastLogLikelihood = this->populationPool.computeLoglikelihood();
        this->logLikelihood.emplace_back(lastLogLikelihood);
    }

    // Calculate the initial acceptability value
    long double lastAcceptability =
            lastLogLikelihood + this->parameterPool.computeLogPrior();

    // Create a Progress object for this time consuming task
    std::string message;
    if (preExistingLikelihood > 0) {
        message = "Extending the Markov chain by " +
                  format::formatNumber(nIterations, nullptr, ',') +
                  " iterations... ";

    } else {
        message = "Building a Markov chain of " +
                  format::formatNumber(nIterations, nullptr, ',') +
                  " iterations... ";
    }
    util::Progress progress(message, nIterations, 10l); // Update rate: 10 sec
    progress.print(0l, true, true);


    std::vector<Parameter *> sampledParameters;
    for (Parameter *parameter : freeParameters) {
        for (Integer::Type i = 0; i < parameter->getSamplingFrequency(); i++) {
            sampledParameters.emplace_back(parameter);
        }
    }

    // Shuffle the list of sampled parameters
    RandomEngine::singleton()->shuffle(sampledParameters);

    // Calculate how frequently parameter tuning should be performed
    bool isTuningFinished = false;
    const unsigned long maxIterForTuning = burnin * 7 / 10;
    unsigned long tuningCycleLength = maxIterForTuning / maxTuningTimes;
    if (tuningCycleLength < minTuningCycleLength) {
        tuningCycleLength = minTuningCycleLength;
    }

    // Execute the MCMC run...
    const unsigned long startIter = preExistingLikelihood > 0 ? 0ul : 1ul;
    for (auto iteration = startIter; iteration < nIterations; iteration++) {
        const unsigned long realIter = iteration - startIter;

        progress.print(iteration, false);

        // Update free parameters
        for (Parameter *parameter : sampledParameters) {
            // Propose a new value for the considered parameter.
            // All related variables will also be automatically updated.
            parameter->proposeNewValue();

            // Calculate the new log-likelihood and the new acceptability value
            long double newLogLikelihood = populationPool.computeLoglikelihood();
            long double newAcceptability =
                    newLogLikelihood + parameterPool.computeLogPrior();

            long double logAcceptanceRatio =
                    newAcceptability - lastAcceptability;
            auto randomNum = RandomEngine::singleton()->
                    uniformReal<long double>(0.0l, 1.0l);

            if (std::log(randomNum) <= logAcceptanceRatio
                || randomNum <= parameter->getAutoAcceptRate()) {
                // Accept the proposed value if the new acceptability is
                // "high enough", or the randomly generated number is smaller
                // than the auto-accept rate of the parameter.
                lastLogLikelihood = newLogLikelihood;
                lastAcceptability = newAcceptability;
                parameter->acceptNewProposal();

            } else {
                // Reject the proposed value.
                // All related variables will also be re-calculated.
                parameter->rejectNewProposal();
            }
        }

        // Record the values of parameters at the end of each iteration.
        for (Parameter *freeParameter : freeParameters) {
            freeParameter->recordCurrentValue();
        }
        for (Parameter *presetParameter : presetParameters) {
            presetParameter->recordCurrentValue();
        }

        // Store the log-likelihood at the end of each iteration.
        this->logLikelihood.emplace_back(lastLogLikelihood);

        // Tuning proposal distributions if necessary
        if (!isTuningFinished
            && realIter > 0
            && realIter <= maxIterForTuning
            && realIter % tuningCycleLength == 0) {

            isTuningFinished = true;
            for (Parameter *freeParameter : freeParameters) {
                bool moreTuningRequired = freeParameter->tuneProposer();
                isTuningFinished = isTuningFinished && (!moreTuningRequired);
            }
        }
    }

    progress.print(nIterations, false, true);
    util::outputStream << "\n\n";
}

// -----------------------------------------------------------------------------

void MarkovChain::run(const unsigned long &nIterations,
                      const unsigned long &burnin) {
    this->postRunProcess();
    this->buildChain(nIterations, burnin);

    // Update the description of the Markov chain
    this->description.update();
    this->description.updateInfoCriteria(burnin);
}

// -----------------------------------------------------------------------------

Json MarkovChain::toJson() const {
    Json json;
    json[ParameterPool::JSON_KEY] = parameterPool.toJson();
    json[PopulationPool::JSON_KEY] = populationPool.toJson();
    json[Description::JSON_KEY] = description.toJson();
    json[MarkovChain::AttrKey::LOG_LIKELIHOOD] = logLikelihood;
    return json;
}

// -----------------------------------------------------------------------------
