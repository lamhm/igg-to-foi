/**
 * Created by Ha Minh Lam on 2018-01-24.
 */

#ifndef DENGUEFOI_DESCRIPTION_H
#define DENGUEFOI_DESCRIPTION_H


#include "framework/Serializable.h"


class MarkovChain;


class Description : public Serializable {
private:
    
    MarkovChain* markovChain;
    
    std::string modelCode;
    
    std::vector<std::string> notes;
    
    long chainLength;
    
    long parameterCount;
    long double freeParameterCount;
    long presetParameterCount;
    long sampleCount;
    
    long double maxLogLikelihood;
    long double akaikeInfoCriterion;
    long double bayesianInfoCriterion;
    long double devianceInfoCriterion;
    unsigned long burnin;
    
    unsigned long randomSeed;
    std::string lastUpdateTime;
    
    explicit Description();
    
    struct AttrKey {
        static const std::string MODEL_CODE;
        static const std::string NOTES;
        static const std::string CHAIN_LENGTH;
        
        static const std::string PARAMETER_COUNT;
        static const std::string FREE_PARAMETER_COUNT;
        static const std::string PRESET_PARAMETER_COUNT;
        
        static const std::string SAMPLE_COUNT;
        
        static const std::string MAX_LOG_LIKELIHOOD;
        static const std::string AKAIKE_INFO_CRITERION;
        static const std::string BAYESIAN_INFO_CRITERION;
        static const std::string DEVIANCE_INFO_CRITERION;
        static const std::string BURNIN;
        
        static const std::string RANDOM_SEED;
        static const std::string LAST_UPDATE_TIME;
    };
    

public:
    
    static const std::string JSON_KEY;
    
    explicit Description(Json json, MarkovChain * markovChain);
    
    Json toJson() const override;
    
    void update();
    
    void updateInfoCriteria(const unsigned long &burnin);
    
    void print() const;
};


#endif //DENGUEFOI_DESCRIPTION_H
